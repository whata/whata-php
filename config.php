<?php
	define('WHATA_SOURCE_LABEL', 'Source : '); /* The label to use in a quote header */
	define('WHATA_SHOW_QUOTE_HEADER', true); /* Show the source of a blockquote */
	define('WHATA_QUOTE_HEADER_BEFORE', false); /* Show the header before a blockquote */
	define('WHATA_OUTER', true); /* Will put generated code into a <div class="wata"> ou <span class="whata"> */
	define('WHATA_MATH_METHOD', 'url');
	define('WHATA_MATH_URL', 'http://localhost/cgi-bin/mathtex.cgi?');
	define('WHATA_MATH_INCLUDE', '../mtex2img.php');
	define('WHATA_IMAGE_PREFIX','');
	define('WHATA_MIN_TITLE_LEVEL', 1);
	define('WHATA_SIMPLEX', false); // Enables Whata Simplex Mode by default
	define('WHATA_SIMPLEX_SEMANTIC', false); // use <em> and <strong> rather than <i> and <b> for simplex synthax (you must enable it for xhtml 1.0 and 1.1, you can enable it for plain html)
	define('WHATA_GESHI_URL', ''); // where to find GeSHi if needed
	define('WHATA_HTML5', true); // enable html5 features

    if (strpos(strtolower(php_uname()), "windows" ) === false) {
        if (!defined( "PATH_SEPARATOR")) {
            define("PATH_SEPARATOR", ":");
        }

        set_include_path(
            get_include_path() . PATH_SEPARATOR .
            __DIR__ . '/../geshi-1.0' . PATH_SEPARATOR .
            __DIR__ . '/../geshi' . PATH_SEPARATOR .
            __DIR__ . '/../php-geshi' . PATH_SEPARATOR .
            '/usr/share/php-geshi' . PATH_SEPARATOR .
            '/usr/local/share/php-geshi' . PATH_SEPARATOR .
            '/opt/php-geshi' . PATH_SEPARATOR .
            '/opt/geshi-1.0' . PATH_SEPARATOR .
            '/opt/geshi'
        );
    }
?>
