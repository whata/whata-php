#ifdef{title}
<h1 id="doc-title">${html-inline title}</h1>
#fidef{title}
#ifdef{author}
<p id="doc-author">${html-inline author}</p>
#fidef{author}
#ifdef{date}
<p id="doc-date">${html-inline date}</p>
#fidef{date}
<div id="doc-content">
    ${!content}
</div>
${!footnotes}

