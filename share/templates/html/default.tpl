<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" #ifdef{lang}lang="${text lang}" xml:lang="${text lang}"#fidef{lang}>
    <head>
        ${!meta_charset}
        <title>${text title}#ifndef{title} Untitled document #findef{title}</title>
        #ifdef{author}
        <meta name="author" content="${text author}" />
        #fidef{author}

        ${!styles}
        ${!scripts}
    </head>
    <body class="whata">
        #ifdef{title}
        <h1 id="doc-title">${html-inline title}</h1>
        #fidef{title}
        #ifdef{author}
        <p id="doc-author">${html-inline author}</p>
        #fidef{author}
        #ifdef{date}
        <p id="doc-date">${html-inline date}</p>
        #fidef{date}

        <div id="doc-content">
            ${!content}
        </div>

        ${!footnotes}
    </body>
</html>
