<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" #ifdef{lang}lang="${text lang}" xml:lang="${text lang}"#fidef{lang}>
    <head>
        ${!meta_charset}
        <title>${text title}#ifndef{title} Untitled presentation #findef{title}</title>
        #ifdef{author}
        <meta name="author" content="${text author}" />
        #fidef{author}

        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!--[if lt IE 9]>
            var t = ['header','nav','section','article','aside','footer','hgroup'];
            while(t.length) document.createElement(t.pop());
        <![endif]-->
        <script src="${!tplfiles}/reveal/lib/js/head.min.js"></script>
        <script src="${!tplfiles}/reveal/js/reveal.js"></script>
        <script>
            window.addEventListener('DOMContentLoaded', function() {
                var head = document.getElementsByTagName('head')[0];
                // If the query includes 'print-pdf', use the PDF print sheet

                var link   = document.createElement('link');
                link.rel   = 'stylesheet';

                link.href = '${!tplfiles}/reveal/css/reveal.min.css';
                head.insertBefore(link, head.firstChild);

                var lastLink = link;

                link = link.cloneNode(false);
                link.href = '${!tplfiles}/reveal/css/theme/${text theme}#ifndef{theme}default#findef{theme}.css';
                link.id = 'theme';
                head.insertBefore(link, lastLink.nextSibling);

                lastLink = link;
                link = link.cloneNode(false);
                link.id = null;
                link.media = 'print',
                link.href  = '${!tplfiles}/reveal/css/print/' + ( window.location.search.match( /print-pdf/gi ) ? 'pdf' : 'paper' ) + '.css';
                head.insertBefore(link, lastLink.nextSibling);

                var links = document.getElementsByTagName('a'), i, len, e, root = document.querySelector('.reveal>.slides');

                for (i = 0, len = links.length; i < len; ++i) {
                    if (links[i].href && links[i].href[0] === '#') {
                        e = document.querySelector(links[i].href);
                        if (e) {
                            links[i].href = '#/' + e.id;
                        }
                    }
                }

                Reveal.initialize({
                    controls: true,
                    progress: true,
                    history: true,
                    backgroundTransition:"${text backgroundTransition}",
                    parallaxBackgroundImage:"${text parallaxBackgroundImage}",
                    parallaxBackgroundSize:"${text parallaxBackgroundImage}",
                    center: true,
                    theme: Reveal.getQueryHash().theme, // available themes are in /css/theme
                    transition: Reveal.getQueryHash().transition || '${text transition}#ifndef{transition}none#findef{transition}', // default/cube/page/concave/zoom/linear/fade/none
                    width: 960,
                    height: 700,
                    margin: 0.1,
                    minScale: 0.2,
                    maxScale: 10.0,
                #ifdef{slideNumber}
                    slideNumber: true,
                #fidef{slideNumber}
                    dependencies: document.body.classList ? [
                        { src: '${!tplfiles}/reveal/plugin/zoom-js/zoom.js', async: true},
                        { src: '${!tplfiles}/reveal/plugin/notes/notes.js', async: true}
                    ] : []
                });

                document.body.classList.add('whata-presentation-enabled');
            });
        </script>
        ${!styles}

        ${!scripts}
    </head>
    <body class="whata">
        <div id="doc-content" class="reveal">
            <div class="slides">
                #ifdef{autoFirstSlide}
                <section id="first-slide" class="whata-slide">
                    #ifdef{title}
                    <h1 id="doc-title">${html-inline title}</h1>
                    #fidef{title}
                    #ifdef{author}
                    <p id="doc-author">${html-inline author}</p>
                    #fidef{author}
                    #ifdef{date}
                    <p id="doc-date">${html-inline date}</p>
                    #fidef{date}
                </section>
                #fidef{autoFirstSlide}
                ${!content}
            </div>
        </div>

        ${!footnotes}
    </body>
</html>
