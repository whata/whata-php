<?php

register_whata_custom_tags(
    function ($node, $flags, &$mustclear, &$openedParagr, & $conf, &$html)
    {
        $toFrench = function ($n)
        {
            switch($n)
            {
                case 'def': return 'Définition';
                case 'theo': return 'Théorème';
                case 'propo': return 'Proposition';
                case 'propr': return 'Propriété';
                case 'coro': return 'Corollaire';
                case 'preuve': return 'Preuve';
                case 'rmq': return 'Remarque';
                case 'lemme': return 'Lemme';
                case 'ex': return 'Exemple';
                case 'note': return 'Note';
                default: return 'Boîte';
            }
        };

        if($html) {
            // we shouldn't do anything if the tag was already handled by another custom tags set
            return false;
        }

        switch($node['tagName'])
        {
            case 'def':
            case 'theo':
            case 'propr':
            case 'propo':
            case 'coro':
            case 'preuve':
            case 'rmq':
            case 'note':
            case 'lemme':
            case 'ex':
                closeParagr($html, $openedParagr, $conf);

                $node['text'] = '<h1>' . $toFrench($node['tagName']) . (isset($node['attributes'][0]) && $node['attributes'][0] ? ' (<strong>' . trim(whatadom2html($node['attributes'][0], $flags | WHATAHTML_INLINE, $conf)) . '</strong>)' : '') . '</h1><div class="in-box">'
                    . whatadom2html($node['content'], $flags, $conf)
                    . '</div>';

                $html .= whatahtml_tag('section', $node, $flags, $mustclear, $openedParagr, $conf, array('class'=>'box ' . $node['tagName']));
                break;
            case 'schem':
                closeParagr($html, $openedParagr, $conf);
                $html .= whatahtml_tag('pre', $node, $flags | WHATAHTML_PRE | WHATAHTML_DO_NOT_AUTO_OPEN_PARAGR, $mustclear, $openedParagr, $conf, array('class'=>'illustr'));
                break;
            case 'dot':
                $descriptorspec = array(
                    0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
                    1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
                    2 => array("pipe", "w") // stderr is a file to write to
                );

                $process = proc_open('timeout 10 dot -Tsvg', $descriptorspec, $pipes);

                if (is_resource($process)) {
                    fwrite($pipes[0], whata_text_content($node));
                    fclose($pipes[0]);
                    $node['text'] = '';
                    $html .= whatahtml_tag('img', $node, $flags, $mustclear, $openedParagr, $conf, array(
                        'class'=>'whata-dot',
                        'src'=> 'data:image/svg+xml;base64,' . base64_encode(stream_get_contents($pipes[1])),
                        'autoclose'
                    ));
                    echo stream_get_contents($pipes[1]);
                    echo stream_get_contents($pipes[2]);
                    fclose($pipes[1]);
                    fclose($pipes[2]);

                    proc_close($process);
                }
                break;
            default:
                return false;
        }
        return true;
    }
);

?>
