<?php

register_whata_custom_tags(
    function (&$node, $flags, &$mustclear, &$openedParagr, & $conf, &$html)
    {
        if (isset($node['attributes']['delayed'])) {
            $node['customAttributes']['class'] = (isset($node['customAttributes']['class']) ? $node['customAttributes']['class'] . ' ':'') . 'fragment';
            $node['customAttributes']['data-fragment-index'] = $node['attributes']['delayed'];

            if (isset($node['attributes']['transition'])) {
                $node['customAttributes']['class'] .= ' ' . $node['attributes']['transition'];
            }
        }

        if (isset($node['attributes']['navigate'])) {
            $node['customAttributes']['class'] = (isset($node['customAttributes']['class']) ? $node['customAttributes']['class'] . ' ':'') . 'navigate-' . htmlspecialchars($node['attributes']['navigate']);
        }

        if ($html) {
            // we shouldn't do anything if the tag was already handled by another custom tags set
            return false;
        }

        switch($node['tagName'])
        {
            case 'slide':
                static $slide_id = 0;
                ++$slide_id;
                closeParagr($html, $openedParagr, $conf);

                if (isset($node['attributes']['docBackground'])) {
                    $node['customAttributes']['data-bakcground'] = $node['attributes']['docBackground'];
                }

                if (isset($node['attributes']['docState'])) {
                    $node['customAttributes']['data-state'] = $node['attributes']['docState'];
                }

                if (isset($node['attributes']['transition'])) {
                    $node['customAttributes']['data-transition'] .= ' ' . $node['attributes']['transition'];
                }

                if (isset($node['attributes']['transitionSpeed'])) {
                    $node['customAttributes']['data-transition-speed'] .= ' ' . $node['attributes']['transitionSpeed'];
                }

                if (isset($node['attributes']['autoSlide'])) {
                    $node['customAttributes']['data-autoSlide'] .= ' ' . $node['attributes']['autoSlide'];
                }

                $html .= whatahtml_tag('section', $node, $flags, $mustclear, $openedParagr, $conf, array('class' => 'whata-slide', 'id' => isset($node['attributes']['ref']) ? htmlspecialchars($node['attributes']['ref']) : $slide_id), array('prefix' => '<div>', 'suffix' => '</div>'));
                break;
            case 'presentator':
                $html .= whatahtml_tag('aside', $node, $flags, $mustclear, $openedParagr, $conf, array('class' => 'whata-slide-presentator notes'));

            case ' ':
                // override links if slide:
                break;
            default:
                return false;
        }
        return true;
    }
);

?>