<?php
	/*
		This file will make whata.php and simplex.php from whata.dev.php.
		Use it as a command line program (PHP CLI).
	*/

	$whata_dev = file_get_contents('whata.dev.php');

	$both = str_replace('<?php', '<?php /* Do not modify this file, it is automatically generated from whata.dev.php */',
			preg_replace('#\n*/\*\* @@@ OFF \*/[\s\S]+/\*\* ON @@@ \*/(\n?)\n*#U', '$1', $whata_dev));

	file_put_contents(
		'whata.php',
		preg_replace('#\n*/\*\* not whata simplex end \*/(\n?)\n*#U', '$1',
	        	preg_replace('#(\n?)\n*/\*\* not whata simplex \*/\n*#U', '$1',
	        		preg_replace('#/\*\* for whata simplex ?: [\s\S]+ \*/#U', '', $both)))
	);

	file_put_contents(
		'simplex.php',
		str_replace('(,', '(',
		preg_replace('/,?[\s]*&?\$(?:must_clear|vals) ?(?:= ?(?:array ?\( ?\)|[^;,\)]+;?))?/', '',
			preg_replace('#(\n?)\n*/\*\* not whata simplex \*/[\s\S]*/\*\* not whata simplex end \*/\n*#U', '$1',
				preg_replace('#(\n?)\n*/\*\* for whata simplex ?:\n*([\s\S]+)\n*\*/(\n?)\n*#U', '$1/** not whata simplex end */$2/** not whata simplex */$3', $both))))
	);
?>
