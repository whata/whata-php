= Introduction to [whata]

[whata] exists in two versions: Simplex and Advanced. The Simplex version supports basics stuffs that does not hurt the user that is not at ease with computers but still offers a good formatting solution. The Advanced version doest quite a lot of things and is flexible but user must take care with special characters like '~[', '~]' and '~~' (which is the escape character).

[whata] can be customized with CSS in order to look like what you want and produce a lightweight HTML code. For a real example, you can read the original [whata] code of this page, since it's written in [whata]. You can use [whata] anywhere and is very simple to use in other projects. There is already a Dotclear plugin to use [whata] with it.

Lets begin a basic presentation of the syntax. As of today, a full documentation of the syntax and the code of the parser is still to be written...

== Titles
 - To write a title, put a ``=`` before.
 - Second level title : ``==``
 - Third level : ``===``
 - ...

Note: titles are part of [whata] Simplex.

== Basic formating: bold, italic, underline, strike

 - **Bold**: [code=whata <<<**Text in bold**>>>] becomes: [output **Text in bold**]
 - **Italic** : [code=whata <<<''Text in italic''>>>] becomes: [output ''Text in italic'']
    ''Note:'' These are not double quotes like this: ``"``, but two single quotes : ``''``
 - **Underline**:  [code=whata <<<__underlined text__>>>] becomes: [output __underlined text__]
 - **strike**: [code=whata <<<--This is false-->>>] Becomes: [output --This is false--]

Note: basic formating is supported by [whata] Simplex.
== Lists

Begin each elements of a list by a dash (``-``) or a star (``*``). For ordered lists, use ``#`` instead. Lists in lists are possible, juste add spaces before sublists or add a dash or a star to each element of the sublist.

=== Examples

 * [ **Unordered lists:**
[code=whata <<< pieces of furniture:
 - a chair which has:
   - a back
   - four legs
 - a table
 - a wardrobe
 - --a hammer-- >>>]

Which is equivalent to:

[code=whata <<< pieces of furniture:
 - a chair which has:
 -- a back
 -- four legs
 - a table
 - a wardrobe
 - --a hammer-- >>>]

Renders like this:

[output pieces of furniture:
 - a chair which has:
   - a back
   - four legs
 - a table
 - a wardrobe
 - --a hammer--]
]

 * [ **Ordered lists:**
 [code=whata <<< The following events happen in this exact order:
 # The morning
 	# breakfast
 	# doughnut 
 	# You are hungry at 11.30am
 # midday
 # evening >>>]
  or:
[code=whata <<< The following events happen in this exact order:
 # The morning
 ## breakfast
 ## doughnut 
 ## You are hungry at 11.30am
 # midday
 # evening >>>]
 Renders:
 [output The following events happen in this exact order:
 # The morning
 	# breakfast
 	# doughnut 
 	# You are hungry at 11.30am
 # midday
 # evening]

 Lists are part of [whata] Simplex.
]


== Paragraphs

Separate paragraphs with blank lines. A new line is just a new line in a paragraph, a blank line closes a paragraph. Another one is open if some text is found after a new line.

Note: Paragraph and new line mechanisms are part of [whata] Simplex.
== Colors
	[code=whata <<<
[color=the_color Your content]
	>>> ]

	where color is a CSS color, [color=red **without any space**].

Note: you need full-featured [whata] in order to use advanced formating like colors.
== Images

For a simple image:

	[code=whata <<<
[img=path/to/your/image.png]
	>>> ]

For an image with a title and an alternative text (better):
	[code=whata <<<
[img=path/to/your/image.png | the alternative text]
	>>> ]

Note: you need full-featured [whata] in order to use images. Maybe a mean to include images in [whata] Simplex will exist in the future.