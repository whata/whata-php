<?php /* Do not modify this file, it is automatically generated from whata.dev.php */
/*
	Copyright (C) 2010 JAKSE Raphaël

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
include(dirname(__FILE__) . '/config.php');

/* constants used inside the parser */
define('_WHATA_CAN_OUTER',32);

define('_WHATA_BLOCK',1);
define('_WHATA_EMBEDED',2);
define('_WHATA_STOP_AFTER_NL',8);
define('_WHATA_CAN_AUTO_OPEN_PARAGRAPHS', 32);
include('geshi.php');

define('_WHATA_PRE',4);
define('_WHATA_STOP_AFTER_PIPE',16);

if(!defined('WHATA_ATTR_START'))
	define('WHATA_ATTR_START', '<');

if(!defined('WHATA_ATTR_END'))
	define('WHATA_ATTR_END', '>');

if(!defined('WHATA_ATTR_SEP'))
	define('WHATA_ATTR_SEP', '|');

if(!defined('WHATA_ATTR_VALUE'))
	define('WHATA_ATTR_VALUE', '=');

if(!defined('WHATA_TAG_START'))
	define('WHATA_TAG_START', '[');

if(!defined('WHATA_TAG_END'))
	define('WHATA_TAG_END', ']');

if(!defined('WHATA_CDATA_START'))
	define('WHATA_CDATA_START', '<');

if(!defined('WHATA_CDATA_END'))
	define('WHATA_CDATA_END', '>');

if(!defined('WHATA_ESCAPE_CHAR'))
	define('WHATA_ESCAPE_CHAR', '~');

global $whata_global_conf;
$whata_global_conf = array // default conf (whata.conf.php)
(

// the method used to handle maths (url, include, function)
	'MATH_METHOD'         => WHATA_MATH_METHOD,

// the http script generating math images if MATH_METHOD = 'url'
	'MATH_URL'            => WHATA_MATH_URL,

// the file defining whata_math_to_html if MATH_METHOD = 'include'
	'MATH_INCLUDE'        => WHATA_MATH_INCLUDE,

// the name of the function generating math markup
	'MATH_FUNCTION'       => 'whata_math2html', // can only be modified by the script that includes us if MATH_METHOD = function

// TRUE = we must show informations aubout quote
	'SHOW_QUOTE_HEADER'   => WHATA_SHOW_QUOTE_HEADER,

// TRUE = the information is displayed BEFORE the quote
	'QUOTE_HEADER_BEFORE' => WHATA_QUOTE_HEADER_BEFORE,

// the text to show before the quote's source
	'SOURCE_LABEL'        => WHATA_SOURCE_LABEL,

	'IMAGE_PREFIX'        => WHATA_IMAGE_PREFIX,
	'WHATA_SIMPLEX'       => WHATA_SIMPLEX,

/** for whata simplex:

	'WHATA_SIMPLEX'       => true,

*/

	'SIMPLEX_SEMANTIC'       => WHATA_SIMPLEX_SEMANTIC,

	'MIN_TITLE_LEVEL'     => WHATA_MIN_TITLE_LEVEL,

// TRUE = wrap whata code with <div class="whata"> ... </div> or <span class="whata"> ... </span>
	'OUTER'               => WHATA_OUTER
);

if(isset($whata_override_conf)) // This allow the script that includes us to redefine our conf on the fly
{
	foreach($whata_override_conf as $key => $value)
	{
		$whata_global_conf[$key] = $value;
	}
}

function whata_MathJax($code, $displayBlock=false)
{
	if($displayBlock)
	{
		$delimL = '\[';$delimR = '\]';
		$tagName = 'div';
	}
	else
	{
		$delimL = '\(';$delimR = '\)';
		$tagName = 'span';
	}

	return array(
		'name'=>$tagName,
		'attr'=>array
		(
			'title'=>$code,
			'class'=>'whata-math'
		),
		'content'=>$delimL . htmlspecialchars($code) . $delimR,
		'autoclose'=>false
	);
}

if($whata_global_conf['MATH_METHOD'] === 'url')
{
	function whata_math2html($code)
	{
		return array(
			'name'=>'img',
			'attr'=>array
			(
				'alt'=> $code,
				'title'=>$code,
				'src'=> $GLOBALS['whata_global_conf']['MATH_URL'] . $code,
				'class'=>'whata-math'
			),
			'content'=>'',
			'autoclose'=>true
		);
	}
}
else if(!($whata_global_conf['MATH_METHOD'] === 'function' && isset($whata_global_conf['MATH_FUNCTION'])))
	include($whata_global_conf['MATH_INCLUDE']); // included file MUST have a whata_math_to_html function.
// else $whata_override_conf['MATH_FUNCTION'] must be a valid function name

function whata_simplex($enable = true)
{// simplify the Whata! (enable the Whata! Simplex mode)
	$GLOBALS['whata_global_conf']['WHATA_SIMPLEX'] = $enable;
}

function is_blank($c, $includeNL = true)
{
	return $c === ' ' || $c === '	' || ($includeNL && ($c === "\n")); // "\r" should not happen
}

function whata_get_following_word($s,&$i,$len)
{
	$r = '';
	while($i < $len && preg_match('/[a-zA-Z]/',$s[$i]))
	{
		$r.= $s[$i];
		++$i;
	}
	return $r;
}

function whata_simplex_tag($closing_c,&$s,&$i,$len, &$opts, &$conf, $vals, $parse = true)
{ // will return string parsed in "string$closing_c$closing_c" (don't parse if $parse = false)
	if($opts & _WHATA_EMBEDED)
		$opts ^= _WHATA_EMBEDED;

	if($opts & _WHATA_BLOCK)
		$opts ^= _WHATA_BLOCK;

	$tmp = '';
	$save_i = $i;
	$j = $i+2;
	while($j + 1 < $len)
	{
		if($s[$j] === $closing_c && $s[$j+1] === $closing_c)
		{
			$tmp_i = 0;
			if($parse)
				$tmp = whata2html_recur($tmp, $tmp_i, strlen($tmp),$conf, $opts, $vals, $openedParagr);
			$i = $j+1;
			return $tmp;
		}
		else $tmp .= $s[$j];

		++$j;
	}
	$i = $save_i;
	return false;
}

function whata_list_char($c)
{
	return $c === '#' || $c === '*' || $c === '-';
}

function count_new_spaces(&$s,$i)
{
	$i_before = $i;

	while($i_before && is_blank($s[$i_before-1], false))
		--$i_before;

	return $i - $i_before;
}

function whata_handle_list(&$r,&$s,&$i,$len,$conf,$space_count,$vals,$list_in_list=false, &$must_clear = false)
{
	$list_character = $s[$i];

	if($list_character === '#')
		$r .= '<ol'; // ordered list
	else
		$r .= '<ul'; // unordered list

	if($vals['list-style'] === 'auto')
	{
		if(!empty($vals['list-class']))
			$r .= ' class="' . $vals['list-class'] . '"';
	}
	else
	{
		$r .= ' class="whata-list-' . $vals['list-style'] .
		      ($must_clear?' whata-clear':'') .
		      (empty($vals['list-class'])?'':' ' . $vals['list-class']) .
		      '"';
	}

	$vals['list-style']=$vals['list-class']='';
	$must_clear = false;
	$r .= '>';

/** for whata simplex:
	if($list_character === '#')
		$r .= '<ol>';
	else
		$r .= '<ul>';
*/

	$li_opened = 0;

	$new_space_count = $space_count;
	do
	{
		if($new_space_count > $space_count)
			$new_space_count = whata_handle_list($r,$s,$i,$len,$conf,$new_space_count,$vals,true);

		if($new_space_count < $space_count)
		{
			if($list_in_list || $new_space_count === -1 || $i >=$len || $s[$i] !== $list_character)
			{
				if($list_character === '#')
				{
					$r .= '</li></ol>'; // ordered list
				}
				else
				{
					$r .= '</li></ul>'; // unordered list
				}
				return $new_space_count;
			}
			else
				$r.= '</li>';
		}
		else
		{
			if($li_opened)
				$r.= '</li>';
			if($s[$i] !== $list_character)
			{
				if($list_character === '#')
					$r .= '</ol>'; // ordered list
				else
					$r .= '</ul>'; // unordered list
				return $new_space_count;
			}
			$space_count = $new_space_count;
		}

		++$i;

		$openedParagr = false;

		$r .= '<li>' . whata2HTML_recur ($s,$i,$len,$conf,
			($conf['whataSimplex']?0:_WHATA_BLOCK) |
				_WHATA_STOP_AFTER_NL,
			$vals,$openedParagr);
			// in Whata Simplex, we consider list as inline elements (block completely useless)

		$li_opened = 1;
		$inContentList = false; // We didn't parse any extra bullet content
		do
		{
			$countNL = 0;

			while($i < $len && $s[$i] === "\n")
			{
				++$i;
				++$countNL;
				skipSpace($s, $i, $len, false);
			}

			if($i < $len)
			{
				$new_space_count = count_new_spaces($s, $i);
				$listContinues  = whata_list_char($s[$i]);

				$newListContent = // there is still new content if
					(
						! $listContinues // we are not getting a new bullet
						&& $new_space_count > $space_count // and the content is indented compared to the bullet
					);
			}

			if( // We stop parsing the list if :
			    $i >= $len // no more char in buffer
			||  (
				$countNL !== 1 // or too many \n for another bullet
				&& !( // and we are not parsing new bullet content :
				    $inContentList // We didn't start parsing this content
				    && $newListContent // and there is not new content anymore
			        )
			    )
			)
			{
				if($list_character === '#')
					$r .= '</li></ol>';
				else
					$r .= '</li></ul>';

				return -1;
			}

			if($newListContent)
			{
				if(!$inContentList || $countNL > 1)
				{
					$inContentList = true;
					closeParagr($openedParagr, $r);
				}
				elseif($inContentList && $openedParagr)
				{
					$r .= '<br />';
				}

				$r .= whata2HTML_recur ($s,$i,$len,$conf,_WHATA_CAN_AUTO_OPEN_PARAGRAPHS | _WHATA_BLOCK | _WHATA_STOP_AFTER_NL | _WHATA_EMBEDED,$vals,$openedParagr); // We let Whata create Paragraphes even in Simplex Mode.
			}
		}
		while($newListContent);
	}
	while($i < $len && $listContinues);

	if($list_character === '#')
		$r .= '</li></ol>';
	else
		$r .= '</li></ul>';
	return $new_space_count;
}

function whata_get_attribute($s, &$i, $len, $explicit_attr_list = false)
{
	$attr = '';
	$opened = 0;
	skipSpace($s,$i,$len);

	if($s[$i] === '|') ++$i;

	skipSpace($s,$i,$len);

	if($i < $len && letter($s[$i]))
	{ // named attribute ?
		while($i < $len && letter_or_figure($s[$i]))
		{
			$attr .= $s[$i];
			++$i;
		}
		if($i < $len && $s[$i] === WHATA_ATTR_VALUE)
		{ // named attribute, yes.
			$value='';++$i;
			while($i < $len && !is_blank($s[$i]) && $s[$i] !== WHATA_ATTR_SEP && !($s[$i] === WHATA_ATTR_END && $explicit_attr_list))
			{
				if($s[$i] === WHATA_ESCAPE_CHAR)
					++$i;

				$value .= $s[$i];
				++$i;
			}
			skipSpace($s,$i,$len);

			if($s[$i] === WHATA_ATTR_SEP)
				++$i;

			return array($attr, $value);
		}
	}

	while($i < $len)
	{
		if($s[$i] === WHATA_ESCAPE_CHAR)
		{
			$attr.=WHATA_ESCAPE_CHAR;
			if($i+1 < $len)
			{
				++$i;
				$attr.=$s[$i];
			}
		}
		elseif($s[$i] === WHATA_TAG_END)
		{
			if($opened)
			{
				$attr .= $s[$i];
				--$opened;
			}
			else return $attr;
		}
		elseif($s[$i] === WHATA_ATTR_SEP)
		{
			return $attr;
		}
		elseif($s[$i] === WHATA_TAG_START)
		{
			++$opened;
			$attr .= $s[$i];
		}
		elseif($explicit_attr_list && $s[$i] === WHATA_ATTR_END)
			return $attr;
		else
			$attr .= $s[$i];
		++$i;
	}
	return $attr;
}

function letter($c)
{
	$n = ord($c);
	return ($n > 64 && $n < 91) || ($n > 96 && $n < 123);
}

function letter_or_figure($c)
{
	$n = ord($c);
	return ($n > 64 && $n < 91) || ($n > 96 && $n < 123) || ($n > 47 && $n < 58);
}

function whata_parse_cdata($s,&$i,$len, $raw=false)
{
	$j=0;
	while($i + $j < $len && $s[$i + $j] === WHATA_CDATA_START)
		++$j;

	if($j > 2)
	{
		$tmp = '';
		$tmp_i = $i + $j;

		while($tmp_i < $len)
		{
			if($s[$tmp_i] === WHATA_CDATA_END)
			{
				$k=0;
				while($k < $j && $i + $k < $len && $s[$tmp_i + $k] === WHATA_CDATA_END)
					++$k;

				if($k === $j)
				{
					$i = $tmp_i + $k;

					if($raw)
						return $tmp;

					return str_replace("\n", '<br />', htmlspecialchars($tmp));
				}
			}

			$tmp .= $s[$tmp_i];
			++$tmp_i;
		}
	}
	return '';
}

function whata_get_tag_content($s,&$i,$len)
{ //$s[$i-1] === WHATA_TAG_START
	$r = array(
		'name'    => '',
		'attr'    => array(),
		'content' => '',
		'value'   => ''
	);
	$j=false;

	if($s[$i] === WHATA_ATTR_START) // support for attributes
	{
		$attrs = true;
		$explicit_attr_list = true; // the attribute list ends with WHATA_ATTR_END
		++$i;
	}
	else
	{
		$attrs = false;
		$explicit_attr_list = false;
	}

	$attrs_max = 0; // maximun attribute count (default = unlimited)

	do
	{
		$r['name'] .= $s[$i];
		++$i;
		if(!$j)
		{
			$j=true;
			if(!letter($s[$i-1]) && $s[$i-1] !== '*') // let the user write e.g. [[http://www.perdu.com] instead of [[ http://www.perdu.com] for links
				break;
		}
	}
	while($i < $len && $s[$i] !== '=' && $s[$i] !== WHATA_TAG_END && $s[$i] !== WHATA_ATTR_SEP && !is_blank($s[$i]));

	$matter_about_tags = true; // take care of tags while parsing contents

	if($r['name'] !== 'Math')
		$r['name'] = strtolower($r['name']);

	if($r['name'] === WHATA_TAG_START) // links, turn on attributes
	{
		$attrs = true;
		$attrs_max = 1; // only one attribute
		$can_double_end_tag = true; // We let the user write [[http://perdu.com]] or [[http://perdu.com] (if WHATA_TAG_START & WHATA_TAG_END = '[' & ']')
	}
	else
	{
		$can_double_end_tag = false;

		if($r['name'] === 'img') // img too
		{
			$attrs = true;
			$attrs_max = 2; // only one attribute
		}
		else if($r['name'] === 'code')
			$matter_about_tags = false;
		else if (
		   $r['name'] === 'div'
		|| $r['name'] === 'span'
		|| $r['name'] === 'p'
		|| ( // hX
		   	   $r['name'][0] === 'h'
		   	&& intval($r['name'][1])
		))
		{ //We enable attributes by default for "close-to-html" tags like p, hX, span, div.
			$attrs = true;
		}
	}


	if($i<$len && $s[$i] === '=')
	{ // the tag's value
		++$i;
		while($i<$len && !is_blank($s[$i]) && $s[$i] !== WHATA_TAG_END && ($s[$i] !== WHATA_ATTR_END || !$explicit_attr_list))
		{
			$r['value'] .= $s[$i];
			if($s[$i] === WHATA_ESCAPE_CHAR && $i+1 < $len)
			{
				++$i;
				$r['value'] .= $s[$i];
			}
			++$i;
		}
	}

	if($i < $len && $s[$i] === WHATA_TAG_END)
	{
		if($can_double_end_tag && $i+1 < $len && $s[$i+1] === WHATA_TAG_END)
			++$i;
		return $r;
	}
	$count_attr = 0;

	if($attrs)
	{ // complex tag with attributes
		$namedAttr = false;
		do
		{
			/** Getting the value of the attribute */
			$attr = whata_get_attribute($s, $i, $len, $explicit_attr_list);

			if($i < $len && ($s[$i] === WHATA_ATTR_SEP || is_array($attr) || ($explicit_attr_list && $s[$i] === WHATA_ATTR_END))) /* There are still attributes to parse or attribute is not the tag's content */
			{
/*				// named attribute
				if(preg_match('/^([a-zA-Z]+)' . WHATA_ATTR_VALUE . '([\s\S]+)$/', $attr, $matches))
					$r['attr'][$matches[1]] = $matches[2];
*/
				if(is_array($attr))
				{// named attributes
					$r['attr'][$attr[0]] = $attr[1];

					if(!($s[$i] === WHATA_ATTR_END && $explicit_attr_list))
						$namedAttr = true;
				}
				else // unamed attribute
				{
					++$count_attr;
/* We only count unamed attributes since they are the only attributes to be limited.
   This behavior enable the user to add a class to a link or an img, for example. */
					$r['attr'][] = trim($attr);
				}

				if($s[$i] === WHATA_ATTR_SEP)
					++$i;
			}
			else
				$r['content'] = trim($attr); // the attribute is tag's content
		}
		while(
			$i < $len // still data to read
			&& ($s[$i] === WHATA_ATTR_SEP || ($namedAttr && !($namedAttr = false)))// we are not end of tag ($s[$i] === WHATA_TAG_END)
			&& (
				!$attrs_max // unlimited number of attrs
				|| $count_attr < $attrs_max // or limit not reached
			)
		);
		if($explicit_attr_list && $s[$i] === WHATA_ATTR_END)
			++$i;
	}

	if($i < $len)
	{
		if($s[$i] === WHATA_TAG_END) // end of tag
		{
			if($can_double_end_tag && $i+1 < $len && $s[$i+1] === WHATA_TAG_END)
				++$i;
			return $r;
		}


	}
	$tags_to_be_closed = 0;

	while($i < $len)
	{
		if($s[$i] === WHATA_CDATA_START && ($tmp = whata_parse_cdata($s,$i,$len, $r['name'] === 'code')))
			$r['content'] = $r['content'] . $tmp;
		elseif($s[$i] === WHATA_ESCAPE_CHAR && $i + 1 < $len)
		{// escape
			++$i;
			$r['content'] = $r['content'] . WHATA_ESCAPE_CHAR . $s[$i];
		}
		else if($s[$i] === WHATA_TAG_START && $matter_about_tags)
		{// new tag if $matter_about_tags
			$r['content'] = $r['content'] . WHATA_TAG_START;
			++$tags_to_be_closed;
		}
		else if($s[$i] === WHATA_TAG_END)
		{//closing opened tag. if !$tags_to_be_closed, end of tag
			if($tags_to_be_closed)
			{
				$r['content'] = $r['content'] . $s[$i];
				--$tags_to_be_closed;
			}
			else
			{
				if($can_double_end_tag && $i+1 < $len && $s[$i+1] === WHATA_TAG_END)
					++$i;
				return $r;
			}
		}
		else $r['content'] .= $s[$i]; // text
		++$i;
	}
	// End of tag
	// FIXME : if $s[$i] !== WHATA_TAG_END, there is a syntax error.

	if($can_double_end_tag && $i+1 < $len && $s[$i+1] === WHATA_TAG_END)
		++$i;
	return $r;
}

function whata_tag(&$must_clear,$name,&$tag,&$conf,&$openedParagr,$vals,$opts=0,$attr=array())
{
	$s_attr='';

	if($tag['name'] === 'float' && $must_clear)
	{
		$clear = '<br class="whata-clear" />';
		$must_clear = false;
	}
	else
	{
		$clear = '';
		if($must_clear)
		{
			if(isset($attr['class']))
				$attr['class'] .= ' whata-clear';
			else
				$attr['class'] = 'whata-clear';
		}
	}

	if(isset($tag['attr']['class']))
	{
		if(isset($attr['class']))
			$attr['class'] = $attr['class'] . ' ' . $tag['attr']['class'] . ($must_clear?' whata-clear':'');
		else
			$attr['class'] = $tag['attr']['class'] . ($must_clear?' whata-clear':'');
	}
	if(isset($tag['attr']['color']))
	{
		if(isset($attr['style']))
			$attr['style'] = $attr['style'] . ';color:' . $tag['attr']['color'];
		else
			$attr['style'] = 'color:' . $tag['attr']['color'];
	}
	if($opts & _WHATA_BLOCK && isset($tag['attr']['align']))
	{
		if(isset($attr['style']))
			$attr['style'] = $attr['style'] . ';text-align:' . $tag['attr']['align'];
		else
			$attr['style'] = 'text-align:' . $tag['attr']['align'];
	}

	if(isset($tag['attr']['width']))
	{
		if($name === 'img')
			$attr['width'] = $tag['attr']['width'];
		elseif(isset($attr['style']))
			$attr['style'] = $attr['style'] . ';width:' . $tag['attr']['width'];
		else
			$attr['style'] = 'width:' . $tag['attr']['width'];
	}

	if(isset($tag['attr']['height']))
	{
		if($name === 'img')
			$attr['height'] = $tag['attr']['height'];
		elseif(isset($attr['style']))
			$attr['style'] = $attr['style'] . ';height:' . $tag['attr']['height'];
		else
			$attr['style'] = 'height:' . $tag['attr']['height'];
	}

	$must_clear = false;

	foreach($attr as $key => $value)
	{
		if(is_string($key))
			$s_attr .= ' ' . htmlspecialchars($key) . '="' . htmlspecialchars($value) . '"';
	}

	if(isset($attr[0]))
		return '<' . $name . $s_attr . ' />';
	else
	{
		if(isset($tag['text']))
			$content = $tag['text'];
		else
		{
			$i=0;
			$content = whata2html_recur($tag['content'],$i,strlen($tag['content']), $conf,$opts | ($opts & _WHATA_BLOCK?0:_WHATA_EMBEDED),$vals, $openedParagr);
		}

		return $clear . '<' . $name . $s_attr . '>' . trim($content) . '</' . $name . '>';
	}
}

function whata_unescape($s)
{
	return str_replace(WHATA_ESCAPE_CHAR . WHATA_TAG_END, WHATA_TAG_END, $s);
}

function _whata_strip_escape($s)
{
	return str_replace(WHATA_ESCAPE_CHAR , '',$s);
}

function skipSpace(&$s, &$i, &$len,$includeNL = true)
{
	while($i < $len && is_blank($s[$i], $includeNL))
		++$i;
}

function openParagr(&$openedParagr,$bloc,&$r, &$must_clear, &$pendingNL)
{
	if(!$openedParagr && $bloc)
	{
		if($must_clear)
		{
			$r .= '<p class="whata-clear">';
			$must_clear = false;
		}
		else
			$r .= '<p>';
		$openedParagr = true;
	}
	elseif($must_clear)
	{
		$r .= '<br class="whata-clear" />';
		$must_clear = false;
	}
	elseif($pendingNL)
	{
		if($must_clear)
		{
			$must_clear = false;
			$r .= '<br class="whata-clear" />';
		}
		else
			$r.= '<br />';
	}
	$pendingNL = false;
}

function closeParagr(&$openedParagr, &$r)
{
	if($openedParagr)
		$r .= '</p>';

	$openedParagr = false;
}

function whata_link(&$url, &$content)
{
	$e_url = explode(' ', $url, 2);
	$e_url_0 = explode(':', $e_url[0], 2);

	if($e_url_0[0] === 'wp' || $e_url_0[0] === 'wiki')
	{ // Wikipedia
		if(isset($e_url_0[1]))
			$lang = strtolower($e_url_0[1]);
		else
			$lang = $conf['lang'];
		if(isset($e_url[1]))
			$link = $e_url[1];
		else // FIXME:syntax error
			$link = '';
		$url = 'http://' . $lang . '.wikipedia.org/wiki/' . rawurlencode($link);
	}
	elseif ($e_url_0[0] === 'php')
	{
		if(isset($e_url[1]))
			$link = $e_url[1];
		else // FIXME:syntax error
			$link = '';
		$url = 'http://php.net/' . rawurlencode($link);
	}
	if(empty($content))
		$content = isset($link)?$link:$url;
}

function test_whata_simplex($s,&$i,$len,&$r,$autoOpenParagr,&$openedParagr,&$must_clear,&$pendingNL, $vals)
{
	if($i+2 < $len && $s[$i] === '*' && $s[$i + 1] === '*')
	{ // bold for whata Simplex
		$tmp = whata_simplex_tag('*', $s,$i,$len, $opts, $conf, $vals);
		if($tmp !== false)
		{
			openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
			if($conf['simplex_semantic'])
				$r .= '<strong>' . $tmp . '</strong>';
			else
				$r .= '<b>' . $tmp . '</b>';
		}
		return $tmp !== false;
	}
	elseif($i+2 < $len && $s[$i] === "'" && $s[$i + 1] === "'")
	{ // italic for whata Simplex
		$tmp = whata_simplex_tag("'", $s,$i,$len, $opts, $conf, $vals);
		if($tmp !== false)
		{
			openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
			if($conf['simplex_semantic'])
				$r .= '<em>' . $tmp . '</em>';
			else
				$r .= '<i>' . $tmp . '</i>';
		}
		return $tmp !== false;
	}
	elseif($i+2 < $len && $s[$i] === '-' && $s[$i + 1] === '-')
	{ // strike for whata Simplex
		$tmp = whata_simplex_tag('-', $s,$i,$len, $opts, $conf, $vals);
		if($tmp !== false)
		{
			openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
			$r .= '<strike>' . $tmp . '</strike>';
		}
		return $tmp !== false;
	}
	elseif($i+2 < $len && $s[$i] === '_' && $s[$i + 1] === '_')
	{ // underline for whata Simplex
		$tmp = whata_simplex_tag('_', $s,$i,$len, $opts, $conf, $vals);
		if($tmp !== false)
		{
			openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
			$r .= '<span style="text-decoration:underline">' . $tmp . '</span>';
		}
		return $tmp !== false;
	}
	else return false;
}


function whata2HTML_recur($s,&$i,$len,&$conf,$opts,$vals,&$openedParagr=false, &$must_clear = false)
{// this function assumes $s to have the UNIX format ("\n" for new lines). the  whata2HTML function do the conversion if this was not the case
	global $whata_global_conf;
	$block = $opts & _WHATA_BLOCK; // We are in a block element

	$embeded = $opts & _WHATA_EMBEDED; // prevent paragraphs from being automatically opened and closed when leaving the function (whata2HTML_recur was called from whata2HTML_recur)

	if($opts & _WHATA_STOP_AFTER_NL) // We must stop parsing when a new line is reached
	{
		$opts ^= _WHATA_STOP_AFTER_NL;
		$stop_after_nl = true;
	}
	else
		$stop_after_nl = false;

	if(!isset($vals['list-style']))
		$vals['list-style'] = 'auto';

	$pre = $opts & _WHATA_PRE; // We are in a pre element

	if($opts & _WHATA_STOP_AFTER_PIPE) // We must stop after the next "|" (if not in a tag or escaped)
	{
		$opts ^= _WHATA_STOP_AFTER_PIPE;
		$stop_after_pipe = true;
	}
	else
		$stop_after_pipe = false;
		

	$r = '';

	$NL = ($opts & _WHATA_CAN_AUTO_OPEN_PARAGRAPHS) || !$stop_after_nl; // we consider a new ligne if we don't stop after a new line. Otherwise, yes.

	$autoOpenParagr = $block && (($opts & _WHATA_CAN_AUTO_OPEN_PARAGRAPHS) || !$embeded);

	skipSpace($s,$i,$len);

	while($i < $len)
	{
		if(test_whata_simplex($s,$i,$len,$r,$autoOpenParagr,$openedParagr,$must_clear,$pendingNL,$vals))
		{
			if($NL)
				$NL = false;
		}
		else
		{
			$c = $s[$i];
			if($NL || $c === "\n")
			{
	
				if($pre && $c === "\n")
					$r .="\n";
				elseif($stop_after_nl && $c === "\n")
				{
	
					if($must_clear)
						$r.='<br class="whata-clear" />';
	
					if(!$embeded)
						closeParagr($openedParagr, $r);
					return $r;
				}
				else
				{
					$countNL = 0;
					while($i < $len && $s[$i] === "\n")
					{
						++$i;
						++$countNL;
						skipSpace($s, $i, $len, false);
					}

					if($i < $len)
					{
						$NL = false;

						if(test_whata_simplex($s,$i,$len,$r,$autoOpenParagr,$openedParagr,$must_clear,$pendingNL,$vals))
							$countNL = 0;
						elseif($block && whata_list_char($s[$i]))
						{//lists
							closeParagr($openedParagr, $r);

							/* How many space are before the list item ? */
							$i_before = $i;
							while($i_before && is_blank($s[$i_before-1], false))
								{--$i_before;}

							whata_handle_list($r,$s,$i,$len,$conf,$i - $i_before,$vals, false,$must_clear);

							$NL = true;
	
							$must_clear = false;
	
							$countNL = 0;
						}
						elseif($block && $s[$i] === '=')
						{// title
							closeParagr($openedParagr, $r);

							$lev=0;
							while($s[$i] === '=')
							{
								++$i;
								++$lev;
							}

							// j is the title's level

							$j = $conf['min_title_level'] - 1 + $lev;
							if($j > 6)
								$j = 6;
							elseif($j < 1)
								$j = 1;
	
							if($must_clear)
							{
								$must_clear = false;
								$lev .= ' whata-clear';
							}
	
							$r .= '<h' . $j . ' class="whata-h' . $lev . '">' . whata2HTML_recur($s,$i,$len,$conf, _WHATA_STOP_AFTER_NL,$vals, $openedParagr) . '</h' . $j . '>';
							$NL = true;
							$countNL = 0;
						}

						if($countNL)
						{// new Paragraph or new line
							if($openedParagr || !$autoOpenParagr)
							{
								if($countNL === 1 || !$autoOpenParagr) // new line
									$pendingNL = true;
								else //new Paragraph
									closeParagr($openedParagr, $r);
							}
						}

						skipSpace($s,$i,$len);

						//if title escaped, don't go back (for Whata Simplex!)
						if($i >= $len || !($s[$i] === WHATA_ESCAPE_CHAR && $i+1 < $len && $s[$i+1] === '='))
							--$i;
					}
				}
			}
	
			elseif($c === WHATA_CDATA_START && $tmp = whata_parse_cdata($s,$i,$len))
				$r .= $tmp;

			elseif($c === WHATA_ESCAPE_CHAR) // ne pas mettre après le test de NL ou \n
			{ // Échappement
				++$i;
				openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
				if($i < $len)
				{
					if($s[$i] === 'n')
						$r .= '<br />';
					else
						$r .= $s[$i];
				}
				else $r.=WHATA_ESCAPE_CHAR;
			}
			elseif(!$conf['whataSimplex'] && $c === WHATA_TAG_START && $i+1 < $len)
			{ // balise
				if(is_blank($s[++$i])) // a void tag let you building lists with multiple lines or even paragraphs
				{
					++$i;
					$r .= whata2HTML_recur($s,$i, $len, $conf, _WHATA_EMBEDED | ($opts & _WHATA_BLOCK),$vals,$openedParagr);
					--$i;
				}
				else
				{
					$tag = whata_get_tag_content($s,$i,$len);
					$name = $tag['name'];
					if($name === 'i')
					{
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						$r .= whata_tag($must_clear, 'i', $tag, $conf,$openedParagr,$vals);
					}
					elseif($name === 'b')
					{
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						$r .= whata_tag($must_clear, 'b', $tag, $conf,$openedParagr,$vals);
					}
					elseif($name === '*')
					{ // em
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						$r .= whata_tag($must_clear, 'em', $tag, $conf,$openedParagr,$vals);
					}
					elseif($name === '**') // strong
					{
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						$r .= whata_tag($must_clear, 'strong', $tag, $conf,$openedParagr,$vals);
					}
					elseif($name === '+')
					{
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						$r .= whata_tag($must_clear, 'ins', $tag, $conf,$openedParagr,$vals);
					}
					elseif($name === '-')
					{
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						$r .= whata_tag($must_clear, 'del', $tag, $conf,$openedParagr,$vals);
					}
					else if($name === '--')
					{
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						$r .= whata_tag($must_clear, 'strike', $tag, $conf,$openedParagr,$vals);
					}
					elseif($name === 'u')
					{
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						$r .= whata_tag($must_clear, 'span', $tag, $conf,$openedParagr,$vals,0,array('style'=>'text-decoration:underline'));
					}
					elseif($name === '_')
					{ //sub
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						$r .= whata_tag($must_clear, 'sub', $tag, $conf,$openedParagr,$vals);
					}
					elseif($name === '^')
					{ // sup
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						$r .= whata_tag($must_clear, 'sup', $tag, $conf,$openedParagr,$vals);
					}

					elseif($name === 'code')
					{
						closeParagr($openedParagr, $r);

						if(empty($tag['value']))
						{
							$tag['text'] = $tag['content'];
							$r .= whata_tag($must_clear, 'code', $tag,$conf,$openedParagr,$vals,_WHATA_PRE, array('class'=>'whata-code'));
						}
						else
						{	$type = $tag['value'];
							if($type==='html')
								$type='html4strict';

							$j=0;
							$clen = strlen($content = rtrim($tag['content']));
							while($j < $clen && is_blank($content[$j]))
							{
								if($content[$j] === "\n")
								{
									++$j;
									break;
								}
								++$j;
							}
							$geshi = new GeSHi(whata_unescape(substr($content,$j)), $type);
							$geshi->enable_classes();
	//						$geshi->enable_strict_mode(GESHI_MAYBE); // NOTICE ??
							$geshi->set_header_type(GESHI_HEADER_PRE_TABLE);
							$geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS);
							$tag['text'] = $geshi->parse_code();
							$r .= whata_tag($must_clear, 'div', $tag,$conf,$openedParagr,$vals,_WHATA_BLOCK, array('class'=>'whata-code'));
						}

					}
					elseif($name === 'list')
					{
						$j=0;
						$newvals = $vals;
						if(!empty($tag['value']))
							$newvals['list-style'] = htmlspecialchars($tag['value']);
						if(!empty($tag['attr']['class']))
							$newvals['list-class'] = $tag['attr']['class'];

						$r .=  whata2html_recur($tag['content'], $j, strlen($tag['content']), $conf, $opts,$newvals,$openedParagr, $must_clear);
					}
					elseif($name === 'output')
					{ // Output of a program, not <pre> formated
						closeParagr($openedParagr, $r);
						$r .= whata_tag($must_clear, 'blockquote', $tag,$conf,$openedParagr,$vals,_WHATA_BLOCK, array('class'=>'whata-output'));
					}
					elseif($name === 'out')
					{ // Output of a program, <pre> formated
						closeParagr($openedParagr, $r);
						$r .= whata_tag($must_clear, 'pre', $tag, $conf,$openedParagr, $vals, _WHATA_PRE, array('class'=>'whata-out'));
					}
					elseif($name === 'pre')
					{ //<pre> formated
						closeParagr($openedParagr, $r);
						$r .= whata_tag($must_clear, 'pre', $tag, $conf,$openedParagr, $vals, _WHATA_PRE);
					}
					elseif($name === 'quote')
					{
						closeParagr($openedParagr, $r);

						$tag['text'] = '';

						$source_header_show = false;
						if(empty($tag['value'])) // url
							$cite = '';
						else
						{
							$url = $tag['value'];
							$cite = ' cite="' . htmlspecialchars($url) . '"';

							if($whata_global_conf['SHOW_QUOTE_HEADER'] && $whata_global_conf['QUOTE_HEADER_BEFORE'])
							{
								$tag['text'] = $tag['text'] .
									       '<p class="whata-quote-header"> ' .
									       $whata_global_conf['SOURCE_LABEL'] .
									       '<a href="' . rawurlencode($url) . '">' . htmlspecialchars($url) . '</a></p>';
							}
							else $source_header_show = true;
						}

						$undefined = array();

						$tag['text'] = $tag['text'] .
							       '<blockquote' . $cite . '>' .
							       whata2HTML($tag['content'], $conf,$undefined, _WHATA_BLOCK,$vals) .
							       '</blockquote>';

						if($source_header_show)
						{
							$tag['text'] = $tag['text'] .
								       '<p class="whata-quote-header"> ' .
								       $whata_global_conf['SOURCE_LABEL'] .
								       '<a href="' . rawurlencode($url) . '">' . htmlspecialchars($url) . '</a></p>';
						}

						$r .= whata_tag($must_clear, 'div', $tag, $conf,$openedParagr,$vals, _WHATA_BLOCK, array('class'=>'whata-quote'));
					}
					elseif($name === 'q')
					{
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						if(empty($tag['value'])) // url
							$url = array();
						else
							$url = array('cite'=>$tag['value']);

						$r .= whata_tag($must_clear, 'q', $tag, $conf,$openedParagr, $vals, 0, $url);
					}
					elseif($name === 'whata')
					{
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						$tag['text'] = 'Whata!';
						$r .= whata_tag($must_clear, 'em', $tag,$conf,$openedParagr,$vals);
					}
					else if($name === 'math' || $name === 'Math')
					{
						
						$j=$i;
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						$tag_to_append=$whata_global_conf['MATH_FUNCTION'](trim(_whata_strip_escape($tag['content'])), $name === 'Math');
						if($tag_to_append['autoclose'])
							$tag_to_append['attr'][0] = true;
						$tag['text'] = $tag_to_append['content'];
						$r .= whata_tag($mustr_clear, $tag_to_append['name'], $tag,$conf,$openedParagr,$vals, 0, $tag_to_append['attr']);
					}
					else if($name === 'img')
					{
	//					openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						if(!empty($tag['content']))
						{
							$alt = trim($tag['content']);
							$attr = array('title'=>$alt, 'alt'=>$alt, 'src'=>$conf['image_prefix'] . $tag['value'],true);
						}
						else $attr=array('alt'=>'','src'=>$conf['image_prefix'] . $tag['value'],true);

						$r .= whata_tag($must_clear, 'img', $tag,$conf,$openedParagr,$vals,0, $attr);
					}
					elseif($name === WHATA_TAG_START)
					{//lien
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);

						if(isset($tag['attr'][0]))
						{
							$url = $tag['attr'][0];
							whata_link($url, $tag['content']);
						}
						else
						{
							$url = $tag['content'];
							$tag['text'] = '';
							whata_link($url, $tag['text']);
						}

						$r .= whata_tag($must_clear, 'a', $tag,$conf,$openedParagr,$vals,0,array('href'=>$url));
					}
					elseif($name === 'table')
					{
						$alignmeaning = array('auto','left','center','right','justify');
						closeParagr($openedParagr, $r);

						$j = 0; $tlen = strlen($t = trim($tag['content']));
						$ntc = '<tr>'; // New Tag Content
						$closed_tr = false; // We have to close the current <tr>
						while($j < $tlen)
						{
							$th = false; // the cell being parsed is an heading cell
							$colspan = 0; // cell fusion (horizontally)
							$align = 0; // 0:auto, 1:left, 2:center, 3:right, 4:justify
							while($j < $tlen && $t[$j] === '|')
							{
								++$j;
								++$colspan;
							}
							skipSpace($t,$j,$tlen);
							if($j < $tlen)
							{
								if($t[$j] === '<')
								{
									if($j+1 < $tlen && $t[++$j] === '>')
									{// < : left
										$align = 4;
										++$j;
									}
									else
									{ // <> : justify
										$align = 1;
									}
								}
								elseif($t[$j] === '>')
								{
									if($j+1 < $tlen && $t[++$j] === '<')
									{// >< : center
										$align = 2;
										++$j;
									}
									else
									{// > : right
										$align = 3;
									}
								}
								if($t[$j] === '=')
								{ // We parse an heading cell
									$ntc .='<th';
									$cellEnd = '</th>';
									++$j;
								}
								else
								{
									$cellEnd = '</td>';
									$ntc .='<td';
								}

								if($colspan > 1)
									$ntc .= ' colspan="' . $colspan . '"';
								if($align)
									$ntc .= ' class="whata-align-' . $alignmeaning[$align] . '"';

								$ntc .= '>' . trim(whata2html_recur($t, $j, $tlen, $conf, _WHATA_EMBEDED | _WHATA_STOP_AFTER_PIPE,$vals,$openedParagr)) . $cellEnd;
								skipSpace($t,$j,$tlen,false);

								if($j < $tlen)
								{
									if($t[$j] === "\n")
									{//new row
										$ntc .='</tr>';
										skipSpace($t,$j,$tlen);
										if($j < $tlen)
										{
											$ntc .= '<tr>';
											$closed_tr = false;
										}
										else $closed_tr = true;
									}
									elseif($t[$j] === '|')
										--$j; //empty cell, let parse it
								}
							}
						}
						if($closed_tr)
							$tag['text'] = $ntc;
						else
							$tag['text'] = $ntc . '</tr>';


						if(!empty($tag['attr']['align']))
							$attr = array
							(
								'class'=>'whata-align-' . $tag['attr']['align'] .
									 ' whata-table'
							);
						else
							$attr = array
							(
								'class'=> 'whata-table'
							);

						$r .= whata_tag($must_clear, 'table', $tag,$conf,$openedParagr,$vals,_WHATA_BLOCK, $attr);
					}
					elseif($name === 'div' || $name === 'float')
					{ // Take care ! div is NOT an equivalent to float ! div has attributes enabled by default.
						closeParagr($openedParagr, $r);
						if(empty($tag['value']))
							$attr = array('class'=>'whata-div');
						else
							$attr = array('class'=>'whata-div whata-float-' . htmlspecialchars($tag['value']));

						$r .= whata_tag($must_clear, 'div', $tag,$conf,$openedParagr,$vals,_WHATA_BLOCK, $attr);
					}
					elseif($name === 'p')
					{
						closeParagr($openedParagr, $r);
						$r .= whata_tag($must_clear, 'p', $tag,$conf,$openedParagr,$vals);
					}
					elseif($name === 'span')
					{
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						if(empty($tag['value']))
							$attr = array();
						else
							$attr = array('style'=>'display:inline-block;min-width:' . htmlspecialchars($tag['value'])); //FIXME danger !
						$r .= whata_tag($must_clear, 'span',$tag,$conf,$openedParagr,$vals,0,$attr);
					}
					elseif($name === 'color')
					{
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						if(empty($tag['value']))
							$attr = array();
						else
							$attr = array('style'=>'color:' . htmlspecialchars($tag['value'])); //FIXME danger !
						$r .= whata_tag($must_clear, 'span',$tag,$conf,$openedParagr,$vals,0,$attr);
					}
					elseif($name === 'align')
					{
						closeParagr($openedParagr, $r);
						if(empty($tag['value']))
							$attr = array();
						else
							$attr = array('style'=>'text-align:' . htmlspecialchars($tag['value'])); //FIXME danger !
						$r .= whata_tag($must_clear, 'div',$tag,$conf,$openedParagr,$vals,_WHATA_BLOCK,$attr);
					}
					elseif($name === 'clear')
					{
						closeParagr($openedParagr, $r);
						$must_clear = true;
					}
					elseif($name[0] === 'h' && ($level = intval($name[1])))
					{// title
						closeParagr($openedParagr, $r);
						$lev = $conf['min_title_level'] - 1 + $level;
						if($lev > 6)
							$lev = 6;

						$r .= whata_tag($must_clear, 'h' . $lev, $tag,$conf,$openedParagr,$vals,0,array('class'=>'whata-h' . $level));
					}
	//				...
					else // Unknown tag
					{
						openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
						$j=0;
						$r .= whata2html_recur($tag['content'], $j, strlen($tag['content']),$conf,_WHATA_EMBEDED,$vals,$openedParagr);
					}
				}
			}
			elseif(!$conf['whataSimplex'] && (($c === WHATA_TAG_END && $embeded) || ($c === WHATA_ATTR_SEP && $stop_after_pipe)))
			{
				++$i;
				if($must_clear)
					$r.='<br class="whata-clear" />';
				return $r;
			}
	
			elseif($i+2 < $len && $c === '[' && $s[$i + 1] === '[')
			{ // links for whata Simplex
				$url = whata_simplex_tag(']', $s,$i,$len, $opts, $conf, $vals, false);

				if($url !== false)
				{
					$content = '';
					whata_link($url, $content);

					if(!preg_match('/^[a-zA-Z]+:/',$url))
						$url = 'http://' . $url;
					openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);
					$r .= '<a href="' . $url . '">' . htmlspecialchars($content) . '</a>';
				}
			}
			else
			{
				if(!is_blank($c))
					openParagr($openedParagr,$autoOpenParagr,$r,$must_clear,$pendingNL);

				$old_i = $i;
				do
				{
					++$i;
				}
				while($i < $len && !(
					($c = $s[$i]) === "\n"
					|| $c === WHATA_TAG_START
					|| $c === WHATA_TAG_END
					|| $c === WHATA_ATTR_SEP
					|| $c === WHATA_ESCAPE_CHAR
					|| $c === '*'
					|| $c === '_'
					|| $c === '"'
					|| $c === "'"
					|| $c === '`'
				));

				$r .= htmlspecialchars(substr($s, $old_i, ($i--) - $old_i));
			}
		}
		++$i;
	}
	if($must_clear)
		$r.='<br class="whata-clear" />';
	if(!$embeded)
		closeParagr($openedParagr, $r);

	return $r;
}

function whata2HTML($s, $conf=array(), &$set_array = array(), $opts=33,$vals=array())
{
	if(empty($s)) return '';

	global $whata_global_conf;
	$s = str_replace("\r", "\n", str_replace("\r\n", "\n", trim($s))); // To UNIX format, trim spaces.

	if(!isset($conf['lang']))
		$conf['lang'] = 'en';
	if(!isset($conf['min_title_level']))
		$conf['min_title_level'] = $whata_global_conf['MIN_TITLE_LEVEL'];
	if(!isset($conf['simplex_semantic']))
		$conf['simplex_semantic'] = $whata_global_conf['SIMPLEX_SEMANTIC'];

	if(isset($conf['inline']) && $conf['inline'] && ($opts & _WHATA_BLOCK))
		$opts ^= _WHATA_BLOCK;
	elseif(isset($conf['block']) && $conf['block'])
		$opts |= _WHATA_BLOCK;

	$i=0; $len = strlen($s);
	if(!isset($conf['image_prefix']))
		$conf['image_prefix'] = $whata_global_conf['IMAGE_PREFIX'];

	if($s[0] === WHATA_TAG_START && ++$i && whata_get_following_word($s,$i,$len) === 'set')
	{// the file specifies values to pass to the program asking for the whata to HTML conversion
		++$i;
		$continue = true;
		while($continue && $i < $len)
		{
			$key = '';
			while($i < $len)
			{
				if($s[$i] === WHATA_ESCAPE_CHAR && $i+1<$len)
					$key .= $s[++$i];
				elseif($s[$i] === WHATA_TAG_END)
				{
					$continue = false;
					break;
				}
				elseif($s[$i] === '=')
				{
					++$i;
					break;
				}
				else $key .= $s[$i];

				++$i;
			}
			if($continue)
			{
				$value = '';
				while($i < $len)
				{
					if($s[$i] === WHATA_ESCAPE_CHAR && $i+1<$len)
						$value .= $s[++$i];
					elseif($s[$i] === WHATA_TAG_END)
					{
						$continue = false;
						break;
					}
					elseif($s[$i] === ',')
					{
						++$i;
						break;
					}
					else $value .= $s[$i];

					++$i;
				}
				$set_array[trim($key)] = trim($value);
			}
		}
		++$i;
	}
	else $i=0;

	if(isset($set_array['whataSimplex']))
	{
		if(strtolower($set_array['whataSimplex']) === 'true' || intval($set_array['whataSimplex']))
			$conf['whataSimplex'] = true;
		else
			$conf['whataSimplex'] = false;
	}
	elseif(!isset($conf['whataSimplex']))
		$conf['whataSimplex'] = $whata_global_conf['WHATA_SIMPLEX'];

	$can_outer = $opts & _WHATA_CAN_OUTER;
	$block = $opts & _WHATA_BLOCK;

	return ($whata_global_conf['OUTER'] && $can_outer
	        ?
	        	($block
	        		? '<div'
	        		: '<span'
	        	) .
	        	' class="whata">'
	         : ''
	       ) .
	       whata2HTML_recur($s,$i,$len,$conf,_WHATA_BLOCK & $opts,$vals) .
	       (
	       	($whata_global_conf['OUTER'] && $can_outer)
	       	 ?
	       		($block
	       			? '</div>'
	       			: '</span>'
	       		)
	       	 : ''
		);
}

if(defined('DDBLOG_MIN_TITLE_LEVEL'))
{
	$ddBlogWhataConf = array (
		'lang'            => DDBLOG_LANGUAGE,
		'min_title_level' => DDBLOG_MIN_TITLE_LEVEL,
		'image_prefix'    => DDBLOG_PUB . '/imgs/'
	);

	function ddBlog_syntax2HTML($s, $tofile=false)
	{
		if($tofile)
			return file_put_contents($tofile, whata2HTML($s, $GLOBALS['ddBlogWhataConf']));
		return whata2HTML($s, $GLOBALS['ddBlogWhataConf']);
	}

	function ddBlog_file2HTML($f, $tofile=false)
	{
		if($tofile)
			return file_put_contents($tofile,whata2HTML(file_get_contents($f),$GLOBALS['ddBlogWhataConf']));
		return whata2HTML(file_get_contents($f),$GLOBALS['ddBlogWhataConf']);
	}
}
