<?php
// The Whata! syntax Parser
//FIXME : tag and spaces
include('whata.conf.php');
include('geshi.php');

if(WHATA_MATH_METHOD === 'url')
{
	function whataMath2HTML($code)
	{
		$code = htmlspecialchars($code);
		return '<img alt="' . $code . '" title="' . $code . '" src="' . WHATA_MATH_URL . $code . '" />'; 
	}
}
else include(WHATA_MATH_INCLUDE); // included file MUST have a whataMath2HTML function.

function isBlank($c)
{
	return $c === ' ' || $c === '	' || $c === "\n" || $c === "\r"	;
}

function whataParseGetWord($s,&$i,$end)
{
	$r = '';
	while($i < $end && preg_match('/[a-zA-Z]/',$s[$i]))
	{
		$r.= $s[$i];
		++$i;
	}
	return $r;
}
/*
function whata_get_attribute($s, &$i, $end, $reallyTheEnd=true) // or pipe.
{
/*	$r = '';
	while($i < $end)
	{
		if($s[$i] === '\\')
			++$i;
		elseif($s[$i] === ']' || $s[$i] === '|')
			break;
		$r.=$s[$i];
		++$i;
	}
*/
/*	if($reallyTheEnd && $s[$i] === '|') // skip from here to the end of the tag
	{
		while($i < $end)
		{
			if($s[$i] === '\\')
				++$i;
			elseif($s[$i] === ']')
				break;
			++$i;
		}
	}
//	else ++$i; //FIXME !
*/
/*	return $r;
}
*/
function whata_get_tag_content($s,&$i,$len)
{
	$r = array('tagName' => '', 'attr'=>array(), 'content'=> '', 'value'=>'');

	do
	{
		$r['tagName'] .= $s[$i];
		++$i;
	}
	while(isset($s[$i]) && $s[$i] !== '=' && !isBlank($s[$i]));
	skipSpace($s,$i,$len);

	if($s[$i] === '=')
		$r['value'] = trim(whata_get_attribute($s, $i, $len, false));
	else $r['value'] = '';	//FIXME : here we should get an error if $s[$i] === '|'

	if($s[$i] === ']') return;

	do
	{
		$attr = whata_get_attribute($s,$i,$len,false);
		if($s[$i] === '|')
		{
			if(preg_match('/([a-zA-Z]+)=([\s\S]+)/', $attr, $matches))
				$r['attr'][$matches[1]] = $matches[2];
			else $r['attr'][] = $attr;
		}
		else $r['content'] = $attr;
	}
	while(isset($s[$i] && $s[$i] === '|');

	return $r;
}

function skipSpace(&$s, &$i, &$len)
{
	while($i < $len && isBlank($s[$i])
	{
		++$i;
	}
}

function openParagr(&$o,$b,&$r)
{
	if(!$o && $b)
	{
		$r .= '<p>';
		$o = true;
	}
}

function whata2HTML_recur($s, $block, &$partial, $len, &$conf, $pre=false)
{
	if($block)
		$autoOpenParagr  = 1;

	$openedParagr = false;

	$r = '';
	$i=$partial;

	skipSpace($s, $i, $len);

/*WHATA fuck ?	if($i < $len && ($s[$i] === '=' || $s[$i] === '-'))
		$NL = true; // si on est sur une nouvelle ligne + caractère spécial
	else $NL=false; */

	$NL = false;
	while($i < $len)
	{
		if($s[$i] === '\\') // ne pas mettre après le test de NL ou \n
		{ // Échappement
			++$i;
			openParagr($openedParagr,$block,$r);
			if($s[$i] === '[')
				$r .= '[';
			elseif($s[$i] === ']')
				$r .= ']';
			elseif($s[$i] === '\\')
				$r .= '\\';
			elseif($s[$i] === '=')
				$r .= '=';
			elseif($s[$i] === '-')
				$r .= '-';
			else
				$r .= '\\' . $s[$i];
		}
		elseif($NL || $s[$i] === "\n")
		{ // Retour à la ligne ...
			if($pre &&  $s[$i] === "\n")
				$r .="\n";
			elseif($NL || ($i < $len && ($s[$i] === '-' || $s[$i] === '=')))
			{
				$NL = false;
				if($s[$i] === '-')
				{ // Liste implicite FIXME : multiline lists with "*"
					if($openedParagr)
						$r .= '</p>';
					$r .= '<ul>';

					while($i < $len && $s[$i] === '-')				
					{
						++$i;
						$tmp='';
						while($i < $len && $s[$i] !== "\n")
						{	$tmp.=$s[$i];
							++$i;
						}
						$r .= '<li>' . whata2HTML($tmp, $conf, false, false) . '</li>';
						++$i;
					}
					$r .= '</ul>';
					$openedParagr = false;

					if($i < $len && ($s[$i] === '=' || $s[$i] === '-'))
						$NL = true;

					--$i;
				}
				elseif($s[$i] === '=')
				{ // Titre
					if($openedParagr)
					{
						$r .= '</p>';
						$openedParagr = false;
					}

					$lev=0;
					while($s[$i] === '=')
					{
						++$i;
						++$lev;
					}

					// j est le niveau de titre.
					$j = $conf['min_title_level'] - 1 + $lev;
					if($j > 6)
						$j = 6; 

					$tmp = '';
					while($i < $len && $s[$i] !== "\n" && $s[$i] !== "\r")
					{
						$tmp .= $s[$i];
						++$i;
					}
					$r .= '<h' . $j . ' class="whata-h' . $lev . '">' . whata2HTML($tmp, $conf, false, false) . '</h' . $j . '>';

					if($i < $len && ($s[$i] === '=' || $s[$i] === '*'))
						$NL = true;
				}
			}
			else
			{
				++$i;
				if($s[$i] === "\n")
				{ // Paragraphe.
					if($openedParagr)
					{
						$r .= '</p>';
						$openedParagr = false;
					}
					while($s[$i] === "\n")
					{
						++$i;
					}
					if($s[$i] === '-' || $s[$i] === '=')
						$NL = true;
				}
				elseif($i < $len && ($s[$i] === '-' || $s[$i] === '='))
					$NL = true;
				else if($openedParagr) // Bon, bah nouvelle ligne :)
					$r .='<br />';
				--$i;
			}
		}
		else if($s[$i] === '[')
		{ // balise
			++$i;
			$name = strtolower(whataParseGetWord($s,$i,$len));
			if($name)
			{
				++$i;
				if($name === 'i')
				{
					openParagr($openedParagr,$block,$r);
					$r .= '<i>' . whata2HTML_recur($s, 0, $i,$len, $conf) . '</i>';
				}
				elseif($name === 'b')
				{
					openParagr($openedParagr,$block,$r);
					$r .= '<b>' . whata2HTML_recur($s, 0, $i,$len, $conf) . '</b>';
				}
				elseif($name === 'code')
				{
					if($openedParagr)
						$r .= '</p>';
					$openedParagr = false;
					--$i;
					if($s[$i] === '=')
					{
						++$i;
						$type = strtolower(whataParseGetWord($s,$i, $len));
						if($type==='html') $type='html4strict';
						$geshi = new GeSHi(trim(whata_get_attribute($s, $i, $len, false)), $type);
						$geshi->enable_classes();
//						$geshi->enable_strict_mode(GESHI_MAYBE);
						$geshi->set_header_type(GESHI_HEADER_PRE_TABLE);
						$geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS);
						$r .= '<div class="whata-code">' . $geshi->parse_code() . '</div>';
					}
					else
					{
						$r .= '<code class="whata-code">' . trim(htmlspecialchars(whata_get_attribute($s, $i, $len, false))) . '</code>';
					}
				}
				elseif($name === 'list')
				{//FIXME
					if($openedParagr)
						$r .= '</p>';
					if($s[$i] === '=')
					{
						++$i;
						switch($s[$i])
						{
							case '1':
							case 'I':
							case 'a':
							case 'A':
						}
					}
				}
				elseif($name === 'output')
				{ // Output of a program, not <pre> formated
					if($openedParagr)
						$r .= '</p>';
					$openedParagr = false;

					$r.='<blockquote class="whata-outut">' . whata2HTML_recur($s, 1, $i,$len, $conf) . '</blockquote>';
				}
				elseif($name === 'out')
				{ // Output of a program, not <pre> formated
					if($openedParagr)
						$r .= '</p>';
					$openedParagr = false;

					$r.='<pre class="whata-out">' . whata2HTML_recur($s, 0, $i,$len, $conf,true) . '</pre>';
				}
				elseif($name === 'quote')
				{
					if($openedParagr)
						$r .= '</p>';
					$openedParagr = false;
					$r.='<div class="whata-quote">';
					$source_header_show = false;
					if($s[$i-1] === '=') // url
					{
						$cite = ' cite="' . htmlspecialchars($url=whata_get_attribute($s, $i, $len, false)) . '"';
						if(WHATA_SHOW_QUOTE_HEADER && WHATA_QUOTE_HEADER_BEFORE)
						{
							$r.='<p class="whata-quote-header"> ' . WHATA_SOURCE_LABEL . '<a href="' . rawurlencode($url) . '">' . htmlspecialchars($url) . '</a></p>';
						}
						else $source_header_show = true;
					}
					else $cite = '';

					$r.='<blockquote' . $cite . '>' . whata2HTML_recur($s, 1, $i,$len, $conf) . '</blockquote>';

					if($source_header_show)
					{
						$r.='<p class="whata-quote-header"> ' . WHATA_SOURCE_LABEL . '<a href="' . rawurlencode($url) . '">' . htmlspecialchars($url) . '</a></p>';
					}

					$r.='</div>';
				}
				elseif($name === 'q')
				{
					openParagr($openedParagr,$block,$r);
					if($s[$i-1] === '=') // url
						$url = ' cite="' . htmlspecialchars(whata_get_attribute($s, $i, $len, false)) . '"';
					else $url = '';

					$r.='<q' . $url . '>' . whata2HTML_recur($s, 0, $i, $len, $conf) . '</q>';
				}
				elseif($name === 'whata')
				{
					openParagr($openedParagr,$block,$r);
					--$i;
					while(isset($s[$i]) && $s[$i] !== ']')
						++$i;
					$r.='<em>Whata!</em>';
				}
				else if($name === 'math')
				{
					$j=$i;
					while(isset($s[$i]) && $s[$i] !== ']')
						++$i;
					$r.=whataMath2HTML(substr($s, $j, $i-$j));
				}
				else if($name === 'img')
				{
					$r .= '<img src="' . htmlspecialchars(whata_get_attribute($s, $i, $len, false)) . '" alt="';
					if($s[$i] !==']')
						$r .=htmlspecialchars(whata_get_attribute($s, $i, $len, false));
					$r.='" />';
				}
//			...
				else
				{ //FIXME Gestion correcte des tags inconnus
					openParagr($openedParagr,$block,$r);
					$r .= '[' . $name;
					$i-=2;
				}
			}
			elseif($s[$i] === '[')
			{//lien
				openParagr($openedParagr,$block,$r);
				++$i;
				$j=$i; // on vérifie s'il y a un type; sinon on reprend depuis $i;

				$type = strtolower(whataParseGetWord($s,$j, $len));
				if($type === 'wp')
				{ // Wikipedia
					if($s[$j] === ':')
					{
						++$j;
						$lang = strtolower(whataParseGetWord($s,$j, $len));
					}
					else
						$lang=$conf['lang'];

					skipSpace($s,$j,$len);

					$link = trim(whata_get_attribute($s, $j, $len, false));
					--$j;
					$r.= '<a href="http://' . $lang . '.wikipedia.org/wiki/' . rawurlencode($link) . '">';
				}
				elseif ($type === 'php')
				{
					++$j;
					$link = whata_get_attribute($s, $j, $len);
					$r.= '<a href="http://php.net/' . rawurlencode($link) . '">';
				}
				else
				{
					$link = $type . htmlspecialchars(whata_get_attribute($s, $j, $len, false));
					$r.= '<a href="' . $link . '">';
				}

				$i = $j - 1;

				if($s[$i] === '|')
				{
					++$i;
					$r .= htmlspecialchars(whata2HTML_recur($s, 0, $i,$len, $conf));
				}
				else $r .= $link;

				$r .= '</a>';
			}
			elseif($s[$i] === '*')
			{
				openParagr($openedParagr,$block,$r);
				++$i;
				if($s[$i] === '*') // strong
				{
					++$i;
					$r .= '<strong>' . whata2HTML_recur($s, 0, $i,$len, $conf) . '</strong>';
				}
				else // em
				{
					$r .= '<em>' . whata2HTML_recur($s, 0, $i,$len, $conf) . '</em>';					
				}
			}
			elseif($s[$i] === '+')
			{
				++$i;
				openParagr($openedParagr,$block,$r);
				$r .= '<ins>' . whata2HTML_recur($s, 0, $i,$len, $conf) . '</ins>';
			}
			elseif($s[$i] === '-')
			{
				++$i;
				openParagr($openedParagr,$block,$r);
				if($s[$i+1] === '-')
				{
					++$i;
					$r .= '<strike>' . whata2HTML_recur($s, 0, $i,$len, $conf) . '</strike>';
				}
				$r .= '<del>' . whata2HTML_recur($s, 0, $i,$len, $conf) . '</del>';
			}
			elseif($s[$i] === 'u')
			{
				++$i;
				openParagr($openedParagr,$block,$r);
				$r .= '<u>' . whata2HTML_recur($s, 0, $i,$len, $conf) . '</u>'; // FIXME : invalide.
			}
			elseif($s[$i] === '_')
			{ //sub
				++$i;
				openParagr($openedParagr,$block,$r);
				$r .= '<sub>' . whata2HTML_recur($s, 0, $i,$len, $conf) . '</sub>';
			}
			elseif($s[$i] === '^')
			{ // sup
				++$i;
				openParagr($openedParagr,$block,$r);
				$r .= '<sup>' . whata2HTML_recur($s, 0, $i,$len, $conf) . '</sup>';
			}
			else
			{ // a void tag let you building lists with multiple lines or even paragraphs
				whata2HTML_recur($s, $block, $i,$len, $conf);
			}
		}
		else if($s[$i] === ']' && $partial)
		{
			if($openedParagr)
				$r .= '</p>';
			$partial=$i;
			return $r;
		}
		else
		{
			openParagr($openedParagr,$block,$r);
			$r .= htmlspecialchars($s[$i]);
		}
		++$i;
	}
	if($openedParagr)
		$r .= '</p>';

	$partial=$i;
	return $r;
}

function whata2HTML($s, $conf=array(), $block=true, $can_outer = true)
{
	$i=0;
	$s = str_replace("\r", "\n", str_replace("\r\n", "\n", trim($s))); // To UNIX format, trim spaces.
	$len=strlen($s);


	if(!isset($conf['lang'])) $conf['lang']='en';
	if(!isset($conf['min_title_level'])) $conf['min_title_level']=1;
	
	return ((WHATA_OUTER && $can_outer)?($block ? '<div':'<span') . ' class="whata">':'') . whata2HTML_recur($s,$block,$i, $len, $conf) . ((WHATA_OUTER && $can_outer)?($block ? '</div>':'</span>'):'');
}
