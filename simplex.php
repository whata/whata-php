<?php /* Do not modify this file, it is automatically generated from whata.dev.php */
/*
	Copyright (C) 2010 JAKSE Raphaël

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
include(dirname(__FILE__) . '/config.php');

/* constants used inside the parser */
define('_WHATA_CAN_OUTER',32);

define('_WHATA_BLOCK',1);
define('_WHATA_EMBEDED',2);
define('_WHATA_STOP_AFTER_NL',8);
define('_WHATA_CAN_AUTO_OPEN_PARAGRAPHS', 32);

if(!defined('WHATA_ESCAPE_CHAR'))
	define('WHATA_ESCAPE_CHAR', '~');

global $whata_global_conf;
$whata_global_conf = array // default conf (whata.conf.php)
(

	'WHATA_SIMPLEX'       => true,

	'SIMPLEX_SEMANTIC'       => WHATA_SIMPLEX_SEMANTIC,

	'MIN_TITLE_LEVEL'     => WHATA_MIN_TITLE_LEVEL,

// TRUE = wrap whata code with <div class="whata"> ... </div> or <span class="whata"> ... </span>
	'OUTER'               => WHATA_OUTER
);

if(isset($whata_override_conf)) // This allow the script that includes us to redefine our conf on the fly
{
	foreach($whata_override_conf as $key => $value)
	{
		$whata_global_conf[$key] = $value;
	}
}

function is_blank($c, $includeNL = true)
{
	return $c === ' ' || $c === '	' || ($includeNL && ($c === "\n")); // "\r" should not happen
}

function whata_get_following_word($s,&$i,$len)
{
	$r = '';
	while($i < $len && preg_match('/[a-zA-Z]/',$s[$i]))
	{
		$r.= $s[$i];
		++$i;
	}
	return $r;
}

function whata_simplex_tag($closing_c,&$s,&$i,$len, &$opts, &$conf, $parse = true)
{ // will return string parsed in "string$closing_c$closing_c" (don't parse if $parse = false)
	if($opts & _WHATA_EMBEDED)
		$opts ^= _WHATA_EMBEDED;

	if($opts & _WHATA_BLOCK)
		$opts ^= _WHATA_BLOCK;

	$tmp = '';
	$save_i = $i;
	$j = $i+2;
	while($j + 1 < $len)
	{
		if($s[$j] === $closing_c && $s[$j+1] === $closing_c)
		{
			$tmp_i = 0;
			if($parse)
				$tmp = whata2html_recur($tmp, $tmp_i, strlen($tmp),$conf, $opts, $openedParagr);
			$i = $j+1;
			return $tmp;
		}
		else $tmp .= $s[$j];

		++$j;
	}
	$i = $save_i;
	return false;
}

function whata_list_char($c)
{
	return $c === '#' || $c === '*' || $c === '-';
}

function count_new_spaces(&$s,$i)
{
	$i_before = $i;

	while($i_before && is_blank($s[$i_before-1], false))
		--$i_before;

	return $i - $i_before;
}

function whata_handle_list(&$r,&$s,&$i,$len,$conf,$space_count,$list_in_list=false)
{
	$list_character = $s[$i];
	if($list_character === '#')
		$r .= '<ol>';
	else
		$r .= '<ul>';

	$li_opened = 0;

	$new_space_count = $space_count;
	do
	{
		if($new_space_count > $space_count)
			$new_space_count = whata_handle_list($r,$s,$i,$len,$conf,$new_space_count,true);

		if($new_space_count < $space_count)
		{
			if($list_in_list || $new_space_count === -1 || $i >=$len || $s[$i] !== $list_character)
			{
				if($list_character === '#')
				{
					$r .= '</li></ol>'; // ordered list
				}
				else
				{
					$r .= '</li></ul>'; // unordered list
				}
				return $new_space_count;
			}
			else
				$r.= '</li>';
		}
		else
		{
			if($li_opened)
				$r.= '</li>';
			if($s[$i] !== $list_character)
			{
				if($list_character === '#')
					$r .= '</ol>'; // ordered list
				else
					$r .= '</ul>'; // unordered list
				return $new_space_count;
			}
			$space_count = $new_space_count;
		}

		++$i;

		$openedParagr = false;

		$r .= '<li>' . whata2HTML_recur ($s,$i,$len,$conf,
			($conf['whataSimplex']?0:_WHATA_BLOCK) |
				_WHATA_STOP_AFTER_NL,$openedParagr);
			// in Whata Simplex, we consider list as inline elements (block completely useless)

		$li_opened = 1;
		$inContentList = false; // We didn't parse any extra bullet content
		do
		{
			$countNL = 0;

			while($i < $len && $s[$i] === "\n")
			{
				++$i;
				++$countNL;
				skipSpace($s, $i, $len, false);
			}

			if($i < $len)
			{
				$new_space_count = count_new_spaces($s, $i);
				$listContinues  = whata_list_char($s[$i]);

				$newListContent = // there is still new content if
					(
						! $listContinues // we are not getting a new bullet
						&& $new_space_count > $space_count // and the content is indented compared to the bullet
					);
			}

			if( // We stop parsing the list if :
			    $i >= $len // no more char in buffer
			||  (
				$countNL !== 1 // or too many \n for another bullet
				&& !( // and we are not parsing new bullet content :
				    $inContentList // We didn't start parsing this content
				    && $newListContent // and there is not new content anymore
			        )
			    )
			)
			{
				if($list_character === '#')
					$r .= '</li></ol>';
				else
					$r .= '</li></ul>';

				return -1;
			}

			if($newListContent)
			{
				if(!$inContentList || $countNL > 1)
				{
					$inContentList = true;
					closeParagr($openedParagr, $r);
				}
				elseif($inContentList && $openedParagr)
				{
					$r .= '<br />';
				}

				$r .= whata2HTML_recur ($s,$i,$len,$conf,_WHATA_CAN_AUTO_OPEN_PARAGRAPHS | _WHATA_BLOCK | _WHATA_STOP_AFTER_NL | _WHATA_EMBEDED,$openedParagr); // We let Whata create Paragraphes even in Simplex Mode.
			}
		}
		while($newListContent);
	}
	while($i < $len && $listContinues);

	if($list_character === '#')
		$r .= '</li></ol>';
	else
		$r .= '</li></ul>';
	return $new_space_count;
}

function skipSpace(&$s, &$i, &$len,$includeNL = true)
{
	while($i < $len && is_blank($s[$i], $includeNL))
		++$i;
}

function openParagr(&$openedParagr,$bloc,&$r, &$pendingNL)
{
	if(!$openedParagr && $bloc)
	{
			$r .= '<p>';
		$openedParagr = true;
	}
	elseif($pendingNL)
	{
			$r.= '<br />';
	}
	$pendingNL = false;
}

function closeParagr(&$openedParagr, &$r)
{
	if($openedParagr)
		$r .= '</p>';

	$openedParagr = false;
}

function whata_link(&$url, &$content)
{
	$e_url = explode(' ', $url, 2);
	$e_url_0 = explode(':', $e_url[0], 2);

	if($e_url_0[0] === 'wp' || $e_url_0[0] === 'wiki')
	{ // Wikipedia
		if(isset($e_url_0[1]))
			$lang = strtolower($e_url_0[1]);
		else
			$lang = $conf['lang'];
		if(isset($e_url[1]))
			$link = $e_url[1];
		else // FIXME:syntax error
			$link = '';
		$url = 'http://' . $lang . '.wikipedia.org/wiki/' . rawurlencode($link);
	}
	elseif ($e_url_0[0] === 'php')
	{
		if(isset($e_url[1]))
			$link = $e_url[1];
		else // FIXME:syntax error
			$link = '';
		$url = 'http://php.net/' . rawurlencode($link);
	}
	if(empty($content))
		$content = isset($link)?$link:$url;
}

function test_whata_simplex($s,&$i,$len,&$r,$autoOpenParagr,&$openedParagr,&$pendingNL)
{
	if($i+2 < $len && $s[$i] === '*' && $s[$i + 1] === '*')
	{ // bold for whata Simplex
		$tmp = whata_simplex_tag('*', $s,$i,$len, $opts, $conf);
		if($tmp !== false)
		{
			openParagr($openedParagr,$autoOpenParagr,$r,$pendingNL);
			if($conf['simplex_semantic'])
				$r .= '<strong>' . $tmp . '</strong>';
			else
				$r .= '<b>' . $tmp . '</b>';
		}
		return $tmp !== false;
	}
	elseif($i+2 < $len && $s[$i] === "'" && $s[$i + 1] === "'")
	{ // italic for whata Simplex
		$tmp = whata_simplex_tag("'", $s,$i,$len, $opts, $conf);
		if($tmp !== false)
		{
			openParagr($openedParagr,$autoOpenParagr,$r,$pendingNL);
			if($conf['simplex_semantic'])
				$r .= '<em>' . $tmp . '</em>';
			else
				$r .= '<i>' . $tmp . '</i>';
		}
		return $tmp !== false;
	}
	elseif($i+2 < $len && $s[$i] === '-' && $s[$i + 1] === '-')
	{ // strike for whata Simplex
		$tmp = whata_simplex_tag('-', $s,$i,$len, $opts, $conf);
		if($tmp !== false)
		{
			openParagr($openedParagr,$autoOpenParagr,$r,$pendingNL);
			$r .= '<strike>' . $tmp . '</strike>';
		}
		return $tmp !== false;
	}
	elseif($i+2 < $len && $s[$i] === '_' && $s[$i + 1] === '_')
	{ // underline for whata Simplex
		$tmp = whata_simplex_tag('_', $s,$i,$len, $opts, $conf);
		if($tmp !== false)
		{
			openParagr($openedParagr,$autoOpenParagr,$r,$pendingNL);
			$r .= '<span style="text-decoration:underline">' . $tmp . '</span>';
		}
		return $tmp !== false;
	}
	else return false;
}


function whata2HTML_recur($s,&$i,$len,&$conf,$opts,&$openedParagr=false)
{// this function assumes $s to have the UNIX format ("\n" for new lines). the  whata2HTML function do the conversion if this was not the case
	global $whata_global_conf;
	$block = $opts & _WHATA_BLOCK; // We are in a block element

	$embeded = $opts & _WHATA_EMBEDED; // prevent paragraphs from being automatically opened and closed when leaving the function (whata2HTML_recur was called from whata2HTML_recur)

	if($opts & _WHATA_STOP_AFTER_NL) // We must stop parsing when a new line is reached
	{
		$opts ^= _WHATA_STOP_AFTER_NL;
		$stop_after_nl = true;
	}
	else
		$stop_after_nl = false;

	$r = '';

	$NL = ($opts & _WHATA_CAN_AUTO_OPEN_PARAGRAPHS) || !$stop_after_nl; // we consider a new ligne if we don't stop after a new line. Otherwise, yes.

	$autoOpenParagr = $block && (($opts & _WHATA_CAN_AUTO_OPEN_PARAGRAPHS) || !$embeded);

	skipSpace($s,$i,$len);

	while($i < $len)
	{
		if(test_whata_simplex($s,$i,$len,$r,$autoOpenParagr,$openedParagr,$pendingNL))
		{
			if($NL)
				$NL = false;
		}
		else
		{
			$c = $s[$i];
			if($NL || $c === "\n")
			{
	if($stop_after_nl && $c === "\n")
				{
	
					if(!$embeded)
						closeParagr($openedParagr, $r);
					return $r;
				}
				else
				{
					$countNL = 0;
					while($i < $len && $s[$i] === "\n")
					{
						++$i;
						++$countNL;
						skipSpace($s, $i, $len, false);
					}

					if($i < $len)
					{
						$NL = false;

						if(test_whata_simplex($s,$i,$len,$r,$autoOpenParagr,$openedParagr,$pendingNL))
							$countNL = 0;
						elseif($block && whata_list_char($s[$i]))
						{//lists
							closeParagr($openedParagr, $r);

							/* How many space are before the list item ? */
							$i_before = $i;
							while($i_before && is_blank($s[$i_before-1], false))
								{--$i_before;}

							whata_handle_list($r,$s,$i,$len,$conf,$i - $i_before, false);

							$NL = true;
	
							$countNL = 0;
						}
						elseif($block && $s[$i] === '=')
						{// title
							closeParagr($openedParagr, $r);

							$lev=0;
							while($s[$i] === '=')
							{
								++$i;
								++$lev;
							}

							// j is the title's level

							$j = $conf['min_title_level'] - 1 + $lev;
							if($j > 6)
								$j = 6;
							elseif($j < 1)
								$j = 1;
	
							$r .= '<h' . $j . ' class="whata-h' . $lev . '">' . whata2HTML_recur($s,$i,$len,$conf, _WHATA_STOP_AFTER_NL, $openedParagr) . '</h' . $j . '>';
							$NL = true;
							$countNL = 0;
						}

						if($countNL)
						{// new Paragraph or new line
							if($openedParagr || !$autoOpenParagr)
							{
								if($countNL === 1 || !$autoOpenParagr) // new line
									$pendingNL = true;
								else //new Paragraph
									closeParagr($openedParagr, $r);
							}
						}

						skipSpace($s,$i,$len);

						//if title escaped, don't go back (for Whata Simplex!)
						if($i >= $len || !($s[$i] === WHATA_ESCAPE_CHAR && $i+1 < $len && $s[$i+1] === '='))
							--$i;
					}
				}
			}
	
			elseif($i+2 < $len && $c === '[' && $s[$i + 1] === '[')
			{ // links for whata Simplex
				$url = whata_simplex_tag(']', $s,$i,$len, $opts, $conf, false);

				if($url !== false)
				{
					$content = '';
					whata_link($url, $content);

					if(!preg_match('/^[a-zA-Z]+:/',$url))
						$url = 'http://' . $url;
					openParagr($openedParagr,$autoOpenParagr,$r,$pendingNL);
					$r .= '<a href="' . $url . '">' . htmlspecialchars($content) . '</a>';
				}
			}
			else
			{
				if(!is_blank($c))
					openParagr($openedParagr,$autoOpenParagr,$r,$pendingNL);

				$old_i = $i;
				do
				{
					++$i;
				}
				while($i < $len && !(
					($c = $s[$i]) === "\n"
					|| $c === '*'
					|| $c === '_'
					|| $c === '"'
					|| $c === "'"
					|| $c === '`'
				));

				$r .= htmlspecialchars(substr($s, $old_i, ($i--) - $old_i));
			}
		}
		++$i;
	}
	if(!$embeded)
		closeParagr($openedParagr, $r);

	return $r;
}

function whata2HTML($s, $conf=array(), &$set_array = array(), $opts=33)
{
	if(empty($s)) return '';

	global $whata_global_conf;
	$s = str_replace("\r", "\n", str_replace("\r\n", "\n", trim($s))); // To UNIX format, trim spaces.

	if(!isset($conf['lang']))
		$conf['lang'] = 'en';
	if(!isset($conf['min_title_level']))
		$conf['min_title_level'] = $whata_global_conf['MIN_TITLE_LEVEL'];
	if(!isset($conf['simplex_semantic']))
		$conf['simplex_semantic'] = $whata_global_conf['SIMPLEX_SEMANTIC'];

	if(isset($conf['inline']) && $conf['inline'] && ($opts & _WHATA_BLOCK))
		$opts ^= _WHATA_BLOCK;
	elseif(isset($conf['block']) && $conf['block'])
		$opts |= _WHATA_BLOCK;

	$i=0; $len = strlen($s);

	if(isset($set_array['whataSimplex']))
	{
		if(strtolower($set_array['whataSimplex']) === 'true' || intval($set_array['whataSimplex']))
			$conf['whataSimplex'] = true;
		else
			$conf['whataSimplex'] = false;
	}
	elseif(!isset($conf['whataSimplex']))
		$conf['whataSimplex'] = $whata_global_conf['WHATA_SIMPLEX'];

	$can_outer = $opts & _WHATA_CAN_OUTER;
	$block = $opts & _WHATA_BLOCK;

	return ($whata_global_conf['OUTER'] && $can_outer
	        ?
	        	($block
	        		? '<div'
	        		: '<span'
	        	) .
	        	' class="whata">'
	         : ''
	       ) .
	       whata2HTML_recur($s,$i,$len,$conf,_WHATA_BLOCK & $opts) .
	       (
	       	($whata_global_conf['OUTER'] && $can_outer)
	       	 ?
	       		($block
	       			? '</div>'
	       			: '</span>'
	       		)
	       	 : ''
		);
}

if(defined('DDBLOG_MIN_TITLE_LEVEL'))
{
	$ddBlogWhataConf = array (
		'lang'            => DDBLOG_LANGUAGE,
		'min_title_level' => DDBLOG_MIN_TITLE_LEVEL,
		'image_prefix'    => DDBLOG_PUB . '/imgs/'
	);

	function ddBlog_syntax2HTML($s, $tofile=false)
	{
		if($tofile)
			return file_put_contents($tofile, whata2HTML($s, $GLOBALS['ddBlogWhataConf']));
		return whata2HTML($s, $GLOBALS['ddBlogWhataConf']);
	}

	function ddBlog_file2HTML($f, $tofile=false)
	{
		if($tofile)
			return file_put_contents($tofile,whata2HTML(file_get_contents($f),$GLOBALS['ddBlogWhataConf']));
		return whata2HTML(file_get_contents($f),$GLOBALS['ddBlogWhataConf']);
	}
}
