<?php

function handle_styles($styles) {
    $styles = explode('\\', $styles);
    $r = '';
    foreach ($styles as $s) {
        $r .= '<link rel="stylesheet" href="' . htmlspecialchars(trim($s), ENT_QUOTES) . '" />';
    }
    return $r;
}

$whata_override_conf = array(
    'MATH_METHOD'   => 'function',
    'MATH_FUNCTION' => 'whata_MathJax',
    'GESHI_URL'     => 'geshi.php'
);

function my_require_once($filename) {
    if (is_file(dirname(__FILE__) . '/' . $filename)) {
        require_once(dirname(__FILE__) . '/' . $filename);
    } else {
        require('whata/' . $filename);
    }
}


my_require_once('whatadom.php');
my_require_once('whata2html.php');


function setValue($tpl, $key, $value, $isWhataNode = false) {
    if ($isWhataNode) {
        if (false !== strpos($tpl, $search = '${html-inline ' . $key . '}')) {
            $tpl = str_replace($search, whatadom2html($value, WHATAHTML_INLINE, $conf), $tpl);
        }

        if (false !== strpos($tpl, $search = '${html-block ' . $key . '}')) {
            $tpl = str_replace($search, whatadom2html($value, 0, $conf), $tpl);
        }

        if (false !== strpos($tpl, $search = '${text ' . $key . '}')) {
            $tpl = str_replace($search, htmlspecialchars(whata_text_content($value)), $tpl);
        }
    } else {
        $tpl = str_replace('${' . $key . '}', $value, $tpl);
    }

    $tpl = preg_replace('/\n[ \t]*#ifdef\{' . $key . '\} *\n[ \t]*/', '', $tpl);
    $tpl = preg_replace('/#ifdef\{' . $key . '\}/', '', $tpl);
    $tpl = preg_replace('/ *\n[ \t]*#fidef\{' . $key . '\}\n/', '', $tpl);
    $tpl = preg_replace('/#fidef\{' . $key . '\}/', '', $tpl);
    $tpl = preg_replace('/ *\n[ \t]*#ifndef\{' . $key . '\}[\s\S]*?#findef\{' . $key . '\}\n/', '', $tpl);
    $tpl = preg_replace('/#ifndef\{' . $key . '\}[\s\S]*?#findef\{' . $key . '\}/', '', $tpl);

    return $tpl;
}

function finalizeTemplate($tpl) {
    $tpl = preg_replace('/[ \t]*\n[ \t]*\#ifdef\{([\s\S]+?)\}[\s\S]*?\#fidef\{\1\}[ \t]*\n/', '', $tpl);
    $tpl = preg_replace('/\#ifdef\{([\s\S]+?)\}[\s]*([\s\S]*?)[\s]*\#fidef\{\1\}/', '$2', $tpl);
    $tpl = preg_replace('/[ \t]*\n[ \t]*\#ifndef\{([\s\S]+?)\}[\s]*([\s\S]*?)[\s]*\#findef\{\1\}[ \t]*\n/', '$2', $tpl);
    $tpl = preg_replace('/\#ifndef\{([\s\S]+?)\}[\s]*([\s\S]*?)[\s]*\#findef\{\1\}/', '$2', $tpl);
    $tpl = preg_replace('/\$\{[\s\S]+?\}/', '', $tpl);
    return $tpl;
}

$known = array();

function whataFileToHTMLFile($f, $dest = '', $opts = [], $forceRedo = false) {
    global $known;

    if (!$forceRedo && isset($known[$dest])) {
        return;
    }

    $known[$dest] = true;

    $bf = basename($f, '.whata');

    $suffix = isset($opts['suffix']) ? $opts['suffix'] : '';

    $output_directory = isset($opts['outputDir']) ? $opts['outputDir'] . '/' : dirname($f) . '/';

    if (!$dest) {
        $dest = $output_directory . $bf . $suffix;
    }

    $xhtml = isset($opts['xhtml']) && $opts['xhtml'];

    $df = dirname($f);
    $conf = array(
        'processFootnotes'  => false,
        'lang'              => isset($opts['lang']) ? $opts['lang'] : 'en',
        'xhtml'             => $xhtml,
        'workingDir'        => $df,
        'min_title_level'   => isset($opts['min_title_level']) ? $opts['min_title_level'] : 2,
        'customTags'        => isset($opts['customTags']) ? $opts['customTags'] : '',
        'linkToWhataSuffix' => $suffix,
        'linkToWhataPrefix' => isset($opts['prefix']) ? $opts['prefix'] : '',
        'footnoteRefPrefix' => isset($opts['footnoteRefPrefix']) ? $opts['footnoteRefPrefix'] : ''
    );

    if (isset($opts['mathFunction'])) {
        $conf['mathFunction'] = $opts['mathFunction'];
    }

    if (isset($opts['recursive']) && $opts['recursive']) {
        $conf['docRefCallback'] = function ($file, $dest) use ($df, $opts, $output_directory) {
            whataFileToHTMLFile($file, $output_directory . '/' . $dest, $opts);
        };
    }

    $conftitle = $conf;
    if ($f) {
        if ($f !== 'php://stdin' && !is_file($f)) {
            echo 'Warning: file ', $f, " doesn’t exist.\n";
            return;
        }

        $dom = whataDOM(file_get_contents($f), $conf);

        $template = '';
        $tplfiles = '';
        if (isset($opts['tplFunc'])) {
            $template = $opts['tplFunc']($conf, $f, $tplfiles);
        } else if (isset($opts['tpl'])) {
            $template = $opts['tpl'];
        } else {
            throw new Exception('You need to supply a template (\'tpl\' option)');
        }

        $html = whatadom2html($dom, 0, $conf);
        $opts['defines']['!styles'] = isset($opts['defines']['styles']) ? $opts['defines']['styles'] : '';

        if (isset($conf['sets']['css'])) {
            $opts['defines']['!styles'] .= handle_styles(whata_text_content($conf['sets']['css']));
        }

        if (isset($conf['needGeshi']) && isset($opts['geshiPath']) && $opts['geshiPath']) {
            $opts['defines']['!styles'] .= '<link rel="stylesheet" href="' . htmlspecialchars($opts['geshiPath'], ENT_QUOTES) . '" />';
        }

        if (isset($opts['defines'])) {
            foreach ($opts['defines'] as $key => $value) {
                $template = setValue($template, $key, $value);
            }
        }

        foreach ($conf['sets'] as $key => $value) {
            $template = setValue($template, $key, $value, true);
        }

        $template = setValue($template, '!meta_charset', ($xhtml ? '<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />' : '<meta charset="utf-8" />'));

        if (isset($conf['needMath']) && $conf['needMath']) {
            $mathFunction = isset($opts['mathFunction']) ? $opts['mathFunction'] : 'whata_MathJax';

            $KaTeXPath = (isset($opts['KaTeXPath']) && $opts['KaTeXPath'])
                ? $opts['KaTeXPath']
                : 'https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.5.1';

            if ($mathFunction === 'whata_OfflineKaTeX') {
                $template = setValue(
                    $template,
                    '!scripts',
                    '<link rel="stylesheet" href="' . $KaTeXPath . '/katex.min.css" />'
                );
            } else if ($mathFunction === 'whata_KaTeX') {
                $template = setValue(
                    $template,
                    '!scripts',
                    '<script type="text/javascript" src="' . $KaTeXPath . '/katex.min.js"></script>' .
                    '<link rel="stylesheet" href="' . $KaTeXPath . '/katex.min.css" />' .
                    '<script>
                        window.addEventListener("DOMContentLoaded", function () {
                            "use strict";

                            var r = function (elems, displayMode) {
                                for (var i = 0; i < elems.length; i++) {
                                    var katexElem = document.createElement(displayMode ? "div" : "span");
                                    katexElem.className="whata-math whata-katex";
                                    try {
                                        katex.render(elems[i].textContent, katexElem, {displayMode: displayMode}),
                                        elems[i].parentNode.replaceChild(
                                            katexElem,
                                            elems[i]
                                        );
                                    } catch (e) {
                                        console.error(e);
                                        katexElem.className="whata-math-error whata-katex-error";
                                        katexElem.textContent = elems[i].textContent;
                                        elems[i].parentNode.insertBefore(katexElem, elems[i])
                                    }
                                }

                            }

                            r([].slice.call(document.querySelectorAll("[type=\"math/tex\"]")), false);
                            r([].slice.call(document.querySelectorAll("[type=\"math/tex; mode=display\"]")), true);
                        });
                    </script>'
                );
            } else if ($mathFunction === 'whata_MathJax') {
                $mathJaxPath = (isset($opts['MathJaxPath']) && $opts['MathJaxPath'])
                    ? $opts['MathJaxPath']
                    : (
                            'https://cdn.mathjax.org/mathjax/latest'
                    );

                $template = setValue($template, '!scripts', '<script type="text/javascript" src="' . $mathJaxPath . '/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
                    <script>
                        MathJax.Hub.Config({
                            tex2jax: {
                                inlineMath : [],
                                displayMath : [],
                                ignoreClass: "tex2jax_ignore",
                                processClass: "tex2jax_process",
                            //  preview: "none",
                                processEnvironments: false
                            },
                            TeX: {extensions: ["AMSmath.js","AMSsymbols.js"]}
                        });
                    </script>');
            }
        }

        $template = setValue($template, '!tplfiles', $tplfiles);
        $template = setValue($template, '!basename', $bf);
        $template = setValue($template, '!footnotes', whatahtml_process_footnotes($conf));
        $template = setValue($template, '!content', $html);
        $template = finalizeTemplate($template);

        if ($dest !== 'php://stdout') {
            $p = dirname($dest);
            if (!is_dir($p)) {
                mkdir($p, 0777, true);
            }
        }

        file_put_contents($dest, $template);
    }

    return $conf;
}
?>
