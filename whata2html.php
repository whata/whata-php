<?php
/*
        Copyright (C) 2012-2014 JAKSE Raphaël

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

define('WHATAHTML_PRE', 1);
define('WHATAHTML_INLINE', 2);
define('WHATAHTML_DO_NOT_AUTO_OPEN_PARAGR', 4);
define('WHATAHTML_IN_TITLE', 8);
define('WHATAHTML_IS_BLOCK', 16);
define('WHATAHTML_SURROUNDING', 32);
define('WHATAHTML_IN_SECTION', 64);
include_once(dirname(__FILE__) . '/config.php');

global $whata_global_conf;
$whata_global_conf = array // default conf (whata.conf.php)
(

// the method used to handle maths (url, include, function)
    'MATH_METHOD'         => WHATA_MATH_METHOD,

// the http script generating math images if MATH_METHOD = 'url'
    'MATH_URL'            => WHATA_MATH_URL,

// the file defining whata_math_to_html if MATH_METHOD = 'include'
    'MATH_INCLUDE'        => WHATA_MATH_INCLUDE,

// the name of the function generating math markup
    'MATH_FUNCTION'       => 'whata_math2html', // can only be modified by the script that includes us if MATH_METHOD = function

// TRUE = we must show informations aubout quote
    'SHOW_QUOTE_HEADER'   => WHATA_SHOW_QUOTE_HEADER,

// TRUE = the information is displayed BEFORE the quote
    'QUOTE_HEADER_BEFORE' => WHATA_QUOTE_HEADER_BEFORE,

// the text to show before the quote's source
    'SOURCE_LABEL'        => WHATA_SOURCE_LABEL,

    'IMAGE_PREFIX'        => WHATA_IMAGE_PREFIX,
    'WHATA_SIMPLEX'       => WHATA_SIMPLEX,
    'SIMPLEX_SEMANTIC'    => WHATA_SIMPLEX_SEMANTIC,
    'MIN_TITLE_LEVEL'     => WHATA_MIN_TITLE_LEVEL,

// TRUE = wrap whata code with <div class="whata"> ... </div> or <span class="whata"> ... </span>
    'OUTER'               => WHATA_OUTER,

    'GESHI_URL'           => WHATA_GESHI_URL,
    'HTML5'               => WHATA_HTML5
);

function whata_MathJax($code, $displayBlock=false, $conf=[]) {
   $xml = isset($conf['xhtml']) ? $conf['xhtml'] : false;
    if ($displayBlock) {
//        $delimL = '\[';$delimR = '\]';
//        $tagName = 'div';
        $mode = '; mode=display';
    } else {
//        $delimL = '\(';$delimR = '\)';
//        $tagName = 'span';
        $mode = '';
    }

    return array (
//        'name' => $tagName,
        'name'      => 'script',
        'innerHTML' => $xml ? '<![CDATA[' . $code . ']]>' : $code,
        'autoclose' => false,
        'attr'      => array (
            'title' => htmlspecialchars($code),
//          'class' => 'whata-math whata-MathJax tex2jax_process'
            'type'  => 'math/tex' . $mode,
        )
    );
}

function whata_KaTeX($code, $displayBlock=false, $conf=[]) {
    return whata_MathJax($code, $displayBlock, $conf);
}

function whata_offlineKaTeX($code, $displayBlock=false, $conf=[]) {
    $mode = $displayBlock ? '--display-mode' : '';

    return array (
        'name'      => 'span',
        'innerHTML' => shell_exec('echo ' . escapeshellarg(addslashes($code)) . ' | katex ' . $mode),
        'autoclose' => false,
        'attr'     => array()
    );
}

if (isset($whata_override_conf)) { // This allow the script that includes us to redefine our conf on the fly
    foreach ($whata_override_conf as $key => $value) {
        $whata_global_conf[$key] = $value;
    }
}

if ($whata_global_conf['MATH_METHOD'] === 'url') {
    function whata_math2html($code) {
        return array (
            'name' => 'img',
            'attr' => array (
                'alt' => $code,
                'title' => $code,
                'src' => $GLOBALS['whata_global_conf']['MATH_URL'] . $code,
                'class' => 'whata-math'
            ),
            'innerHTML' => '',
            'autoclose' => true
        );
    }
} else if (!($whata_global_conf['MATH_METHOD'] === 'function' && isset($whata_global_conf['MATH_FUNCTION']))) {
    include($whata_global_conf['MATH_INCLUDE']); // included file MUST have a whata_math_to_html function.
}
// else $whata_override_conf['MATH_FUNCTION'] must be a valid function name

function whatahtml_get_footnote_prefix($conf) {
    if (!isset($conf['footnoteRefPrefix']) || empty($conf['footnoteRefPrefix'])) {
        return '';
    }

    return $conf['footnoteRefPrefix'] . '-';
}

function closeParagr(&$html, &$openedParagr, &$conf) {
    if ($openedParagr) {
        if (isset($conf['suround_with_inline_tags'])) {
            while ($conf['suround_with_inline_tags']['inside']) {
                -- $conf['suround_with_inline_tags']['inside'];
                -- $conf['suround_with_inline_tags']['openedByCurrentInstance'];
                $html .= $conf['suround_with_inline_tags'][$conf['suround_with_inline_tags']['inside']]['after'];
            }

            if ($conf['suround_with_inline_tags']['openedByCurrentInstance'] !== 0) {
                $conf['suround_with_inline_tags']['openedByCurrentInstance'] = 0;
            }
        }

        $html .= '</p>';
        $openedParagr = false;
    }
}

function whata_inline($text, $flags, &$openedParagr, &$mustclear, &$conf) {
    $html = '';

    openParagr($html, $flags, $openedParagr, $mustclear, $conf);
    return $html . htmlspecialchars($text);
}

function openParagr(&$html, $flags, &$openedParagr, &$mustclear, &$conf) {
    if (!$openedParagr && !($flags & (WHATAHTML_INLINE | WHATAHTML_IN_TITLE | WHATAHTML_DO_NOT_AUTO_OPEN_PARAGR | WHATAHTML_PRE))) {
        if ($mustclear) {
            $html .= '<p class="whata-tag-clear" style="clear:' . $mustclear . '">';
            $mustclear = '';
        } else {
            $html .= '<p>';
        }

        $openedParagr = true;
    } elseif ($mustclear) {
        $html .= '<br class="whata-clear" style="clear:' . $mustclear . '" />';
        $mustclear = '';
    }

    if (isset($conf['suround_with_inline_tags'])) {
        while ($conf['suround_with_inline_tags']['inside'] < $conf['suround_with_inline_tags']['count']) {
            $html .= $conf['suround_with_inline_tags'][$conf['suround_with_inline_tags']['inside'] ++]['before'];
            ++$conf['suround_with_inline_tags']['openedByCurrentInstance'];
        }
    }
}

function whata_title_number($node, & $conf) {
    $i = 1;
    $ret = '';
    while ($i < $node['realLevel']) {
        if (!isset($conf['title_number_' .$i]))
            $conf['title_number_' . $i] = 1;

        $ret .= $conf['title_number_' . $i] . '.';
        ++$i;
    }

    $ret .= $conf['title_number_' .$i] = $node['titleNumber'];
    ++$i;
    while (isset($conf['title_number_' .$i])) {
        $conf['title_number_' .$i] = 1;
        ++$i;
    }
    return $ret;
}

function whata_latex_dollar($s, &$i, $len, $flags, $mustclear, &$openedParagr, &$conf) {
   global $whata_global_conf;
   assert($s[$i] === '$');
   $conf['needMath'] = true;
   ++$i;
   $m = $i;
   while ($i < $len && $s[$i] !== '$') {
      if ($s[$i] === '\\')
         ++$i;
      ++$i;
   }
   $fun = isset($conf['mathFunction']) ? $conf['mathFunction'] : $whata_global_conf['MATH_FUNCTION'];
   $tag_to_append=$fun(substr($s, $m, $i-$m), false, $conf);
   if ($tag_to_append['autoclose'])
         $tag_to_append['attr'][0] = true;

   $node['text'] = $tag_to_append['innerHTML'];

   return whatahtml_tag($tag_to_append['name'], $node, $flags, $mustclear, $openedParagr, $conf, $tag_to_append['attr']);
}

function whata_algorithmic($s, $node, $flags, $mustclear, &$openedParagr, &$conf) {
    $r = '';
    $indent = 0;
    $begun = false;
    $endStruct = false;
    $closeBracket = false;
    $i=0; $len = strlen($s);
    while ($i < $len) {
        if ($s[$i] === '\\') {
            ++$i;
            $cmd = '';

            while ($i < $len && letter($s[$i])) {
                $cmd .= $s[$i];
                ++$i;
            }

            switch ($cmd = strtolower($cmd)) {
            case 'state':
                if ($r) {
                    $r .= '<br />' . str_repeat('   ', $indent);
                }
                skipSpace($s, $i, $len);
                break;
            case 'forall':
                $cmd = 'for all';
            case 'for':
            case 'loop':
            case 'if':
            case 'repeat':
            case 'loop':
            case 'while':
            case 'procedure':
            case 'function':
                $r .= ($r?'<br />':'') . str_repeat('   ', $indent) . '<b class="whata-algorithmic-struct">' . $cmd . '</b> ';
                ++$indent;
                if ($cmd === 'procedure' || $cmd === 'function') {
                    // FIXME: factoriser code
                    if ($i < $len && $s[$i] === '{') {
                        ++$i;
                        $r .= '<span class="whata-algorithmic-procedure-name">';

                        $procedureName = '';
                        while ($i < $len && $s[$i] !== '}') {
                            if ($s[$i] === '$') {
                                $r .= $procedureName . whata_latex_dollar($s, $i, $len, $flags, $mustclear, $openedParagr, $conf);
                                $procedureName = '';
                            } else {
                                $procedureName .= $s[$i];
                            }
                            ++$i;
                        }

//                         if ($procedureName !== '') {
//                             $l = '$\textsc{' . $procedureName . '}$';
//                             $li = 0;
//                             $llen = strlen($l);
//                             $r .= whata_latex_dollar($l, $li, $llen, $flags, $mustclear, $openedParagr, $conf);
//                         }

                        $r .= $procedureName . '</span>';
                        ++$i; // '}'
                        if ($i < $len && $s[$i] === '{') {
                            ++$i;
                            if ($s[$i] !== '}') {
                            $r .= '<span class="whata-algorithmic-procedure-parameters">(';
                            while ($i < $len && $s[$i] !== '}') {
                                if ($s[$i] === '$')
                                    $r .= whata_latex_dollar($s, $i, $len, $flags, $mustclear, $openedParagr, $conf);
                                else
                                    $r .= $s[$i];
                                ++$i;
                            }
                            $r .= '</span>)';
                            }
                            ++$i; // '}'
                        }
                    }
                } else {
                    switch ($cmd) {
                        case 'if':
                        case 'elsif':
                            $endStruct = 'then';
                            break;
                        case 'forall':
                        case 'for':
                        case 'while':
                        case 'repeat':
                        case 'while':
                            $endStruct = 'do';
                    }
                }
                break;
            case 'elsif':
                $endStruct = 'then';
            case 'else':
            case 'endfor':
            case 'endif':
            case 'endwhile':
            case 'endloop':
            case 'endprocedure':
            case 'endfunction':
            case 'until':
                --$indent;
                $r .= '<br />' . str_repeat('   ', $indent)
                    . '<b class="whata-algorithmic-struct">'
                        . str_replace(array ('elsi', 'end'), array ('else i', 'end '), $cmd)
                    . '</b> ';
                if ($cmd === 'else' || $cmd === 'elsif')
                    ++$indent;
                break;
            case 'return':
            case 'require':
            case 'ensure':
            case 'print':
                $r .= '<b class="whata-algorithmic-instr">' . $cmd . '</b> ';
                break;
            case 'and':
            case 'or' :
            case 'xor':
            case 'not':
            case 'to' :
                $r .= ' <b class="whata-algorithmic-operator">' . $cmd . '</b> ';
                break;
            case 'true':
            case 'false':
                $r .= ' <b class="whata-algorithmic-value">' . $cmd . '</b> ';
                break;
            case 'comment':
                ++$i; // '{'
                $r .= ' <i class="whata-algorithmic-comment">';
                while ($i < $len && $s[$i] !== '}') {
                    if ($s[$i] === '$') {
                        $r .= whata_latex_dollar($s, $i, $len, $flags, $mustclear, $openedParagr, $conf);
                    } else {
                        $r .= $s[$i];
                    }
                    ++$i;
                }
                ++$i; // '}'
                $r .= '</i>';
                break;
            }

            skipSpace($s, $i, $len);

            if ($i < $len && $s[$i] === '{') {
                $closeBracket = true;
                ++$i;
            }

            continue; // don't ++$i here, that's already done
        } else if ($closeBracket && $s[$i] === '}') {
            $closeBracket = false;
            if ($endStruct) {
                $r .= ' <b class="whata-algorithmic-struct">' . $endStruct . '</b>';
                $endStruct = false;
            }
        } else if ($s[$i] === '\\') {
            ++$i;
            if ($i < $len && $s[$i] === '\\') {
                $r .= "<br/>" . str_repeat('   ', $indent);
            } else {
                $r .= $s[$i];
            }
        } else if ($s[$i] === '$') {
            $r .= whata_latex_dollar($s, $i, $len, $flags, $mustclear, $openedParagr, $conf);
        } else {
            $r .= $s[$i];
        }
        ++$i;
    }
    return $r;
}

$orderedListStyles = array(
    'decimal',
    'cjk-decimal',
    'decimal-leading-zero',
    'lower-roman',
    'upper-roman',
    'lower-greek',
    'lower-alpha',
    'lower-latin',
    'upper-alpha',
    'upper-latin',
    'armenian',
    'georgian',
    'hebrew',
    'ethiopic-numeric',
    'hiragana',
    'katakana',
    'hiragana-iroha',
    'katakana-iroha',
    'japanese-informal',
    'japanese-formal',
    'korean-hangul-formal',
    'korean-hanja-informal',
    'korean-hanja-formal',
    'simp-chinese-informal',
    'cjk-ideographic',
    'simp-chinese-formal',
    'trad-chinese-informal',
    'trad-chinese-formal'
);

function whata_part($partNode, $flags, & $conf) {
    require_once('whata/whatadom.php');
    $title = null;

    $conf2 = $conf;

    $wd = isset($conf['workingDir']) ? $conf['workingDir'] : '.';
    if (isset($partNode['text'])) {
        $title = array('nodeType'=>'text', 'content'=> $partNode['text']);
    } else if ($tContent = trim(whata_text_content($partNode['content']))) {
        $title = $partNode['content'];
    } else if (isset($partNode['value']) && trim($partNode['value']) && $f = @file_get_contents($wd . '/' . $partNode['value'] . '.whata')) {
        $values = whataGetValues($f, $conf2, array());
        if(isset($values['title'])) {
            $title = $values['title'];
        }
    }

    $url = 'doc:' . trim($partNode['value']);
    whata_link($url, $title, $conf);

    if (!$title) {
        $title = array('nodeType'=>'text', 'content'=> '(untitled part)');
    }

    return '<a href="' . $url . '">' . whatadom2html($title, $flags | WHATAHTML_INLINE, $conf2) . '</a>';
}

function whatadom2html($dom, $flags = 0, & $conf=array (), &$openedParagr = false, $light=false) {
    global $orderedListStyles;

    if (!isset($conf['min_title_level'])) {
        $conf['min_title_level'] = $GLOBALS['whata_global_conf']['MIN_TITLE_LEVEL'];
    }

    if (!isset($conf['id_prefix'])) {
        $conf['id_prefix'] = '';
    }

    $html = '';
    $mustclear = '';

    global $whata_global_conf;

    if (!$light) {
        if ($surround = isset($conf['suround_with_inline_tags'])) {
            $openedByCurrentInstance = $conf['suround_with_inline_tags']['openedByCurrentInstance'];
            $conf['suround_with_inline_tags']['openedByCurrentInstance'] = 0;
        }

        if ($openedParagr || $flags & WHATAHTML_SURROUNDING)
            $paragrOpenedBeforeInstance = true;
        else
            $paragrOpenedBeforeInstance = false;

        if ($flags & WHATAHTML_SURROUNDING)
            $flags ^= WHATAHTML_SURROUNDING;
    }

    for ($i = 0,$len = count($dom); $i < $len; ++$i) {
        if (is_string($dom))
            print_r(debug_backtrace());
        $node = & $dom[$i];
        switch ($node['nodeType']) {
            case 'whitespace':
                if ($flags & WHATAHTML_PRE)
                    $html .= whata_inline($node['content'], $flags, $openedParagr, $mustclear, $conf);
                break;
            case 'cdata':
                if ($flags & WHATAHTML_PRE) {
                    $html .= whata_inline($node['content'], $flags, $openedParagr, $mustclear, $conf);
                } else if ($flags & WHATAHTML_INLINE) {
                    $html .= whatahtml_tag('code', $node, $flags | WHATAHTML_PRE | WHATAHTML_IS_BLOCK, $mustclear, $openedParagr, $conf, array ('class' => 'whata-cdata'));
                } else {
                    closeParagr($html, $openedParagr, $conf);
                    $html .= whatahtml_tag('pre', $node, $flags | WHATAHTML_PRE | WHATAHTML_INLINE, $mustclear, $openedParagr, $conf, array ('class' => 'whata-cdata'));
                }
                break;
            case 'text':
            case 'esc':
                $html .= whata_inline($node['content'], $flags, $openedParagr, $mustclear, $conf);
                break;

            case 'entity':
                if ($node['content'] === 'nl') {
                    $html .= whatahtml_tag('br', array ('attr' => array ()), $flags, $mustclear, $openedParagr, $conf, array (1));
                } else if ($node['content'] === 'nbsp') {
                    $html .= utf8_encode("\xA0");
                } else {
                    openParagr($html, $flags, $openedParagr, $mustclear, $conf);

                    $html .=  '<span class="whata-warning whata-entity">' . whata_inline('[' . htmlspecialchars($node['content']) . ']', $flags, $openedParagr, $mustclear, $conf) . '</span>';
                }
                break;
            case 'nl':
                if ($openedParagr) {
                    if ($node['count'] > 1)
                        closeParagr($html, $openedParagr, $conf);
                    elseif ($flags & WHATAHTML_PRE)
                        $html .= whata_inline($node['content'], $flags, $openedParagr, $mustclear, $conf);
                    elseif ($i+1 < $len && !($i + 2 === $len && $dom[$i+1]['nodeType'] === 'whitespace')) {
                        $html .= (isset($conf['explicitNL']) && $conf['explicitNL']) ? "\n" : '<br />';
                    }
                } else if ($html && ($flags & WHATAHTML_DO_NOT_AUTO_OPEN_PARAGR)) {
                    if ($flags & WHATAHTML_PRE)
                        $html .= whata_inline($node['content'], $flags, $openedParagr, $mustclear, $conf);
                    elseif ($i+1 < $len && !($i + 1 === $len && $dom[$i+1]['nodeType'] === 'whitespace'))
                        $html .= (isset($conf['explicitNL']) && $conf['explicitNL']) ? "\n" : '<br />';

                    if (isset($conf['indent_inline']))
                        $html .= str_repeat(' ', $conf['indent_inline']);
                }
                break;

            case 'title':
                //FIXME duplicate code, see case 'tag' above, for title specified by tags.

                if ($flags & WHATAHTML_INLINE || $flags & WHATAHTML_IN_TITLE) {
                    $tagName = 'strong';
                    $addFlags = WHATAHTML_INLINE;
                } else {
                    closeParagr($html, $openedParagr, $conf);

                    $lev = min(6, ($flags & WHATAHTML_IN_SECTION ? 1 : $conf['min_title_level']) - 1 + $node['level']);
                    $tagName = 'h' . $lev;
                    $addFlags = WHATAHTML_IN_TITLE;
                }

                $htmlTitle = whatahtml_tag($tagName, $node, $flags | $addFlags | WHATAHTML_DO_NOT_AUTO_OPEN_PARAGR | WHATAHTML_IS_BLOCK, $mustclear, $openedParagr, $conf, array ('class' => 'whata-title whata-' . ($node['ordered'] ? 'ordered' : 'unordered') . '-title whata-h' . $node['level'] . ' whata-real-title-level-' . $node['realLevel']));
                $html .= $node['ordered']
                     ? substr_replace (
                           $htmlTitle,
                           '><span class="whata-title-number">' . whata_title_number($node, $conf) . ' </span>',
                           strpos($htmlTitle, '>'),
                           1
                        )
                     : $htmlTitle;
                break;
            case 'section':
                if ($node['sectionType']) {
                    if ($flags & WHATAHTML_INLINE) {
                        $tagName = 'span';
                    } else {
                        closeParagr($html, $openedParagr, $conf);
                        if ($whata_global_conf['HTML5'])
                            $tagName = 'section';
                        else
                            $tagName = 'div';
                    }

                    $html .= whatahtml_tag($tagName, $node, $flags | ($flags & WHATAHTML_INLINE ? 0 : WHATAHTML_IS_BLOCK) | ($tagName === 'section' ? WHATAHTML_IN_SECTION : 0), $mustclear, $openedParagr, $conf, array ('class' => 'whata-section whata-section-' . $node['sectionType']));
                } else {
                    closeParagr($html, $openedParagr, $conf);
                    $html .= whatadom2html($node['content'], $flags, $conf, $openedParagr);
                }
                break;
            case 'listitem':
                if ($flags & WHATAHTML_INLINE) { // FIXME NOT TESTED ! should never happen in normal conditions
                    if (isset($conf['indent_inline']))
                        $indent = $conf['indent_inline'];
                    else
                        $indent = 0;

                    $childConf = $conf;
                    $childConf['indent'] = $indent + 4;

                    $begin = $i - 1;

                    while ($i < $len && $dom[$i]['nodeType'] === 'listitem' && $dom[$i]['bullet'] === $node['bullet']) {
                        $node = $dom[$i];
                        $html .= str_repeat(' ', 1 + $indent) . ($node['bullet'] === '#' ? $i-$begin . '.' : $node['bullet']) . ' ' . whatadom2html($node['content'], $flags, $childConf, $openedParagr) . '<br />';
                        ++$i;
                    }

                    $html .= str_repeat(' ', 1 + $indent); // FIXME : useless in certain conditions.
                } else {
                    closeParagr($html, $openedParagr, $conf);
                    $bullet = $node['bullet'];

                    if ( ($noOpenList = (isset($conf['noOpenList']) && $conf['noOpenList']))) {
                        unset($conf['noOpenList']);
                    } else if ($bullet === '#') {
                        $html .= '<ol>';
                    } else {
                        $html .= '<ul>';
                    }

                    while ($i < $len && $dom[$i]['nodeType'] === 'listitem' && $dom[$i]['bullet'] === $node['bullet']) {
                        $html .= '<li>' . whatadom2html($dom[$i]['content'], $flags | WHATAHTML_IS_BLOCK, $conf, $openedParagr) . '</li>';
                        ++$i;
                    }
                    --$i;

                    if($noOpenList) {
                        $conf['list-descr'] = false;
                    } else if ($bullet === '#') {
                        $html .= '</ol>';
                    } else {
                        $html .= '</ul>';
                    }
                }
                break;
            case 'table':
                if ($flags & WHATAHTML_INLINE) { //FIXME NOT TESTED + bad output (but should not happen in normal conditions)
                  // could adapt and use this : http://tonylandis.com/php/php-text-tables-class/
                  // if so, take alignement, headers and allow formating in the table.

                    if (isset($conf['indent_inline']))
                        $indent = $conf['indent_inline'];
                    else
                        $indent = 0;

                    $html .= '<br />' . str_repeat(' ', 1 + $indent) . '<b>Table :</b>';

                    $childConf = $conf;
                    $childConf['indent_inline'] = $indent + 4;

                    for ($line = 0, $tlen = count($tcontent =$node['content']); $line < $tlen; ++$line) {
                        $html .= '<br />' . str_repeat(' ', 2 + $indent) . '<b>Line ' . ($line+1) . ' :</b>';
                        for ($cell = 0, $llen = count($lcontent = $tcontent[$line]); $cell < $llen; ++$cell) {
                            $html .= '<br />' . str_repeat(' ', 3 + $indent)
                                . '<b>Cell ' . ($line+1) . '.' . ($cell+1) . ' :</b>';

                            $html .= '<br />' . str_repeat(' ', 4 + $indent)
                                . whatadom2html($lcontent[$cell]['content'], $flags, $childConf, $openedParagr);
                        }
                    }
                } else {
                    closeParagr($html, $openedParagr, $conf);
                    $html .= '<table class="whata-table">';

                    for ($line = 0, $tlen = count($tcontent = $node['content']); $line < $tlen; ++$line) {
                        $html .= '<tr>';
                        for ($cell = 0, $llen = count($lcontent = $tcontent[$line]); $cell < $llen; ++$cell) {
                            if ($lcontent[$cell]['head'])
                                $html .= '<th';
                            else
                                $html .= '<td';

                            if ($lcontent[$cell]['align'] !== 'auto')
                                $html .= ' style="text-align:' . $lcontent[$cell]['align'] . '"';

                            if ($lcontent[$cell]['rowspan'] !== 1)
                                $html .= ' rowspan="' . $lcontent[$cell]['rowspan'] . '"';

                            if ($lcontent[$cell]['colspan'] !== 1)
                                $html .= ' colspan="' . $lcontent[$cell]['colspan'] . '"';

                            $html .= '>' . whatadom2html($lcontent[$cell]['content'], $flags | WHATAHTML_DO_NOT_AUTO_OPEN_PARAGR, $conf);
                                // FIXME : is the WHATAHTML_DO_NOT_AUTO_OPEN_PARAGR flag a good choice ?
                            if ($lcontent[$cell]['head'])
                                $html .= '</th>';
                            else
                                $html .= '</td>';
                        }
                        $html .= '</tr>';
                    }

                    $html .= '</table>';
                }
                break;
            case 'tagList':
                $html .= whatadom2html($node['content'], $flags, $conf, $openedParagr, true);
                break;
            case 'tag':
                $name = $node['tagName'];
                $tag2html = '';

                $node['customAttributes'] = array();

                if (isset($conf['customTags']) && function_exists($conf['customTags']) && $conf['customTags']($node, $flags, $mustclear, $openedParagr, $conf, $tag2html)) {
                    $html .= $tag2html;
                } else {
                    switch ($name) {
                    case 'c': //code (inline pre)
                        $name = 'code';
                    case 'i': // italic
                    case 'b': // bold
                    case 'u': //underline
                    case 's': //strike
                        /* FIXME using the <u> tag for underline because I think it's the right way to do this.
                            Author means it, it is written as-is in the source so don't use CSS there.
                            It also allows underline in non CSS browsers. */
                    case 'mark': // mark
                        $html .= whatadom2html_surounding_tags($node, $flags, $conf, $name, $openedParagr);
                        break;

                    case '*': // em
                        $html .= whatadom2html_surounding_tags($node, $flags, $conf, 'em', $openedParagr);
                        break;
                    case '**': // strong
                        $html .= whatadom2html_surounding_tags($node, $flags, $conf, 'strong', $openedParagr);
                        break;
                    case '+': // add
                        $html .= whatadom2html_surounding_tags($node, $flags, $conf, 'ins', $openedParagr);
                        break;
                    case '-': // del
                        $html .= whatadom2html_surounding_tags($node, $flags, $conf, 'del', $openedParagr);
                        break;
                    case '_': // sub
                        $html .= whatadom2html_surounding_tags($node, $flags, $conf, 'sub', $openedParagr);
                        break;
                    case '^': //sup
                        $html .= whatadom2html_surounding_tags($node, $flags, $conf, 'sup', $openedParagr);
                        break;
                    case 'q': // inline quote
                        openParagr($html, $flags, $openedParagr, $mustclear, $conf);

                        if (empty($node['value'])) // url
                            $url = array ();
                        else
                            $url = array ('cite' => $node['value']);

                        $html .= whatahtml_tag($name, $node, $flags | WHATAHTML_INLINE, $mustclear, $openedParagr, $conf, $url);
                        break;

                    case 'in': //user input
                    case 'kbd': // HTML alias

                        openParagr($html, $flags, $openedParagr, $mustclear, $conf);
                        $html .= whatahtml_tag('kbd', $node, $flags | WHATAHTML_INLINE, $mustclear, $openedParagr, $conf, $url);
                        break;

                    case 'abbr': // abbreviation
                        openParagr($html, $flags, $openedParagr, $mustclear, $conf);

                        if (isset($node['value']) && $node['value']) {
                            $node['text'] = $node['value'];
                        } else {
                            for ($l = 0, $abbrLen = strlen($node['value']); $l < $abbrLen; ++$l) {
                                $n = ord($node['value'][$l]);
                                if ($n > 64 && $n < 91)
                                    $node['text'] .= $node['value'][$l];
                            }
                        }

                        $html .= whatahtml_tag($name, $node, $flags | WHATAHTML_INLINE, $mustclear, $openedParagr, $conf, array ('title' => $node['content']));
                        break;

                    case 'dfn':
                        openParagr($html, $flags, $openedParagr, $mustclear, $conf);
                        if ($node['value'])
                            $attrs = array ('title' => $node['value']);
                        else
                            $attrs = array ();

                        $html .= whatahtml_tag('dfn', $node, $flags | WHATAHTML_INLINE, $mustclear, $openedParagr, $conf, $attrs);
                        break;

                    //The following tags are directly mapped to HTML
                    case 'cite':
                    case 'samp': // HTML alias of Whata's "out" tag
                    case 'small':
                    case 'var':
                        openParagr($html, $flags, $openedParagr, $mustclear, $conf);
                        $html .= whatahtml_tag($name, $node, $flags | WHATAHTML_INLINE, $mustclear, $openedParagr, $conf);
                        break;

                    case 'cmt':
                        break;

                    case 'time': // HTML 5 <time>'s pubDate support might be added at some point in the future
                        if ($node['value']) // value must respect HTML5 <time>'s datetime attribute format.
                            $attr = array ('datetime' => $node['value']);
                        else
                            $attr = array (); // $node['content'] must respect HTML 5 <time>'s content format, when attribute datetime is not specified

                        openParagr($html, $flags, $openedParagr, $mustclear, $conf);
                        $html .= whatahtml_tag($name, $node, $flags | WHATAHTML_INLINE, $mustclear, $openedParagr, $conf, $attr);
                        break;

                    case 'out': //program output - inline
                        openParagr($html, $flags, $openedParagr, $mustclear, $conf);
                        $html .= whatahtml_tag('samp', $node, $flags | WHATAHTML_INLINE, $mustclear, $openedParagr, $conf);
                        break;

                    case 'output': //program output - block
                        closeParagr($html, $openedParagr, $conf);
                        $html .= whatahtml_tag('pre', $node, $flags | WHATAHTML_PRE | WHATAHTML_INLINE |WHATAHTML_IS_BLOCK, $mustclear, $openedParagr, $conf, array ('class' => 'whata-output'));
                        break;

                    case 'input': //program input - block
                        closeParagr($html, $openedParagr, $conf);
                        $html .= whatahtml_tag('pre', $node, $flags | WHATAHTML_PRE | WHATAHTML_INLINE | WHATAHTML_IS_BLOCK, $mustclear, $openedParagr, $conf, array ('class' => 'whata-input'));
                        break;

                    case 'pre': // pre-formated text
                        closeParagr($html, $openedParagr, $conf);
                        $html .= whatahtml_tag($name, $node, $flags | WHATAHTML_PRE | WHATAHTML_INLINE | WHATAHTML_DO_NOT_AUTO_OPEN_PARAGR, $mustclear, $openedParagr, $conf);
                        break;

                    case 'quote': // block quote
                        closeParagr($html, $openedParagr, $conf);

                        $node['text'] = '';

                        $source_header_show = false;
                        if (empty($node['value'])) { // url
                            $cite = '';
                        } else {
                            $url = $node['value'];
                            $cite = ' cite="' . htmlspecialchars($url) . '"';

                            if ($whata_global_conf['SHOW_QUOTE_HEADER'] && $whata_global_conf['QUOTE_HEADER_BEFORE']) {
                                $node['text'] = $node['text'] .
                                        '<p class="whata-quote-header"> ' .
                                            $whata_global_conf['SOURCE_LABEL'] .
                                        '<a href="' . rawurlencode($url) . '">' . htmlspecialchars($url) . '</a></p>';
                            } else {
                                $source_header_show = true;
                            }
                        }

                        $undefined = array ();

                        $node['text'] = $node['text'] .
                                    '<blockquote' . $cite . '>' .
                                        whatadom2html($node['content'], $flags, $conf, $openedParagr) .
                                    '</blockquote>';

                        if ($source_header_show) {
                            $node['text'] .= '<p class="whata-quote-header"> ' .
                                        $whata_global_conf['SOURCE_LABEL'] .
                                        '<a href="' . rawurlencode($url) . '">' . htmlspecialchars($url) . '</a></p>';
                        }

                        $html .= whatahtml_tag('div', $node, $flags | WHATAHTML_IS_BLOCK, $mustclear, $openedParagr, $conf, array ('class' => 'whata-quote'));
                        break;

                    case 'whata':
                        openParagr($html, $flags, $openedParagr, $mustclear, $conf);
                        $node['text'] = 'Whata!';
                        $html .= whatahtml_tag('em', $node, $flags, $mustclear, $openedParagr, $conf); //FIXME: incorect if e.g. "[* [whata]]" : => "<em><em>Whata!</em></em>"
                        break;

                    case 'img':
                        if (!isset($node['attributes']['float']))
                            openParagr($html, $flags, $openedParagr, $mustclear, $conf);

                        if (!empty($node['content'])) {
                            $alt = trim(whata_text_content($node['content']));
                            $attr = array ('title' => $alt, 'alt' => $alt, 'src' => whata_image_url($node['value'], $conf),true);
                        } else {
                            $attr=array ('alt' => '','src' => whata_image_url($node['value'], $conf),true);
                        }

                        if (isset($node['attributes']['scale'])) {
                            // width > 1 because onload, for at least firefox nightly (may 2013) width == 1. (??)
                            $attr['onload'] = 'if (this.width>1) {this.width*=' . floatval($node['attributes']['scale']) . ';}else if (event.type === "load") {var t=this;setTimeout(function() {t.onload, 100})}';
                        }

                        $html .= whatahtml_tag('img', $node, $flags, $mustclear, $openedParagr, $conf, $attr);
                        break;

                    case 'wbr': //FIXME: forbid wbr where it's not relevant
                        $html .= '<wbr />';
                        break;

                    case 'br': //FIXME: forbid br where it's not relevant
                        if ($mustclear) {
                            $html .= '<br style="clear:' . $mustclear . '" />';
                            $mustclear = '';
                        } else {
                            $html .= '<br />';
                        }
                        break;
                    case 'clear':
                        closeParagr($html, $openedParagr, $conf);
                        $mustclear = true;
                        break;

                    case 'm':
                    case 'math':
                        openParagr($html, $flags, $openedParagr, $mustclear, $conf);
                    case 'M':
                    case 'Math':
                        if ($name[0] === 'M') {
                            closeParagr($html, $openedParagr, $conf);
                            $displayBlock = true;
                        } else {
                            $displayBlock = false;
                        }

                        $conf['needMath'] = true;

                        if (isset($conf['mathFunction'])) {
                            $fun = $conf['mathFunction'];
                        } else {
                            $fun = $whata_global_conf['MATH_FUNCTION'];
                        }
                        $tag_to_append=$fun(whata_text_content($node), $displayBlock, $conf);

                        if ($tag_to_append['autoclose'])
                            $tag_to_append['attr'][0] = true;

                        $node['text'] = $tag_to_append['innerHTML'];

                        $html .= whatahtml_tag($tag_to_append['name'], $node, $flags, $mustclear, $openedParagr, $conf, $tag_to_append['attr']);
                        break;

                    case 'latexEnv':
                        if ($name[0] === 'M') {
                            closeParagr($html, $openedParagr, $conf);
                            $displayBlock = true;
                        } else {
                            $displayBlock = false;
                        }

                        $conf['needMath'] = true;
                        $tag_to_append = $whata_global_conf['MATH_FUNCTION'](
                            '\\begin{' . $node['value'] . "}\n" .
                                whata_text_content($node) .
                            '\\end{' . $node['value'] . "}\n",
                            $displayBlock,
                            $conf
                        );

                        if ($tag_to_append['autoclose'])
                            $tag_to_append['attr'][0] = true;

                        $node['text'] = $tag_to_append['innerHTML'];

                        $html .= whatahtml_tag($tag_to_append['name'], $node, $flags, $mustclear, $openedParagr, $conf, $tag_to_append['attr']);
                        break;

                    case 'latexCmd':
                        if ($name[0] === 'M') {
                            closeParagr($html, $openedParagr, $conf);
                            $displayBlock = true;
                        } else {
                            $displayBlock = false;
                        }

                        $conf['needMath'] = true;
                        $nbParam = isset($node['attributes'][0])
                                     ? '[' . whata_text_content($node['attributes'][0]) . ']'
                                     : '';

                        $tag_to_append = $whata_global_conf['MATH_FUNCTION'](
                            '\\newcommand{\\' . $node['value'] . '}' . trim($nbParam) . '{' .
                                whata_text_content($node) .
                            "}\n",
                            $displayBlock,
                            $conf
                        );

                        if ($tag_to_append['autoclose'])
                            $tag_to_append['attr'][0] = true;

                        $node['text'] = $tag_to_append['innerHTML'];

                        $html .= whatahtml_tag($tag_to_append['name'], $node, $flags, $mustclear, $openedParagr, $conf, $tag_to_append['attr']);
                        break;

                    case 'algorithmic':
                        if ($flags & WHATAHTML_INLINE) {
                            $html .= '<span class="whata-algorithmic">' . (isset($node['attributes']['title']) ? '<b class="whata-algorithmic-title">Algorithm : ' . htmlspecialchars($node['attributes']['title']) . '</b><br />':'') . whata_algorithmic(whata_text_content($node), $node, $flags, $mustclear, $openedParagr, $conf) . '</span>';
                        } else {
                            $html .= '<section class="whata-algorithmic"><div class="whata-algorithmic-content">' . (isset($node['attributes']['title']) ? '<h1 class="whata-algorithmic-title">Algorithm : ' . htmlspecialchars($node['attributes']['title']) . '</h1>':'') . whata_algorithmic(whata_text_content($node), $node, $flags, $mustclear, $openedParagr, $conf) . '</div></section>';
                        }
                        break;
                    case 'code':
                    case 'pcode':
                        closeParagr($html, $openedParagr, $conf);

                        if ($node['value'] === 'algo' || $name === 'pcode') {
                            $node['value'] = '';
                            $pcode = ' whata-pcode';
                        } else {
                            $pcode = '';

                            if (!class_exists('GeSHi') && $whata_global_conf['GESHI_URL'])
                                include($whata_global_conf['GESHI_URL']);
                        }

                        if (empty($node['value']) || !class_exists('GeSHi')) {
                            $node['disable_attrs'] = true;
                            $html .= '<pre' . whata_get_html_attributes_string($node, $flags | WHATAHTML_IS_BLOCK, array ('class' => 'whata-code-block whata-not-geshi' . $pcode), $conf) . '>' . whatahtml_tag('code', $node, $flags  | WHATAHTML_INLINE | WHATAHTML_DO_NOT_AUTO_OPEN_PARAGR | WHATAHTML_PRE, $mustclear, $openedParagr, $conf) . '</pre>';
                        } else {
                            $type = $node['value'];
                            if ($type==='html')
                                $type='html4strict';

                            $conf['needGeshi'] = true;

                            $geshi = new GeSHi(trim(whata_text_content($node)), $type);
                            $geshi->enable_classes();
                            // $geshi->enable_strict_mode(GESHI_MAYBE); // NOTICE ??
                            $geshi->set_header_type(GESHI_HEADER_PRE_TABLE);
                            $geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS);
                            $node['text'] = str_replace('&nbsp;', ' ', $geshi->parse_code());
                            $html .= whatahtml_tag('div',$node,$flags,$mustclear, $openedParagr,$conf,array ('class' => 'whata-code-block whata-geshi'));
                        }
                        break;

                    case ' ': //link. real whata tag name was WHATA_TAG_START when the document was parsed. See whatadom.php for more information.
                        $nflags = $flags;
                        if (!$openedParagr && !(
                            isset($dom[$i+1]) && (
                                    ($dom[$i+1]['nodeType'] === 'nl' && $dom[$i+1]['count'] > 1)
                                || (
                                        $dom[$i+1]['nodeType'] === 'whitespace'
                                    && isset($dom[$i+2]) && (
                                        $dom[$i+2]['nodeType'] === 'nl' && $dom[$i+2]['count'] > 1
                                    )
                                )
                            )
                        )) {// if we the link is followed by a whitespace or not and 2+ newlines, we do not open a new paragraph and the link can suround block. Else, the link is inline and we use whata surounding tags
                            openParagr($html, $flags, $openedParagr, $mustclear, $conf);
                        }

                        if (isset($node['attributes'][0])) {
                            $url = $node['attributes'][0];
                            whata_link($url, $node['content'], $conf);
                        } else {
                            $url = $node['content'];
                            $node['content'] = null;
                            $nflags |= WHATAHTML_INLINE;
                            whata_link($url, $node['content'], $conf);
                        }

                        if ($openedParagr && isset($node['attributes'][0]))
                            $html .= whatadom2html_surounding_tags($node, $nflags, $conf, 'a', $openedParagr, array ('href' => $url, 'class' => 'whata-link'));
                        else
                            $html .= whatahtml_tag('a',$node,$nflags,$mustclear, $openedParagr,$conf, array ('href' => $url, 'class' => 'whata-link')); // Note: we allow blocks elements inside <a>, as HTML5 does.
                        break;
                    case 'span':
                        openParagr($html, $flags, $openedParagr, $mustclear, $conf);

                        if (empty($node['value']))
                            $attr = array ();
                        else
                            $attr = array ('style' => 'display:inline-block;min-width:' . $node['value']); //FIXME dirty !

                        $html .= whatahtml_tag('span',$node,$flags | WHATAHTML_INLINE,$mustclear,$openedParagr,$conf, $attr);
                        break;

                    case 'color':
                        if (empty($node['value'])) {
                            $html .= whatadom2html($node['content'], $flags, $conf, $openedParagr); //we simply ignore the tag. Could change in the future. (e.g. set a defaut alternative color when no one was speciafied)
                        } else {
                            $attr = array ('style' => 'color:' . $node['value']); //FIXME dirty !

                            if ($openedParagr || $flags & WHATAHTML_DO_NOT_AUTO_OPEN_PARAGR)
                                $html .= whatadom2html_surounding_tags($node, $flags, $conf, 'span', $openedParagr, $attr);
                            else if ($flags & WHATAHTML_INLINE)
                                $html .= whatahtml_tag('span',$node,$flags,$mustclear, $openedParagr, $conf, $attr);
                            else
                                $html .= whatahtml_tag('div',$node,$flags,$mustclear, $openedParagr, $conf, $attr);
                        }
                        break;

                    case 'align':

                        closeParagr($html, $openedParagr, $conf);

                        if ($flags & WHATAHTML_INLINE || empty($node['value'])) {
                            $html .= whatadom2html($node['content'], $flags, $conf); //we simply ignore the tag. Could change in the future. (e.g. set a defaut alternative color when no one was speciafied)
                        } else {
                            $attr = array ('style' => 'text-align:' . $node['value']); //FIXME dirty !
                            $html .= whatahtml_tag('div',$node,$flags | WHATAHTML_IS_BLOCK,$mustclear, $openedParagr,$conf, $attr);
                        }
                        break;

                    case 'div': //div and float are equivalent. $node['value'] defines CSS's float property
                    case 'float': // However, paragraphs are not auto-opened in float elements.
                    case 'illustr': // illustr is float class="whata-illust"
                        if ($node['value'])
                            $attr = array ('class' => $name === 'illustr' ? 'whata-illustr' : 'whata-div', 'style' => 'float:' . $node['value']); //FIXME : dirty !
                        else
                            $attr = array ('class' => $name === 'illustr' ? 'whata-illustr' : 'whata-div');

                        if ($flags & WHATAHTML_INLINE) {
                            $html .= whatahtml_tag('span',$node,$flags,$mustclear, $openedParagr,$conf, $attr);
                        } else {
                            closeParagr($html, $openedParagr, $conf);
                            if ($name === 'float' || $name === 'illustr')
                                $html .= whatahtml_tag('div',$node,$flags | WHATAHTML_DO_NOT_AUTO_OPEN_PARAGR | WHATAHTML_IS_BLOCK,$mustclear, $openedParagr, $conf, $attr);
                            else
                                $html .= whatahtml_tag('div',$node,$flags | WHATAHTML_IS_BLOCK,$mustclear, $openedParagr,$conf, $attr);
                        }
                        break;

                    case 'p':
                        if ($flags & WHATAHTML_INLINE || $flags & WHATAHTML_PRE) {
                            $html .= whatadom2html($node['content'], $flags, $conf, $openedParagr); //we simply ignore the tag.
                        } else {
                            closeParagr($html, $openedParagr, $conf);
                            if (isset($node['attributes'][0])) // paragraph title
                            {
                                $node['text'] = '<strong class="whata-paragraph-title">' .
                                        trim(whatadom2html($node['attributes'][0], $flags | WHATAHTML_INLINE, $conf)) .
                                        '</strong> ' .
                                        whatadom2html($node['content'], $flags | WHATAHTML_INLINE, $conf, $openedParagr);
                            }
                            $html .= whatahtml_tag('p',$node,$flags | WHATAHTML_IS_BLOCK | WHATAHTML_INLINE,$mustclear, $openedParagr, $conf);
                        }

                        break;
                    case 'footnote':
                        if (!isset($conf['footnotes'])) {
                            $conf['footnotes'] = array ();
                        }
                        $nopenedParagr = false;
                        $conf['footnotes'][] = whatadom2html($node['content'], WHATAHTML_IS_BLOCK, $conf, $nopenedParagr); //we simply ignore the tag.

                        $footrefPrefix = whatahtml_get_footnote_prefix($conf);

                        $id = count($conf['footnotes']);
                        $html .= '<sup class="whata-footref" id="whata-footref-' . $footrefPrefix . $id . '"><a href="#whata-footnote-' . $footrefPrefix . $id . '">[' . $id . ']</a></sup>';
                        break;
                    case 'part':
                        closeParagr($html, $openedParagr, $conf);
                        if(isset($conf['processPart'])) {
                            $html .= $conf['processPart'](-1, $n, $flags, $conf);
                        } else if ($flags & WHATAHTML_INLINE || $flags & WHATAHTML_PRE) {
                            $html .= '<span class="whata-part-inline"><br /><span class="whata-part-label">Part: </span>' . whata_part($node, $flags, $conf) . '<br /></span>';
                        } else {
                            $html .= '<p class="whata-part"><span class="whata-part-label">Part: </span>' . whata_part($node, $flags, $conf) . '</p>';
                        }
                        break;
                    case 'partlist':
                        closeParagr($html, $openedParagr, $conf);

                        if (isset($conf['processPartListBegin'])) {
                            $html .= $conf['processPartListBegin']($node, $flags, $conf);
                        } else if ($flags & WHATAHTML_INLINE || $flags & WHATAHTML_PRE) {
                            $html .= '<span class="whata-parts-inline"><span class="whata-parts-descr">' . (isset($node['attributes'][0]) ? whatadom2html($node['attributes'][0], $flags, $conf) : 'Parts:') . '</span>';
                        } else {
                            if(isset($node['attributes'][0]))
                                $html .= whatadom2html($node['attributes'][0], $flags, $conf, $openedParagr);
                            $html .= '<ol class="whata-part-list">';
                        }

                        if(isset($conf['processPart'])) {
                            foreach ($node['content'] as $n) {
                                $partNumber = 0;
                                if ($n['nodeType'] === 'tag' && $n['tagName'] === 'part') {
                                    $html .= $conf['processPart']($partNumber, $n, $flags, $conf);
                                    ++$partNumber;
                                }
                            }
                        } else if ($flags & WHATAHTML_INLINE || $flags & WHATAHTML_PRE) {
                            $comma = false;
                            foreach ($node['content'] as $n) {
                                if ($n['nodeType'] === 'tag' && $n['tagName'] === 'part') {
                                    if ($comma)
                                        $html .= ', ';
                                    else
                                        $comma = true;

                                    $html .= whata_part($n, $flags, $conf);
                                }
                            }
                        } else {
                            foreach ($node['content'] as $n) {
                                if ($n['nodeType'] === 'tag' && $n['tagName'] === 'part') {
                                    // let's ignore anything else
                                    $html .= '<li>' . whata_part($n, $flags, $conf) . '</li>';
                                }
                            }
                        }

                        if (isset($conf['processPartListEnd'])) {
                            $html .= $conf['processPartListEnd']($node, $flags, $conf);
                        } else if ($flags & WHATAHTML_INLINE || $flags & WHATAHTML_PRE) {
                            $html .= '</span>';
                        } else {
                            $html .= '</ol>';
                        }

                        break;
                    case 'list':
                        closeParagr($html, $openedParagr, $conf);

                        $attr = array();
                        $nodeName = 'ul';

                        if (!empty($node['value'])) {
                            if ($node['value'] === 'ordered' || $node['value'] === 'numbered') {
                                $nodeName = 'ol';
                            } else {
                                if (array_search($node['value'], $orderedListStyles) !== FALSE) {
                                    $nodeName = 'ol';
                                }

                                if ($node['value'] !== 'descr' && $node['value'] !== 'ordered' && $node['value'] !== 'numbered') {
                                    $attr['style'] = 'list-style-type:' . htmlspecialchars($node['value'], ENT_QUOTES);
                                }
                            }
                        }

                        $conf['noOpenList'] = true;
                        $node['text'] = whatadom2html($node['content'], $flags, $conf, $openedParagr);

                        if (isset($conf['list-descr']) && $conf['list-descr']) {
                            $attr['class'] = 'whata-list-descr';
                            unset($conf['list-descr']);
                        }

                        $html .= whatahtml_tag($nodeName, $node, $flags, $mustclear, $openedParagr, $conf, $attr);
                        unset($conf['noOpenList']);
                        break;
                    case 'item':
                        $attr = array();

                        if (!empty($node['value'])) {
                            $attr['class'] = 'whata-custom-list-item';
                        }

                        $prefix = '';
                        $suffix = '';
                        $listDescr = isset($conf['list-descr']) ? $conf['list-descr'] : null;
                        unset($conf['list-descr']);
                        if (isset($node['attributes'][0])) {
                            if($listDescr !== false) {
                                $listDescr = true;
                            }
                            $attr['style'] = 'list-style-type:none';
                            $prefix = '<strong class="whata-custom-list-item-value">' . whatadom2html($node['attributes'][0], WHATAHTML_INLINE, $conf) . '</strong><div class="whata-custom-list-item-content">';
                            $suffix = '</div>';
                        } else {
                            $listDescr = false;
                        }

                        if (isset($node['attributes']['style'])) {
                            $attr['style'] = 'list-style:' . htmlspecialchars($node['attributes']['style'], ENT_QUOTES);
                        }
                        $html .= whatahtml_tag('li', $node, $flags, $mustclear, $openedParagr, $conf, $attr, array('prefix'=>$prefix, 'suffix'=>$suffix));
                        $conf['list-descr'] = $listDescr;
                        break;
                    case 'ref':
                        $html .= whatahtml_tag('a', $node, $flags, $mustclear, $openedParagr, $conf, array('id' => $conf['id_prefix'] . $node['value']));
                        break;
                    case 'comment':
                        break; // we ignore the comment tag
                    default:
                        if ($name && $name[0] === 'h' && ($level = intval($name[1]))) {// title ?
                            //FIXME duplicate code, see case 'title' above.
                            if ($flags & WHATAHTML_INLINE || $flags & WHATAHTML_IN_TITLE) {
                                $tagName = 'strong';
                                $addFlags = WHATAHTML_INLINE;
                            } else {
                                closeParagr($html, $openedParagr, $conf);

                                $lev = min(6, $conf['min_title_level'] - 1 + $level);
                                $tagName = 'h' . $lev;
                                $addFlags = WHATAHTML_IN_TITLE;
                            }
                            $html .= whatahtml_tag($tagName, $node, $flags | $addFlags | WHATAHTML_DO_NOT_AUTO_OPEN_PARAGR | WHATAHTML_IS_BLOCK, $mustclear, $openedParagr, $conf, array ('class' => 'whata-h' . $lev));
                        } else if (isset($node['attributes']['align']) && !($flags & WHATAHTML_INLINE)) { // unknown tag but align attribute specified
                            $html .= whatahtml_tag('div',$node,$flags | WHATAHTML_IS_BLOCK,$mustclear, $openedParagr, $conf);
                        } else { // unknown tag, tag content and let's behave as if there were not the tag.
                            $html .= whatadom2html($node['content'], $flags, $conf, $openedParagr);
                        }

                    //FIXME implement 'table' and 'list' tags
                }
            }
        }
    }

    if (!$light) {
        if ($surround && $conf['suround_with_inline_tags']['openedByCurrentInstance']) {//We close the surrounding tags opened by this instance of whatadom2html. Crappy but seems to work

            while ($conf['suround_with_inline_tags']['openedByCurrentInstance'] && $conf['suround_with_inline_tags']['inside']) {
                $html .= $conf['suround_with_inline_tags'][--$conf['suround_with_inline_tags']['inside']]['after'];
                --$conf['suround_with_inline_tags']['openedByCurrentInstance'];
            }
        }
        if (!($flags & WHATAHTML_INLINE) && !$paragrOpenedBeforeInstance)
            closeParagr($html, $openedParagr, $conf);

        if ($surround)
            $conf['suround_with_inline_tags']['openedByCurrentInstance'] = $openedByCurrentInstance;
    }

    return $html;
}


function whata_get_html_attributes_string($node, $flags, $attr, $conf) {
    $s_attr='';

    if (isset($node['customAttributes'])) {
        // we merge attributes
        foreach ($node['customAttributes'] as $aname => $value) {
            if (isset($attr[$aname]) && $aname === 'class') {
                $attr['class'] .= ' ' . $value;
            } else {
                $attr[$aname] = $value;
            }
        }
    }

    if (isset($node['attributes']['class'])) {
        if (isset($attr['class']))
            $attr['class'] = $attr['class'] . ' ' . $node['attributes']['class'];
        else
            $attr['class'] = $node['attributes']['class'];
    }

    if (isset($node['attributes']['color'])) {
        if (isset($attr['style']))
            $attr['style'] = $attr['style'] . ';color:' . $node['attributes']['color'];
        else
            $attr['style'] = 'color:' . $node['attributes']['color'];
    }

    if (isset($node['attributes']['float'])) {
        if (isset($attr['style']))
            $attr['style'] = $attr['style'] . ';float:' . $node['attributes']['float'];
        else
            $attr['style'] = 'float:' . $node['attributes']['float'];
        if (isset($attr['class']))
            $attr['class'] = $attr['class'] . ' ' . 'float float-' . $node['attributes']['float'];
        else
            $attr['class'] = 'float float-' . $node['attributes']['float'];
    }

    if ($flags & WHATAHTML_IS_BLOCK) {
        if (isset($node['attributes']['align'])) {
            if (isset($attr['style']))
                $attr['style'] = $attr['style'] . ';text-align:' . $node['attributes']['align'];
            else
                $attr['style'] = 'text-align:' . $node['attributes']['align'];
        }
    }

    if (isset($node['attributes']['width']) && strlen($node['attributes']['width'])) {
        if ($node['tagName'] === 'img' && !letter($node['attributes']['width'][strlen($node['attributes']['width'])-1]))
            $attr['width'] = $node['attributes']['width'];
        elseif (isset($attr['style']))
            $attr['style'] = $attr['style'] . ';width:' . $node['attributes']['width'];
        else
            $attr['style'] = 'width:' . $node['attributes']['width'];
    }

    if (isset($node['attributes']['height']) && strlen($node['attributes']['height'])) {
        if ($node['tagName'] === 'img' && !letter($node['attributes']['height'][strlen($node['attributes']['height'])-1]))
            $attr['height'] = $node['attributes']['height'];
        elseif (isset($attr['style']))
            $attr['style'] = $attr['style'] . ';height:' . $node['attributes']['height'];
        else
            $attr['style'] = 'height:' . $node['attributes']['height'];
    }

    if (isset($node['attributes']['ref'])) {
        $attr['id'] = $conf['id_prefix'] . $node['attributes']['ref'];
    }

    foreach ($attr as $key => $value) {
        if (is_string($key))
            $s_attr .= ' ' . htmlspecialchars($key, ENT_QUOTES) . '="' . htmlspecialchars($value, ENT_QUOTES) . '"';
    }

    return $s_attr;
}

function whatadom2html_surounding_tags($node, $flags, &$conf, $htmlName, &$openedParagr, $htmlAttrs=array ()) {
    $s_attr=whata_get_html_attributes_string($node, $flags, $htmlAttrs, $conf);

    $entry = array (
        'before' => '<' . $htmlName . $s_attr . '>',
        'after'  => '</' . $htmlName . '>',
    );

    if (isset($conf['suround_with_inline_tags'])) {
        $conf['suround_with_inline_tags'][$conf['suround_with_inline_tags']['count'] ++] = $entry;
        $count = $conf['suround_with_inline_tags']['count'];
    } else {
        $conf['suround_with_inline_tags'] = array (
            'count'  => $count = 1,
            'inside' => 0,
            'openedByCurrentInstance' => 0,
            $entry
        );
    }

    $ret = whatadom2html($node['content'], $flags | WHATAHTML_SURROUNDING, $conf, $openedParagr);
    -- $conf['suround_with_inline_tags']['count'];

    return $ret;
}


function whatahtml_tag($tagName, $node, $flags, &$mustclear, &$openedParagr, & $conf, $attr = array (), $prefsuf = array()) {
    if (isset($node['disable_attrs']))
        $s_attr = '';
    else
        $s_attr=whata_get_html_attributes_string($node, $flags, $attr, $conf);

    if ($flags & WHATAHTML_IS_BLOCK)
        $flags ^= WHATAHTML_IS_BLOCK;

    if ($mustclear) {
        if (isset($attr['class']))
            $attr['class'] .= ' whata-tag-clear';
        else
            $attr['class'] = 'whata-tag-clear';

        if (isset($attr['style']))
            $attr['style'] .= ';clear:' . $mustclear;
        else
            $attr['style'] = 'clear:' . $mustclear;
    }

    $mustclear = '';

    if (isset($attr[0]))
        return '<' . $tagName . $s_attr . ' />';

    if (isset($node['text']))
        $content = $node['text'];
    else if (is_string($node['content']))
        $content = htmlspecialchars($node['content']);
    else
        $content = whatadom2html($node['content'], $flags, $conf);

    return '<' . $tagName . $s_attr . '>' . (isset($prefsuf['prefix']) ? $prefsuf['prefix'] : '') . $content . (isset($prefsuf['suffix']) ? $prefsuf['suffix'] : '') . '</' . $tagName . '>';
}

function whatahtml_process_footnotes($conf) {
   $footrefPrefix = whatahtml_get_footnote_prefix($conf);

   if (isset($conf['footnotes']) && count($conf['footnotes'])) {
      $html = '<section class="whata-footnotes"><p>Notes :</p><ul>';
      foreach ($conf['footnotes'] as $index => $footnote) {
         $html .= '<li id="whata-footnote-' . $footrefPrefix . ($index+1) . '" class="whata-footnote"><a class="whata-footnote-link" href="#whata-footref-' . $footrefPrefix . ($index+1) . '">[' . ($index+1). ']</a><div class="whata-footnote-content">' . $footnote . '</div></li>';
      }
      return $html . '</ul></section>';
   }
   else return '';
}

function whata2html($s,&$conf=array ()) {
    require_once('whata/whatadom.php');
    $html = whatadom2html(whataDOM($s, $conf), 0, $conf);
    if ((!isset($conf['processFootnotes']) || $conf['processFootnotes'])) {
        $html .= whatahtml_process_footnotes($conf);
    }
    return $html;
}
?>
