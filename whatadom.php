<?php
/*
        Copyright (C) 2012-2014 JAKSE Raphaël

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

// TODO: implement syntax {{image-filename | alternative text }} for Whata! simplex.

include_once(dirname(__FILE__) . '/config.php');

include_once('optimize_indentation.php');

if (!defined('WHATA_ATTR_START'))
    define('WHATA_ATTR_START', '<');

if (!defined('WHATA_ATTR_END'))
    define('WHATA_ATTR_END', '>');

if (!defined('WHATA_ATTR_SEP'))
    define('WHATA_ATTR_SEP', '|');

if (!defined('WHATA_ATTR_VALUE'))
    define('WHATA_ATTR_VALUE', '=');

if (!defined('WHATA_TITLE_CHAR'))
    define('WHATA_TITLE_CHAR', '=');

if (!defined('WHATA_TAG_START'))
    define('WHATA_TAG_START', '[');

if (!defined('WHATA_TAG_END'))
    define('WHATA_TAG_END', ']');

if (!defined('WHATA_CDATA_START'))
    define('WHATA_CDATA_START', '<');

if (!defined('WHATA_CDATA_END'))
    define('WHATA_CDATA_END', '>');

if (!defined('WHATA_ESCAPE_CHAR'))
    define('WHATA_ESCAPE_CHAR', '~');

if (!defined('WHATA_UNORDERED_TITLE_CHAR'))
   define('WHATA_UNORDERED_TITLE_CHAR', '.');

if (!defined('WHATA_LIST_SEPARATOR')) {
    define('WHATA_LIST_SEPARATOR', ',');
}

function whata_list_char($c) {
    return $c === '#' || $c === '*' || $c === '-';
}

// checks if the $c character is white
function is_white($c) {
    return $c === ' ' || $c === "\t" || $c === "\n"; // || $c === "\r"; // \r should not happen
}

function skipSpace(&$s, &$i, &$len) {
    while ($i < $len && (($c = $s[$i]) === ' ' || $c === "\t"))
        ++$i;
}

function whata_addTextContent(&$root, &$domLen, $txt) {
    $ttxt = trim($txt);
    if ($domLen) {
        $node = &$root[$domLen-1];
        if ($node['nodeType'] === 'text') {
            $node['content'] .= $txt;
            return;
        }
    }

    $countNL = 0;
    if ($ttxt) {
        $i = 0;

        $begining_of_the_line = $old_i = $i;

        skipSpace($s,$i,$len);
        if ($i - $begining_of_the_line)
            ++$countNL;

        while ($i < $len && $s[$i] === "\n") {
            ++$i;
            $old_i = $i;
            skipSpace($txt,$i,$len);
            ++$countNL;
        }

        $nodeType = $countNL ? 'nl' : 'text';
    } else {
        $nodeType = 'text';
    }

    $root[$domLen] = array('nodeType'=> $nodeType, 'content'=> $txt);

    if ($countNL) {
        $root[$domLen]['count'] = $countNL;
    }

    $domLen++;
}

function whata_text_content($node, $indent = 0) {
    $text = '';
    $txtIndent = str_repeat(' ', $indent);

    if (isset($node['nodeType']))
        $node = $node['content'];

    if (is_string($node))
        return $txtIndent . $node;


    for ($i=0, $len=count($node); $i < $len; ++$i) {
        $cur = $node[$i];
        switch ($cur['nodeType']) {
        case 'whitespace':
        case 'nl':
        case 'esc':
        case 'text':
        case 'cdata':
            $text .= str_replace("\n", "\n" .  $txtIndent, $cur['content']);
            break;
        case 'entity': // in this version of Whata, entity can only be a new line
            $text .= "\n" . str_repeat(' ', $indent);
            break;
        case 'section':
        case 'tagList':
            foreach ($cur['content'] as $tag) {
                $text .= whata_text_content($tag);
            }
            break;
        case 'tag':
            if ($cur['tagName'] === 'whata') {
                $text .= 'Whata!';
            } else {
                $text .= whata_text_content($cur, $indent + 4);
            }
            break;
        case 'title':
            $text .= "\n" . $txtIndent . str_repeat('=', $cur['level']) . ' ' . whata_text_content($cur, $indent);
            break;
        case 'listitem':
            $text .= "\n" . $txtIndent . $cur['bullet'] . ' ' . whata_text_content($cur, $indent + 4);
            break;
        case 'table': //FIXME: better table text rendering needed
            $content = $cur['content'];
            for ($rows=count($content), $r = 0; $r < $rows; ++$r) {
                $a_c = $rows[$r];
                for ($cols = count($a_c), $c=0; $c < $cols; ++$c) {
                    $text .= whata_text_content($a_c[$c], $indent);
                }
            }
        }
    }
    return $text;
}

function & whata_get_node_to_complete(&$root, &$domLen, $indenting, $listLevelRequired=0) {
    if ($listLevelRequired && $domLen) {
        $indenting = 0; // when using lists like : " - one \n --two \n --- three" we ignore indenting
    } else if (!$indenting || !$domLen) {
        return $root;
    }

    $nextParent = & $root[$domLen - 1];
    $curParent = & $root;

    $lev = 0;

    while ($nextParent['nodeType'] === 'listitem' && ($nextParent['indent'] < $indenting || $listLevelRequired > $lev)) {
        $curParent = & $nextParent['content'];

        $j = count($curParent) - 1;
        if ($j > -1)
            $nextParent = & $curParent[$j];
        else
            break;

        ++$lev;
    }

    if ($listLevelRequired && $lev !== $listLevelRequired) {
        $tmp = NULL;
        return $tmp; // fail
    }

    return $curParent;
}

function whata_cdata($s,&$i,$len) {
    $countChars=0;
    while ($countChars + $i < $len && $s[$i + $countChars] == WHATA_CDATA_START) {
        ++$countChars;
    }

    if ($countChars < 3)
        return false;

    $i+=$countChars;
    $cdata_begin = $i;
    $begining_of_the_end = 0;
    while ($i < $len) {
        if ($s[$i] == WHATA_CDATA_END) {
            $begining_of_the_end = $i;
            while ($i < $len && $s[$i] == WHATA_CDATA_END)
                ++$i;

            if ($i - $begining_of_the_end < $countChars) {
                $begining_of_the_end = 0;
            } else {
                if ($i < $len)
                    ++$i;
                break;
            }
        }
        ++$i;
    }

    if (!$begining_of_the_end)
        $begining_of_the_end = $i;

    $length = $begining_of_the_end - $cdata_begin;
    $cdata_content = substr($s, $cdata_begin, $length);
    $cdata_trimed_content = ltrim(rtrim($cdata_content, "\t "), "\t "); // we remove space and tab characters at the begining and the end
    $nlength=strlen($cdata_trimed_content);

    if ($cdata_trimed_content[0] === "\n" && $cdata_trimed_content[$nlength-1] === "\n") {
        $length = $nlength - 2;
        $cdata_content = optimize_indentation(substr($cdata_trimed_content, 1, $length), $length);
    }

    return array('nodeType'=>'cdata', 'content'=>$cdata_content, 'length'=>$length, 'cdata'=> $countChars);
}

function whata_image_url($src, $conf) {
    $prefix = isset($conf['imgdir']) ? $conf['imgdir'] : (isset($conf['basedir']) ? $conf['basedir'] : '');

    if (preg_match('#^[a-zA-Z]+:|/#', $src))
        return $src;
    else
        return $prefix . $src;
}

function is_absolute_path($url) {
    return $url[0] === '/'  || preg_match('/^(?:[a-zA-Z]+\:\\|[a-zA-Z]+\:\/\/)/', $url);
}

function whata_get_file_path($whata_url, $conf, $anyType = false) {
    if (isset($conf['workingDir']) && !is_absolute_path($whata_url)) {
        $whata_url = $conf['workingDir'] . '/' . $whata_url;
    }
    return $whata_url . ($anyType ? '' : '.whata');
}

function whata_get_file_contents($whata_url, $conf, $anyType = false) {
    return file_get_contents(whata_get_file_path($whata_url, $conf, $anyType));
}

function whata_link(&$url, &$content, $conf) {
    $lang = isset($conf['lang']) ? $conf['lang'] : 'en';

    if (!is_string($url))
        $url = trim(whata_text_content($url));

    $e_url = explode(' ', $url, 2);
    $e_url_0 = explode(':', $e_url[0], 2);

    if ($e_url_0[0] === 'wp' || $e_url_0[0] === 'wiki') { // Wikipedia
        if (isset($e_url_0[1]))
            $lang = strtolower($e_url_0[1]);

        if (isset($e_url[1]))
            $link = $e_url[1];
        else // FIXME:syntax error
            $link = '';
        $url = 'http://' . $lang . '.wikipedia.org/wiki/' . rawurlencode($link);
    } else if ($e_url_0[0] === 'php') {
        if (isset($e_url[1]))
            $link = $e_url[1];
        else // FIXME:syntax error
            $link = '';
        $url = 'http://php.net/' . rawurlencode($link);
    } else if ($e_url_0[0] === 'doc' || $e_url_0[0] === 'part') {
        $url =  (
            isset($conf['linkToWhataPrefix'])
                ? $conf['linkToWhataPrefix']
                : ''
        ) . $e_url_0[1] . (
            isset($conf['linkToWhataSuffix'])
                ? $conf['linkToWhataSuffix']
                : ((isset($conf['xhtml']) && $conf['xhtml']) ? '.xhtml' : '.html')
        );

        if (isset($conf['docRefCallback'])) {
            $conf['docRefCallback'](whata_get_file_path($e_url_0[1], $conf), $url);
        }

        if (empty($content) || $content === null) {
            $values = whataGetValues(whata_get_file_contents($e_url_0[1], $conf));
            if(isset($values['title'])) {
                $content = $values['title'];
            }
        }
    } else if ($e_url_0[0] === 'ref') {
        $url =  '#' . (isset($conf['id_prefix']) ? $conf['id_prefix'] : '') . $e_url_0[1];
        $link = $e_url_0[1];
        if ((empty($content) || $content === null) && isset($conf['refs'][$e_url_0[1]]['content'])) {
            $content = $conf['refs'][$e_url_0[1]]['content'];
        }

    } /* else if (rawurldecode($url) === $url) {
        $url = ((isset($conf['basedir']) && !preg_match('#^(?:[a-zA-Z]+:|/)#', $url)) ? $conf['basedir'] : '') . str_replace(
            array('%3A', '%2F', '%23', '%5B', '%5D', '%40', '%21', '%24', '%26', '%27','%28', '%29', '%2A', '%2B', '%2C', '%3B', '%3D'),
            array(':'  , '/'  , '?'  , '#'  , '['  , ']'  , '@'  , '!'  , '$'  , '&'  , '\'', '('  , ')'  , '*'  , '+'  , ';'  , '='  ),
            rawurlencode($url)
        );
    } */

    if (empty($content) || $content === null) {
        $content = array(
            array(
                'nodeType' => 'text',
                'content'  => isset($link) ? $link : $url
            )
        );
    }
}

function whata_simplex2nodeName($n) {
    return str_replace(array('*',"'",'`','_','-','"'), array('b','i','c','u','s','mark'), $n);
}

function whata_split_values($list_string, $conf) {
    return array($list_string);
    $res = array();
    $i = 0;
    $item = '';
    $len = strlen($list_string);
    while ($i < $len) {
        if ($list_string[$i] === WHATA_ESCAPE_CHAR) {
            ++$i;
        } else if ($list_string[$i] === WHATA_LIST_SEPARATOR) {
            $res[] = $item;
            ++$i;
            $item = '';
        }

        $item .= $list_string[$i];
        ++$i;
    }

    if ($item) {
        $res[] = $item;
    }

    return $res;
}

function whata_simplex($s,&$i,$len, & $conf) {
    $c = $s[$i];
    if (
        ($c === '*') // bold
     || ($c === "'") // italic
     || ($c === '-') // strike
     || ($c === '_') // underline
     || ($c === '`') // code (inline pre)
     || ($c === '"') // mark
    ) {
        $i+=2;
        $d = $i;
        $ok = false;
        while ($i < $len) {
            if ($s[$i] === WHATA_TAG_START) {
                whata_tag($s,$i, $len, $conf, false, true); // we skip the begining of the tag. FIXME: might change conf
            } else if ($s[$i] === $c && $i+1 < $len && $s[$i+1] === $c) {
                ++$i;
                $ok = true;
                break;
            } else {
                ++$i;
            }
        }

        ++$i;

        if ($ok && $i - $d - 2) {
            return array('nodeType'=>'tag', 'tagName'=>whata_simplex2nodeName($c), 'content'=>whataDOM(substr($s, $d, $i - $d - 2), $conf));
        } else {
            $i = $d-2;
            return false;
        }
    } else if ($c === "[" && $s[$i+1] === "[") // link
    {
        ++$i;
        $d = $i;

        while ($i < $len) {
            if ($s[$i] === ']' && $i+1 < $len && $s[$i+1] === ']') {
                $i+=2;
                break;
            }
            ++$i;
        }
        list($url, $content) = explode(substr($s, $d, $i - $d), '|', 2);
        $alreadyHaveContent = true;
        whata_link($url, $alreadyHaveContent, $conf);

        if ($count === 2) {
            ++$i;

            return array('nodeType'=>'tag', 'tagName'=>'[', 'content'=>whataDOM($content, $conf, $tmp=0, 0, ''), 'value'=>$url);
        }
    }

    return false;
}

function whata_math_dollar($s,&$i,$len, & $conf) {
    if (!isset($conf['sets']['mathDollar']) || trim(whata_text_content($conf['sets']['mathDollar'])) !== 'true') {
        return false;
    }

    if ($i < $len && $s[$i] === '$') {
        ++$i;
        if ($i < $len && $s[$i] === '$') {
            ++$i;
            $display = true;
        } else {
            $display = false;
        }

        $d = $i;

        while ($i < $len && $s[$i] !== '$') {
            if($s[$i] === '\\') {
                ++$i;
            }
            ++$i;
            $end = $i;
            if($i < $len && $s[$i] === '$' && $display) {
                ++$i;
            }
        }

        if ($i < $len && $s[$i] === '$' ) {
            ++$i;
        }

        return array('nodeType'=>'tag', 'tagName'=> $display ? 'M' : 'm', 'content'=> substr($s, $d, $end - $d));
    }

    return false;
}

function letter_or_figure($c) {
    return preg_match('/[a-zA-Z0-9]/', $c);
//    $n = ord($c);
//    return ($n > 64 && $n < 91) || ($n > 96 && $n < 123) || ($n > 47 && $n < 58);
}

function letter($c) {
    return preg_match('/[a-zA-Z]/', $c);
//    $n = ord($c);
//    return ($n > 64 && $n < 91) || ($n > 96 && $n < 123);
}

function attrname($s, &$i, $len) {
    $n='';
    $I = $i;
    if (letter($s[$i])) {
        ++$i;
        while ($i < $len && letter_or_figure($s[$i]))
            ++$i;

        if ($i < $len && $s[$i] === WHATA_ATTR_VALUE)
            return substr($s, $I, $i - $I);
    }
    $i = $I;
    return FALSE;
}

function whata_parse_value($s,&$i,$len) {
    $value = '';
    $c = $s[$i];
    if ($c === '"' || $c === "'") {
      ++$i;
        $endOfString = $c;
        while ($i < $len) {
            if ($s[$i] === $endOfString) {
                ++$i;
                break;
            } else if ($s[$i] === WHATA_ESCAPE_CHAR) {
                ++$i;
                if ($i < $len) {
                    if (($c = $s[$i]) !== "\n") {
                        $value .= $c;
                    }
                } else {
                    $value .= WHATA_ESCAPE_CHAR;
                    break;
                }
            } else {
            $value .= $s[$i];
            }
            ++$i;
        }
    } else {
        while ($i < $len) {
            if (($c = $s[$i]) === WHATA_ATTR_END || $c === WHATA_TAG_END || is_white($c) || $c === WHATA_ATTR_SEP)
                break;
            else if ($c === WHATA_ESCAPE_CHAR) {
                ++$i;
                if ($i < $len) {
                    if (($c = $s[$i]) !== "\n") {
                        $value .= $c;
                    }
                } else {
                    $value .= WHATA_ESCAPE_CHAR;
                    break;
                }
            } else
                $value .= $c;
            ++$i;
        }
    }
    return $value;
}

function whata_parse_tag_name($s, &$i, $len) {
    $tagName = '';

    while ($i < $len && !is_white($c = $s[$i]) && $c !== WHATA_ATTR_VALUE && $c !== WHATA_TAG_END && $c !== WHATA_ATTR_END) {
        $tagName .= $c;
        ++$i;
        if (!letter_or_figure($c) && $c !== '*')
            break; // allows writing things like that : [^2]  => will be understood like [^ 2]
    }
    return $tagName;
}

function whata_tag($s,&$i, $len, & $conf, $is_begining_of_the_line=false, $stopBeforeContent = false) {
    if (++$i === $len)
        return false;

    $explicit_attribute_start = false;

    if ($s[$i] === WHATA_ATTR_START) {
        $explicit_attribute_start = true;
        ++$i;
    } else if (is_white($s[$i])) {
        if ($is_begining_of_the_line)
            $conf['lineStartNotParsed'] = true;

        $curTitleLevel = $conf['curLevel'];
        $lastTitleLevel = $conf['lastTitleLevel'];
        $conf['curLevel'] = $conf['lastTitleLevel'];
        $section = array('nodeType'=>'section', 'content'=>whataDOM($s, $conf, $i, $len, WHATA_TAG_END));
        if (isset($section['content'][0]) && $section['content'][0]['nodeType'] ==='title') {
            $section['sectionType'] = 'title';
            $section['titleLevel'] = $section['content'][0]['level'];
        } else
            $section['sectionType'] = '';
        $conf['curLevel'] = $curTitleLevel;
        $conf['lastTitleLevel'] = $lastTitleLevel;
        ++$i;

        return $section;
    }

    $tagName = whata_parse_tag_name($s, $i, $len);

    if ($i == $len)
        return false;

    if ($s[$i] === WHATA_ATTR_VALUE) {
        ++$i;
        $tagValue = whata_parse_value($s,$i,$len);
    } else $tagValue = '';

    $tagAttributes = array();
    $content = array();

    skipSpace($s, $i, $len);

    if ($i < $len && $s[$i] === WHATA_ATTR_END) {
        ++$i;
        if ($stopBeforeContent) {
            return;
        }
        $content = whataDOM($s, $conf, $i, $len, WHATA_TAG_END);
    } else {
        while ($attr = attrname($s,$i,$len)) {
            ++$i;
            if ($attrvalue = whata_parse_value($s,$i,$len)) {
                $tagAttributes[$attr] = $attrvalue;
                skipSpace($s,$i,$len);
            } else {
                $i-=strlen($attr) + 2; // we add the equal sign because it was eaten by attrname()
                break;
            }
        }

        if ($i <$len) {
            if ($s[$i] === WHATA_ATTR_END || $s[$i] === "\n") {
                if ($s[$i] === "\n")
                    $conf['lineStartNotParsed'] = true;

                ++$i;
                if ($stopBeforeContent) {
                    return;
                }
                $content = whataDOM($s, $conf, $i, $len, WHATA_TAG_END);
            } else if ($s[$i] !== WHATA_TAG_END) {
                if ($explicit_attribute_start)
                    $end = WHATA_TAG_END . WHATA_ATTR_SEP . WHATA_ATTR_END;
                else
                    $end = WHATA_TAG_END . WHATA_ATTR_SEP;

                $curAttr = 0;

                while ($i < $len) {
                    $beforeLastAttibute = $i;
                    $tagAttributes[$curAttr++] = whataDOM($s, $conf, $i, $len, $end);

                    if ($i >= $len || $s[$i] !== WHATA_ATTR_SEP)
                        break;
                    ++$i;
                }

                if ($i < $len) {
                    if ($explicit_attribute_start && $s[$i] === WHATA_ATTR_END) {
                        ++$i;
                        if ($stopBeforeContent) {
                            return;
                        }
                        $content = whataDOM($s, $conf, $i, $len, WHATA_TAG_END);
                    } else if ($curAttr) {
                        if ($stopBeforeContent) {
                            $i = $beforeLastAttibute;
                            return;
                        }
                        $content = &$tagAttributes[--$curAttr]; //$curAttr become useless anyway
                        unset($tagAttributes[$curAttr]);
                    }
                } else if ($curAttr) {
                    if ($stopBeforeContent) {
                        $i = $beforeLastAttibute;
                        return;
                    }
                    $content = &$tagAttributes[--$curAttr]; //$curAttr become useless anyway
                    unset($tagAttributes[$curAttr]);
                }
            }
        }
    }

    if ($tagName === WHATA_TAG_START) {
        if ($i+1 < $len && $s[$i+1] === WHATA_TAG_END)
            ++$i;
        $tagName = ' '; // one space codes a tag like this : [[ blah... ] (with the tagName being WHATA_TAG_START (here '[').
        // This allow whata DOM to any format to know that the tag name was WHATA_TAG_START when the document was parsed.
        // ' ' is a forbiden tag name in Whata so no conflict can occure.
    }

    ++$i;

    if ($tagName === 'ref') {
        if(!isset($conf['refs'])) {
            $conf['refs'] = array();
        }

        $conf['refs'][$tagValue] = array(
            'content' => $content
        );
    } else if (isset($tagAttributes['ref']) && (isset($tagAttributes['reftitle']) || isset($tagAttributes['title']))) {
        if(!isset($conf['refs'])) {
            $conf['refs'] = array();
        }

        $conf['refs'][$tagValue] = array(
            'content' => whataDOM(
                            (isset($tagAttributes['reftitle'])
                                ? $tagAttributes['reftitle']
                                : $tagAttributes['title']),
                            $conf
                        )
        );
    }


    if ($tagName === 'deftag') {
        $conf['tagDefinitions'][$tagValue] = array(
            'attributes' => $tagAttributes,
            'content'    => $content
        );

        $content = array();
    } else if (isset($conf['tagDefinitions'][$tagName])) {
        return array(
            'nodeType'    => 'tagList',
            'tagName'     => $tagName,
            'attributes'  => $tagAttributes,
            'contentorig' => $content,
            'content'     => $conf['tagDefinitions'][$tagName]['content'],
            'value'       => $tagValue
        );
    } else if ($tagName === 'cdata') {
        if (isset($tagAttributes['file'])) {
            $c = whata_get_file_contents($tagAttributes['file'], $conf, true);
            return array(
                'nodeType' => 'cdata',
                'content'  => $c,
                'length'   => strlen($c),
                'cdata'    => '',
                'cdataFile' => $tagAttributes['file']
            );
        }
    }

    return array('nodeType'=>'tag','tagName'=>$tagName, 'attributes'=>$tagAttributes, 'content'=>$content, 'value'=>$tagValue);
}

function whataGetValues($s, & $conf = array(), $set_array = null, & $i = 0, $len = -1) {
    if ($len === -1) {
        $len = strlen($s);
    }

    if(!$set_array) {
        if (isset($conf['sets'])) {
            $set_array = & $conf['sets'];
        } else {
            $set_array = array();
        }
    }

    $d = $i;
    skipSpace($s,$d,$len);

    if ($d < $len && $s[$d] === WHATA_TAG_START) {
        ++$d;
        $tagName = '';
        while ($d < $len && letter($s[$d])) {
            $tagName .= $s[$d++];
        }

        if ($tagName === 'set') {
            // the file specifies values to pass to the program calling whataDOM.

            $i = $d + 1;
            $continue = true;
            while ($continue && $i < $len) {
                $key = '';
                while ($i < $len) {
                    if ($s[$i] === WHATA_ESCAPE_CHAR && $i + 1 < $len)
                        $key .= $s[++$i];
                    elseif ($s[$i] === WHATA_TAG_END) {
                        $continue = false;
                        break;
                    } elseif ($s[$i] === '=') {
                        ++$i;
                        break;
                    } else $key .= $s[$i];

                    ++$i;
                }
                if ($continue) {
                    skipSpace($s, $i, $len);
                    if ($s[$i] === '"') {
                        $value = '';
                        ++$i;
                        while ($i < $len && $s[$i] !== '"') {
                            if ($s[$i] === '\\') {
                                ++$i;
                                if ($i === $len)
                                $break;
                            }
                            $value .= $s[$i];
                            ++$i;
                        }
                        ++$i;
                        $value = whataDOM($value,$conf);
                        skipSpace($s, $i, $len);
                    } else {
                        $value = whataDOM($s,$conf, $i, $len, ",\n" . WHATA_TAG_END);
                    }
                    $set_array[trim($key)] = $value;
                }
            }
            ++$i;
        }
    }

    if (isset($set_array['explicitNL'])) {
        if ($set_array['explicitNL'] !== '0' && strtolower(trim(whata_text_content($set_array['explicitNL']))) !== 'false') {
            $conf['explicitNL'] = true;
        }
    }

    if (isset($set_array['nonOrdered'])) {
        if ($set_array['nonOrdered'] !== '0' && strtolower(trim(whata_text_content($set_array['nonOrdered']))) !== 'false') {
            $conf['nonOrderedTitleByDefault'] = true;
        }
    }

    return $set_array;
}

function whataDOM($s, &$conf=array(), &$i=0, $len=0, $end='') {
    if (!$i)
        $s = str_replace("\r", "\n", str_replace("\r\n", "\n", $s)); // To UNIX format, trim spaces.

    if (!isset($conf['curLevel']))
        $conf['curLevel'] = 0;
    if (!isset($conf['lastTitleLevel']))
        $conf['lastTitleLevel'] = $conf['curLevel'];

    if (!isset($conf['tagDefinitions'])) {
        $conf['tagDefinitions'] = array();
    }

    $indenting = 0;

    if (isset($conf['lineStartNotParsed'])) {
        if ($lineStartNotParsed = $conf['lineStartNotParsed'])
            $conf['lineStartNotParsed'] = false;
    } else {
        $lineStartNotParsed = true;
        $conf['lineStartNotParsed'] = false;
    }

    $malformed_list = false;

    $indentingChars = '';

    $root = array();

    if (!isset($conf['sets']) || !is_array($conf['sets']))
        $conf['sets'] = array();

    if (!$len) {
        $len = strlen($s);
        if (!$len)
            return $root;
    }

    $skipNormalParsing = $lineStartNotParsed;

    $nl2append = null;

    $domLen = 0;

    $curTreeCount = & $domLen;


    $curTree = & $root;

    if (isset($conf['skipBlanks'])) {
        unset($conf['skipBlanks']);
        while ($i < $len && is_white($s[$i]))
            ++$i;
    }

    whataGetValues($s, $conf, null, $i, $len);

    if (isset($conf['sets']['includes'])) {
        $includes = whata_split_values(whata_text_content($conf['sets']['includes']), $conf);
        unset($conf['sets']['includes']);
        foreach ($includes as $include) {
        whata_get_file_contents($include, $conf);
            whataDOM(whata_get_file_contents($include, $conf), $conf);
        }
    }

    $begining_of_the_line = -1;

    while ($i < $len) {
        $old_i = $i;
        skipSpace($s,$old_i,$len);
        if ($old_i === $len || $s[$old_i] === "\n")
            $i = $old_i;

        if ($i === $len)
            break;

        $c = $s[$i];
        if (strpos($end, $c) !== FALSE) {
            if ($lineStartNotParsed && $indenting)
                $curTree[$curTreeCount++] = array('nodeType'=>'whitespace', 'content'=>$indentingChars, 'length'=>$indenting);

            return $root;
        }

        if ($skipNormalParsing || $c === "\n") {
//            if $skipNormalParsing, we are at the begining of a line

            $countNL = 0;
            $begining_of_the_line = $old_i = $i;

            skipSpace($s,$i,$len);
            if ($i - $begining_of_the_line)
                ++$countNL;

            while ($i < $len && $s[$i] === "\n") {
                ++$i;
                $old_i = $i;
                skipSpace($s,$i,$len);
                ++$countNL;
            }

            if ($skipNormalParsing) {// We can now do normal parsing
                $skipNormalParsing = false;
            }

            $nl2append = array(
                'nodeType' => $countNL ? 'nl' : 'whitespace',
                'content'=> substr($s, $begining_of_the_line, $i - $begining_of_the_line)
            ); // save NLs in the DOM if not after title / list

            if ($countNL) {
                $nl2append['count'] = $countNL;
            }

            skipSpace($s,$i,$len);

            $indenting = $i - $old_i;

            if ($indenting) // save whitespace at the begining of the line, too, if any.
            {
                $curTree = & whata_get_node_to_complete($root, $domLen, $indenting);
                if ($curTree === $root) {
                    $curTreeCount = & $domLen;
                } else {
                    unset($curTreeCount);
                    $curTreeCount = count($curTree);
                }
                //$curTree[$curTreeCount++] = array('nodeType'=>'whitespace', 'content'=>substr($s, $old_i, $indenting), 'length'=>$indenting);
            } else {
                $curTree = & $root;
                $curTreeCount = & $domLen;
                $indentingChars = '';
            }

            if ($i === $len)
                break;

            $c = $s[$i];
            $lineStartNotParsed = true;
            $begining_of_the_line = $i;
        }

        if ($lineStartNotParsed) {
            if (strpos($end, $c) !== FALSE) {
                if ($nl2append)
                    $curTree[$curTreeCount++] = $nl2append;

                if ($indenting)
                    $curTree[$curTreeCount++] = array('nodeType'=>'whitespace', 'content'=>$indentingChars, 'length'=>$indenting);

                return $root;
            }

            $lineStartNotParsed = false;

// Lists
            if (whata_list_char($c)) {
                $listCharBegin = $i;

                while ($i < $len && whata_list_char($s[$i])) {
                    ++$i;
                }

                $listNode = & whata_get_node_to_complete($curTree, $curTreeCount, $indenting, $i - $listCharBegin - 1);

                if ($listNode === NULL) { // malformed list
                    $i = $listCharBegin;
                    $malformed_list = true;
                } else {
                    unset($listNodeCount);

                    if ($curTree === $listNode) {
                        $listNodeCount = & $curTreeCount;
                    } else {
                        $listNodeCount = count($listNode);
                    }

                    $listNode[$listNodeCount++] = array('nodeType'=>'listitem', 'indent'=>$indenting, 'bullet' => $s[$i-1], 'content'=> whataDOM($s, $conf, $i, $len, "\n" . $end));
                    $skipNormalParsing = true;
                }
            }
// Titles
            else if ($c === WHATA_TITLE_CHAR) {
                $titleBegin = $i;
                ++$i;
                while ($i < $len && $s[$i] === WHATA_TITLE_CHAR)
                    ++$i;

                if ($i < $len && $s[$i] === WHATA_UNORDERED_TITLE_CHAR) {
                    ++$i;
                    $ordered = isset($conf["nonOrderedTitleByDefault"]) ? $conf["nonOrderedTitleByDefault"] : false;
                    $level = $i - 1 - $titleBegin;
                } else {
                    $ordered = isset($conf["nonOrderedTitleByDefault"]) ? !$conf["nonOrderedTitleByDefault"] : true;
                    $level = $i - $titleBegin;
                }

                $curTree[$curTreeCount] = array('nodeType'=>'title', 'ordered'=>$ordered, 'level' => $level, 'content'=> whataDOM($s, $conf, $i, $len, "\n" . $end));
                $curTree[$curTreeCount]['realLevel'] = $curTree[$curTreeCount]['level'] + $conf['curLevel'];

                if ($ordered) {
                    if (!isset($conf['titleNumbers']))
                        $conf['titleNumbers'] = array();

                    if (!isset($conf['titleNumbers'][$curTree[$curTreeCount]['realLevel']]))
                        $conf['titleNumbers'][$curTree[$curTreeCount]['realLevel']] = 1;
                    else
                        ++ $conf['titleNumbers'][$curTree[$curTreeCount]['realLevel']];

                    $t = $curTree[$curTreeCount]['realLevel'] + 1;
                    while (isset($conf['titleNumbers'][$t])) {
                        $conf['titleNumbers'][$t] = 0;
                        ++$t;
                    }

                    $curTree[$curTreeCount]['titleNumber'] = $conf['titleNumbers'][$curTree[$curTreeCount]['realLevel']];
                }

                $conf['lastTitleLevel'] =  $curTree[$curTreeCount]['realLevel'];
                ++$curTreeCount;
                $skipNormalParsing = true;
            }

// Tables

            else if ($c === '|') {
                $table = array(array());
                $curLine = 0;
                $curCell=-1;

                while ($i < $len && $s[$i] === '|') {
                    $table[$curLine][++$curCell] = array('align'=>'auto', 'head'=>false, 'colspan'=>1, 'rowspan'=>1);
                    $cell = & $table[$curLine][$curCell];

                    $colspan = 1;
                    ++$i;

                    if ($i === $len)
                        break;

                    while ($s[$i] === '|') {
                        ++$i;
                        ++$cell['colspan'];
                    }

                    while ($s[$i] === '_') {
                        ++$i;
                        ++$cell['rowspan'];
                    }


                    if ($s[$i] === '<') {
                        ++$i;
                        if ($s[$i] === '>') {
                            $cell['align'] = 'justify';
                            ++$i;
                        } else {
                            $cell['align'] = 'left';
                        }
                    } else if ($s[$i] === '>') {
                        ++$i;
                        if ($s[$i] === '<') {
                            $cell['align'] = 'center';
                            ++$i;
                        } else {
                            $cell['align'] = 'right';
                        }
                    }

                    if ($s[$i] === WHATA_ESCAPE_CHAR && $i+1 < $len && $s[$i+1] === ' ')
                        $i += 2;
                    else {
                        if ($s[$i] === '=') {
                            $cell['head'] = true;
                            ++$i;
                        }
                    }

                    $cell['content'] = whataDOM($s, $conf, $i, $len, "\n|");

                    if ($i < $len && $s[$i] === '|') {
                        $old_i = $i++;
                        skipSpace($s, $i, $len);

                        if ($i < $len && $s[$i] !== "\n")
                            $i = $old_i;
                    }

                    if ($i < $len && $s[$i] === "\n") {
                        ++$i;

                        $old_i = $i;
                        skipSpace($s, $i, $len);

                        if ($i === $len || $s[$i] !== '|') {
                            $i = $old_i;
                            $skipNormalParsing = true;
                        } else {
                            $table[++$curLine] = array();
                            $curCell = -1;
                        }
                    }
                }

                if ($curLine || $curCell) {
                    $curTree[$curTreeCount++] = array('nodeType'=>'table', 'content'=>$table);
                }
            } else if ($nl2append) {
                $curTree[$curTreeCount++] = $nl2append;
            }

            if ($malformed_list) {
                if ($nl2append)
                    $curTree[$curTreeCount++] = $nl2append;

                $malformed_list = false;
            }

            $nl2append = null;

            if ($i >= $len)
                break;

            $c = $s[$i];
        }

        if (!$skipNormalParsing) {
            if ($c === WHATA_CDATA_START && $cdataNode = whata_cdata($s, $i, $len)) {
                $curTree[$curTreeCount++] = $cdataNode;
                --$i;
            } else if ($c === WHATA_ESCAPE_CHAR) {
                ++$i;
                if ($i < $len) {
                    if ($s[$i] === 'n') {
                        $curTree[$curTreeCount++] = array('nodeType'=>'entity', 'content'=>'nl'); // escaping n = new line
                    } else if ($s[$i] === ' ') {
                        $curTree[$curTreeCount++] = array('nodeType'=>'entity', 'content'=>'nbsp'); // escaping space = non-breaking space
                    } else if ($s[$i] !== "\n") { // escaping \n makes whata completely ignore it
                        $curTree[$curTreeCount++] = array('nodeType'=>'esc', 'content'=>$s[$i]); // FIXME: could break utf-8 escaped char (should not happen, utf-8 chars don't need to be escaped)
                    }
                } else {
                    whata_addTextContent($curTree, $curTreeCount, WHATA_ESCAPE_CHAR);
                }
                ++$i;
            }
// Whata tag
            else if (
                ($c === WHATA_TAG_START && $node = whata_tag($s, $i, $len, $conf, $begining_of_the_line === $i)) ||
                (
                    ($i+1 < $len && (
                        $c === $s[$i+1] && $node = whata_simplex($s,$i,$len,$conf)
                    )) ||
                    $node = whata_math_dollar($s, $i, $len, $conf)
                )
            ) {
                $curTree[$curTreeCount++] = $node;
            } else {
                $old_i = $i;
                do
                {
                    ++$i;
                }
                while ($i < $len && !(
                    ($c = $s[$i]) === WHATA_TAG_START
                    || $c === WHATA_TAG_END
                    || $c === "\n"
                    || $c === WHATA_ATTR_SEP
                    || $c === '*'
                    || $c === '_'
                    || $c === '"'
                    || $c === "'"
                    || $c === '-'
                    || $c === '$'
                    || $c === WHATA_ESCAPE_CHAR
                    || $c === WHATA_CDATA_START
                    || $c === '`'
                ));

                whata_addTextContent($curTree, $curTreeCount, substr($s, $old_i, $i - $old_i));
            }
        }
    }
    return $root;
}
?>
