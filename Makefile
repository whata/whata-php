DESTDIR=/usr/local
PHPDESTDIR=/usr/share/php

all:

install:
	mkdir -p ${PHPDESTDIR}/whata
	mkdir -p ${DESTDIR}/share/whata
	cp htmlwhata-tools.php config.php simplex.php whata2html.php whatadom.php ${PHPDESTDIR}/whata
	cp optimize_indentation.php ${PHPDESTDIR}/
	cp htmlwhata ${DESTDIR}/bin
	cp -r share/* ${DESTDIR}/share/whata/
	
	chmod -R +r ${DESTDIR}/share/whata
	chmod -R +r ${PHPDESTDIR}/whata
	chmod +r ${PHPDESTDIR}/optimize_indentation.php
	chmod +x ${DESTDIR}/bin/htmlwhata

clean:
