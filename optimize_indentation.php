<?php
/*
	Copyright (c) 2013, JAKSE Raphaël
	All rights reserved.
	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are
	met:

	    (1) Redistributions of source code must retain the above copyright
	    notice, this list of conditions and the following disclaimer. 

	    (2) Redistributions in binary form must reproduce the above copyright
	    notice, this list of conditions and the following disclaimer in
	    the documentation and/or other materials provided with the
	    distribution.  
	    
	    (3)The name of the author may not be used to
	    endorse or promote products derived from this software without
	    specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
	IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
	INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
	HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
	STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
	IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.
*/

// skips space and tab characters
// i.e. sets $i after the next non-space and non-tab character.
// if $s[$i] is not a space / tab character, leave $i unchanged.
function after_spaces($s, &$i, $len) {
	while($i < $len && strpos(" \t", $s[$i]) !== FALSE) {
		++$i;
	}
}

// checks if $s contains nothing else than space or tab characters.
function indentation_to_be_munched($s, $len) {
	$i=0;
	while($i < $len) {
		if($s[$i] !== " " && $s[$i] !== "\t")
			return false;
		++$i;
	}
	return true;
}

//takes a piece of code and unindent it in an optimal way, i.e.
// - the same amount of white character is retired to each begining of line
// - this amount is a big as possible (i.e without losing actual code)
// NOTICE: only Unix end of lines are supported.
function optimize_indentation($s, &$len=null) {
	if($len === null)
		$len = strlen($s);

	if($len) { // if the first and the last characters are a new line
		$minIndent = $i = 0; // Initial situation : we are a the first character of $s, which if a new line.
		$notEmptyLineEncountered = false; // We still have not been on a non-empty line

		$nl = -1;
		while($i < $len) {
			if($nl === FALSE)
				break; // break if there isn't any

			$nl += $i; // the real index of the new line
			$i = $nl + 1; // we go to just after the new line considered
			$old_i = $i; 
			after_spaces($s, $i, $len);
			if($i >= $len)
            break;

			if($s[$i] !== "\n") { // if we are not on an empty / "white" line
				if(!$notEmptyLineEncountered || $i - $old_i < $minIndent) // if the line is less indented than the least indented
					$minIndent = $i-$old_i; // or if no line was there before this one, we keep the minimum indentation

				$notEmptyLineEncountered = true; // we saw a non-empty line
			}
			$nl = strpos(substr($s, $i, $len-$i), "\n"); // we look for the next new line
		}

		if($minIndent) { // if we can unindent
			$i=0;
			$nl=-1;
			while($i < $len) {
				if($nl === FALSE)
					break;
				else {
					$nl += $i;
					$i = $nl + 1;
					if($i + $minIndent <= $len && indentation_to_be_munched(substr($s, $i, $minIndent), $minIndent)) {
						// we eat $minIndent characters if we are on a non empty line.
						$s = substr_replace($s, '', $i, $minIndent);
						$len -= $minIndent;
					}
				}
				$nl = strpos(substr($s, $i, $len-$i), "\n");
			}
		}
	}
	return $s;
}
?>